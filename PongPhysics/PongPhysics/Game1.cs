﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace PongPhysics
{
    public class Game1 : Game
    {
        enum GameStates
        {
            MENU,
            PLAY,
            GAMEOVER,
        }

        GameStates currentGameState = GameStates.MENU;

        GraphicsDeviceManager graphics;
        SpriteBatch sb;
        SpriteFont sf;

        Random rand = new Random();

        Texture2D blankSquare;
        Texture2D menuTexture;
        Texture2D gameOverTexture;

        Color playGameStateBackgroundColour = Color.White;

        KeyboardState currentKbState;
        KeyboardState prevKbState;

        int windowWidth;
        int windowHeight;

        #region Player 1
        Keys player1Up = Keys.W;
        Keys player1Down = Keys.S;
        Vector2 player1StartPos;
        Vector2 player1Position = new Vector2(0, 100);
        Vector2 player1Velocity = new Vector2(0, 0);
        Vector2 player1Dimensions = new Vector2(25, 150);
        Color player1Colour = Color.Red;
        float player1MovementSpeed = 5;
        float player1Friction = 0.95f;
        int player1Score = 0;
        SoundEffectInstance player1PaddleBounceInstance;
        SoundEffectInstance player1LoseInstance;
        #endregion

        #region Player 2
        Keys player2Up = Keys.Up;
        Keys player2Down = Keys.Down;
        Vector2 player2StartPos;
        Vector2 player2Position = new Vector2(1100, 100);
        Vector2 player2Velocity = new Vector2(0, 0);
        Vector2 player2Dimensions = new Vector2(25, 150);
        Color player2Colour = Color.Black;
        float player2MovementSpeed = 5;
        float player2Friction = 0.95f;
        int player2Score = 0;
        SoundEffectInstance player2PaddleBounceInstance;
        SoundEffectInstance player2LoseInstance;
        #endregion

        #region Ball
        Vector2 ballStartPos;
        Vector2 ballPosition = new Vector2(500, 100);
        Vector2 ballVelocity;
        Vector2 ballDimensions = new Vector2(32, 32);
        Color ballColour = Color.Blue;
        float ballMovementSpeed = 10;
        float ballFriction = 0.55f;
        float ballCurrentFriction;
        //float ballRotation = 1;
        SoundEffectInstance ballBounceSoundEffectInstance;
        #endregion

        bool player1Wins = false;
        
        public Game1() 
            : base()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            windowWidth = graphics.PreferredBackBufferWidth;
            windowHeight = graphics.PreferredBackBufferHeight;

            player1StartPos = player1Position;
            player2StartPos = player2Position;
            ballStartPos = ballPosition;
            ballVelocity = new Vector2(rand.Next(-1000, 1000), rand.Next(-1000, 1000));
            ballVelocity.Normalize();
            ballCurrentFriction = ballFriction;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            sb = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            blankSquare = Content.Load<Texture2D>("blankSquare");
            menuTexture = Content.Load<Texture2D>("playGame");
            gameOverTexture = Content.Load<Texture2D>("gameOver");
            sf = Content.Load<SpriteFont>("spriteFont1");

            SoundEffect player1PaddleBounceSoundEffect = Content.Load<SoundEffect>("paddle1Sound");
            SoundEffect player1LoseSoundEffect = Content.Load<SoundEffect>("paddle1Lose");

            SoundEffect player2PaddleBounceSoundEffect = Content.Load<SoundEffect>("paddle2Sound");
            SoundEffect player2LoseSoundEffect = Content.Load<SoundEffect>("paddle2Lose");

            SoundEffect ballSoundEffect = Content.Load<SoundEffect>("ballBounce");

            player1PaddleBounceInstance = player1PaddleBounceSoundEffect.CreateInstance();
            player1LoseInstance = player1LoseSoundEffect.CreateInstance();
            player2PaddleBounceInstance = player2PaddleBounceSoundEffect.CreateInstance();
            player2LoseInstance = player2LoseSoundEffect.CreateInstance();
            ballBounceSoundEffectInstance = ballSoundEffect.CreateInstance();
        }

        protected override void UnloadContent() { Content.Unload(); }

        #region Update
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            currentKbState = Keyboard.GetState();

            switch (currentGameState)
            {
                case GameStates.MENU:
                    UpdateMenuGameState(gameTime);
                    break;
                case GameStates.PLAY:
                    UpdatePlayGameState(gameTime);
                    break;
                case GameStates.GAMEOVER:
                    UpdateGameOverGameState(gameTime);
                    break;
            }

            prevKbState = currentKbState;
            base.Update(gameTime);
        }
        #endregion

        #region Draw
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            sb.Begin();

            switch (currentGameState)
            {
                case GameStates.MENU:
                    DrawMenuGameState();
                    break;
                case GameStates.PLAY:
                    DrawPlayGameState();
                    break;
                case GameStates.GAMEOVER:
                    DrawGameOverGameState();
                    break;
            }

            sb.End();

            base.Draw(gameTime);
        }
        #endregion

        #region Game state functions
        void UpdateMenuGameState(GameTime a_GT)
        {
            if (currentKbState.IsKeyDown(Keys.Enter) && currentKbState != prevKbState)
            {
                currentGameState = GameStates.PLAY;
            }
        }

        void DrawMenuGameState()
        {
            sb.Draw(menuTexture, new Rectangle(0, 0, windowWidth, windowHeight), Color.White);
        }

        void UpdatePlayGameState(GameTime a_GT)
        {
            #region Player 1
            Player1Boundaries();
            Player1Movement();
            #endregion

            #region Player 2
            Player2Boundaries();
            Player2Movement();
            #endregion

            #region Ball
            BallMovement();
            BallBoundaries();
            #endregion

            #region Paddle and player 1 collision
            if (CheckCollision(GetRectangle(player1Position, player1Dimensions), GetRectangle(ballPosition, ballDimensions)))
            {
                #region Old collision
                //float relativeIntersectY = (player1Position.Y + (player1Dimensions.Y * 0.5f)) - ballPosition.Y;
                //float normalisedRelativeIntersectY = (relativeIntersectY / (player1Dimensions.Y * 0.5f));
                //float bounceAngle = normalisedRelativeIntersectY * 75.0f;

                //if (ballPosition.X > player1Position.X)
                //{
                //    ballVelocity.X = Math.Abs(ballVelocity.X);
                //    ballVelocity.Y = Math.Abs(ballVelocity.Y);

                //    ballPosition += ballVelocity * ballMovementSpeed;
                //}

                //ballVelocity.X = (float)(ballMovementSpeed * Math.Cos(bounceAngle));
                //ballVelocity.Y = (float)(ballMovementSpeed * -Math.Sin(bounceAngle));
                #endregion

                ballVelocity = (Vector2.Reflect(ballVelocity, new Vector2(1, 0)));
                ballCurrentFriction = ballFriction;

                player1PaddleBounceInstance.Play();
            }
            #endregion

            #region Paddle and player 2 collision
            if (CheckCollision(GetRectangle(player2Position, player2Dimensions), GetRectangle(ballPosition, ballDimensions)))
            {
                #region Old collision
                //float relativeIntersectY = (player2Position.Y + (player2Dimensions.Y * 0.5f)) - ballPosition.Y;
                //float normalisedRelativeIntersectY = (relativeIntersectY / (player2Dimensions.Y * 0.5f));
                //float bounceAngle = normalisedRelativeIntersectY * 75.0f;

                //if (ballPosition.X < player2Position.X)
                //{
                //    ballVelocity.X = Math.Abs(ballVelocity.X);
                //    ballVelocity.Y = Math.Abs(ballVelocity.Y);

                //    ballPosition -= ballVelocity * ballMovementSpeed;
                //}

                //ballVelocity.X = (float)(ballMovementSpeed * Math.Cos(bounceAngle));
                //ballVelocity.Y = (float)(ballMovementSpeed * -Math.Sin(bounceAngle));
                #endregion

                ballVelocity = (Vector2.Reflect(ballVelocity, new Vector2(-1, 0)));
                ballCurrentFriction = ballFriction;

                player2PaddleBounceInstance.Play();
            }
            #endregion

            #region End game
            if (player1Score >= 5)
            {
                if (player1Score > player2Score + 2)
                {
                    ResetGame();
                    player1Wins = true;
                    currentGameState = GameStates.GAMEOVER;
                }
            }
            
            if (player2Score >= 5)
            {
                if (player2Score > player1Score + 2)
                {
                    ResetGame();
                    player1Wins = false;
                    currentGameState = GameStates.GAMEOVER;
                }
            }
            #endregion
        }

        void DrawPlayGameState()
        {
            sb.Draw(blankSquare, new Rectangle(0, 0, windowWidth, windowHeight), playGameStateBackgroundColour);
            sb.DrawString(sf, "Player 1 score:", new Vector2(player1Position.X + 10, 10), Color.Black);
            sb.DrawString(sf, "Player 2 score:", new Vector2(player2Position.X - 150, 10), Color.Black);
            sb.DrawString(sf, player1Score.ToString(), new Vector2(player1Position.X + 10, 30), Color.Black);
            sb.DrawString(sf, player2Score.ToString(), new Vector2(player2Position.X - 10, 30), Color.Black);
            sb.Draw(blankSquare, GetRectangle(player1Position, player1Dimensions), player1Colour);
            sb.Draw(blankSquare, GetRectangle(player2Position, player2Dimensions), player2Colour);
            sb.Draw(blankSquare, GetRectangle(ballPosition, ballDimensions), ballColour);
            //sb.Draw(blankSquare, GetRectangle(ballPosition, ballDimensions), null, ballColour, ballRotation += 0.1f, new Vector2(blankSquare.Width * 0.5f, blankSquare.Height * 0.5f), SpriteEffects.None, 0);
            //sb.Draw(blankSquare, GetRectangle(ballPosition, ballDimensions), null, ballColour, ballRotation += 0.1f, new Vector2(0,0), SpriteEffects.None, 0);
            DrawLine(new Vector2(ballPosition.X + ballDimensions.X * 0.5f, ballPosition.Y + ballDimensions.Y * 0.5f), ballPosition + ballCurrentFriction * ballVelocity * 250, Color.Black);
        }

        void UpdateGameOverGameState(GameTime a_GT)
        {
            if (currentKbState.IsKeyDown(Keys.Enter) && currentKbState != prevKbState)
            {
                currentGameState = GameStates.PLAY;
            }
        }

        void DrawGameOverGameState()
        {
            sb.Draw(gameOverTexture, new Rectangle(0, 0, windowWidth, windowHeight), Color.White);
            if (player1Wins)
            {
                sb.DrawString(sf, "Player 1 wins", new Vector2(windowWidth / 2, 200), Color.Black);
            }
            else
            {
                sb.DrawString(sf, "Player 2 wins", new Vector2(windowWidth / 2, 200), Color.Black);
            }
        }
        #endregion

        #region Helper functions
        Rectangle GetRectangle(Vector2 position, Vector2 dimensions)
        {
            Rectangle temp = new Rectangle( (int)(position.X), (int)(position.Y), (int)(dimensions.X), (int)(dimensions.Y) );
            return temp;
        }

        bool CheckCollision(Rectangle rect1, Rectangle rect2)
        {
            if (rect1.Intersects(rect2))
            {
                return true;
            }
            return false;
        }

        void Player1Movement()
        {
            if (!Keyboard.GetState().IsKeyDown(player1Up) && !Keyboard.GetState().IsKeyDown(player1Down))
            {
                player1Velocity *= player1Friction;
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(player1Up))
                {
                    player1Velocity.Y = -1;
                }

                if (Keyboard.GetState().IsKeyDown(player1Down))
                {
                    player1Velocity.Y = 1;
                }
            }
            player1Position += player1Velocity * player1MovementSpeed;
        }

        void Player1Boundaries()
        {
            if (player1Position.X < 0)
            {
                player1Position.X = 0.1f;
            }

            if (player1Position.Y < 0)
            {
                player1Position.Y = 0.1f;
            }

            if (player1Position.X + player1Dimensions.X > windowWidth)
            {
                player1Position.X = windowWidth - player1Dimensions.X - 0.1f;
            }

            if (player1Position.Y + player1Dimensions.Y > windowHeight)
            {
                player1Position.Y = windowHeight - player1Dimensions.Y - 0.1f;
            }
        }

        void Player2Movement()
        {
            if (!Keyboard.GetState().IsKeyDown(player2Up) && !Keyboard.GetState().IsKeyDown(player2Down))
            {
                player2Velocity *= player2Friction;
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(player2Up))
                {
                    player2Velocity.Y = -1;
                }

                if (Keyboard.GetState().IsKeyDown(player2Down))
                {
                    player2Velocity.Y = 1;
                }
            }
            player2Position += player2Velocity * player2Friction * player2MovementSpeed;
        }

        void Player2Boundaries()
        {
            if (player2Position.X < 0)
            {
                player2Position.X = 0.1f;
            }

            if (player2Position.Y < 0)
            {
                player2Position.Y = 0.1f;
            }

            if (player2Position.X + player2Dimensions.X > windowWidth)
            {
                player2Position.X = windowWidth - player1Dimensions.X - 0.1f;
            }

            if (player2Position.Y + player2Dimensions.Y > windowHeight)
            {
                player2Position.Y = windowHeight - player2Dimensions.Y - 0.1f;
            }
        }

        void BallMovement()
        {
            ballVelocity.Normalize();
            ballVelocity *= ballCurrentFriction;
            ballPosition += ballVelocity * ballCurrentFriction * ballMovementSpeed;
        }

        void BallBoundaries()
        {
            if (ballPosition.X < 0)
            {
                ResetBall();
                player2Score++;
                player1LoseInstance.Play();
            }

            //top wall
            if (ballPosition.Y < 0)
            {
                ballPosition.Y = 0.1f;
                ballVelocity = (Vector2.Reflect(ballVelocity, new Vector2(0, -1)));
                ballBounceSoundEffectInstance.Play();
            }

            if (ballPosition.X + ballDimensions.X > windowWidth)
            {
                ResetBall();
                player1Score++;
                player2LoseInstance.Play();
            }

            //bottom wall
            if (ballPosition.Y + ballDimensions.Y > windowHeight)
            {
                ballPosition.Y = windowHeight - ballDimensions.Y - 0.1f;
                ballVelocity = (Vector2.Reflect(ballVelocity, new Vector2(0, 1)));
                ballBounceSoundEffectInstance.Play();
            }
        }

        void ResetBall()
        {
            ballPosition = ballStartPos;
            ballVelocity.X = rand.Next(-1000, 1000);
            ballVelocity.Y = rand.Next(-1000, 1000);
            ballVelocity.Normalize();
            ballMovementSpeed = rand.Next(5, 20);
        }

        void ResetPlayers()
        {
            player1Position = player1StartPos;
            player2Position = player2StartPos;
        }

        void ResetGame()
        {
            ResetBall();
            ResetPlayers();

            player1Score = 0;
            player2Score = 0;
        }
        #endregion

        public void DrawLine(Vector2 begin, Vector2 end, Color color, int width = 1)
        {
            Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length() + width, width);
            Vector2 v = Vector2.Normalize(begin - end);
            float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
            if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
            sb.Draw(blankSquare, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}
