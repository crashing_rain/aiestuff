﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberTest
{
    public class ConsoleKeyEventPublisher
    {
        public delegate void TKeyPressed(char key);

        public char prevKey;
        public char currKey;

        private TKeyPressed KeyPressedEvent = (char key) => {  };

        public void Subscribe(TKeyPressed func)
        {
            KeyPressedEvent = func;
        }

        public void Update()
        {
            if (Console.KeyAvailable)
            {
                char keyPressed = Console.ReadKey(true).KeyChar;
                currKey = keyPressed;
                KeyPressedEvent(keyPressed);
                prevKey = currKey;
            }
        }
    }
}
