﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublisherSubscriberTest
{
    class Program
    {
        public bool shouldQuit = false;
        public delegate void PrintStuff(int num);

        static void Main(string[] args)
        {
            Program prog = new Program();

            //assigning delegate a to a function that
            //takes in a number and does something with it
            PrintStuff a = (int x) => { Console.WriteLine("9000"); };
            PrintStuff b = (int x) => { Console.WriteLine(x > 10 ? 89765 : 982367); };
            ConsoleKeyEventPublisher pause = new ConsoleKeyEventPublisher();
            ConsoleKeyEventPublisher quit = new ConsoleKeyEventPublisher();

            pause.Subscribe((char key) => { if (key == ' ') { Console.ReadLine(); } });
            pause.Subscribe((char key) => { if (key == 'q') { prog.shouldQuit = true; } });

            while (!prog.shouldQuit)
            {
                quit.Update();
                pause.Update();

                a(9000);
                b(9);
            }
        }
    }
}
