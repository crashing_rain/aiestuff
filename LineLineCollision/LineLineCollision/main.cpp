#include <iostream>
#include "include\Vector2.h"
#include "include\Vector4.h"

struct Line
{
	Vector2 start;
	Vector2 end;

	Line()
	{
		start = Vector2();
		end = Vector2();
	}

	Line(const Vector2& a_Start, const Vector2& a_End)
	{
		start = a_Start;
		end = a_End;
	}

	Line(float x1, float y1, float x2, float y2)
	{
		start = Vector2(x1, y1);
		end = Vector2(x2, y2);
	}

	Vector2 GetPointOnLine(float t)
	{
		return start + (end - start) * t;
	}

	//Right hand perp vector
	Vector2 GetNormal(bool normalise)
	{
		Vector2 temp = Vector2();
		temp.x = end.x - start.x;
		temp.y = end.y - start.y;
		
		if (normalise)
		{
			temp.Normalise();
		}
		return temp;
	}

	friend std::ostream& operator<<(std::ostream& output, const Line& a)
	{
		output << "Start: " << a.start << "End: " << a.end;
		return output;
	}
};

struct Ray
{
	Vector4 m_Origin;
	Vector4 m_Direction;
	float m_Range;

	Ray(const Vector4& a_Origin, const Vector4& a_Direction, float a_Range)
	{
		m_Origin = a_Origin;
		m_Direction = a_Direction;
		m_Range = a_Range;
	}
};

struct Plane
{
	//A point on the plane
	Vector4 m_Point;
	Vector4 m_FacingDir;
};

bool LineLine(Line a_Line1, Line a_Line2, Vector2& a_rcCrossPoint)
{
	float top1 = (( (a_Line2.end.x - a_Line2.start.x) * (a_Line1.start.y - a_Line2.start.y) ) - ( (a_Line2.end.y - a_Line2.start.y) * (a_Line1.start.x - a_Line2.start.x) ));
	float top2 = (( (a_Line1.end.x - a_Line1.start.x) * (a_Line1.start.y - a_Line2.start.y) ) - ( (a_Line1.end.y - a_Line1.start.y) * (a_Line1.start.x - a_Line2.start.x) ));
	float bottom = (( (a_Line2.end.y - a_Line2.start.y) * (a_Line1.end.x - a_Line1.start.x) ) - (a_Line2.end.x - a_Line2.start.x) * (a_Line1.end.y - a_Line1.start.y));

	float Ta = top1 / bottom;
	float Tb = top2 / bottom;

	if (Ta == 0 && Tb == 0)
	{
		//Both zero - lines are parallel, never cross
		a_rcCrossPoint = Vector2(0, 0);
		return false;
	}
	else if (Ta > 0 && Ta <= 1 && Tb > 0 && Tb <= 1)
	{
		Vector2 a = a_Line1.GetPointOnLine(Ta);
		Vector2 b = a_Line2.GetPointOnLine(Tb);

		std::cout << "A: " << a << " B: " << b << std::endl;
		return true;
	}
}

void main()
{
	Line a = Line(100, 100, 200, 200);
	Line b = Line(200, 100, 100, 200);
	Vector2 crossPoint = Vector2();

	LineLine(a, b, crossPoint);

	system("pause");
}
