#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#include <math.h>
#include <iostream>

const double MATH_PI = 3.14159265359;

//Multiply to get conversion
const float Rad2Deg = 0.01745329251994f;
const float Deg2Rad = 57.2957795130823f;

#endif