#include <iostream>
#include <time.h>

void Add(int a, int b, int& result)
{
	result = a + b;
}

void Swap(int& first, int& second)
{
	int temp = first;
	first = second;
	second = temp;
}

void Sort(int* a_iArray, int a_iArraySize)
{
	for (int i = 0; i < a_iArraySize; ++i)
	{
		for (int j = i + 1; j < a_iArraySize; ++j)
		{
			if (a_iArray[i] < a_iArray[j])
			{
				Swap(a_iArray[i], a_iArray[j]);
			}
		}
	}
}

void Garble(char* a_iArray)
{
	for (int i = 0; i < strlen(a_iArray); ++i)
	{
		int temp = a_iArray[i];
		int change = a_iArray[i] + 10;
		a_iArray[0] = 'a';//(char)(change);

		if (a_iArray[i] <= 65)
		{
			a_iArray[i] = 90 - temp;
		}

		if (a_iArray[i] >= 90)
		{
			a_iArray[i] = 65 + temp;
		}
	}
}

void main()
{
	srand((unsigned int)(time(NULL)));

	int arraySize = 10;
	int* numArray = new int[arraySize];
	for (int i = 0; i < arraySize; ++i)
	{
		numArray[i] = rand() % 100;
	}

	Sort(numArray, arraySize);

	for (int i = 0; i < arraySize; ++i)
	{
		std::cout << numArray[i] << std::endl;
	}

	char* text = "hello";
	//Garble(text);

	for (int i = 0; i < strlen(text); ++i)
	{
		std::cout << text[i] << std::endl;
	}

	system("pause");
}