﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TestGame
{
    public class Sprite
    {
        public Sprite(Texture2D a_Texture, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour)
        {
            m_Texture = a_Texture;
            m_Position = a_Position;
            m_Dimensions = a_Dimensions;
            m_Colour = a_Colour;
        }

        public virtual void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            a_SB.Draw(m_Texture, SpriteRect, m_Colour);
        }

        public Rectangle SpriteRect
        {
            get { return new Rectangle((int)(m_Position.X), (int)(m_Position.Y), (int)(m_Dimensions.X), (int)(m_Dimensions.Y)); }
        }

        public Vector2 Position
        {
            get { return m_Position; }
            set { m_Position = value; }
        }

        public Vector2 Dimensions
        {
            get { return m_Dimensions; }
            set { m_Dimensions = value; }
        }

        protected Color m_Colour;
        protected Texture2D m_Texture;
        protected Vector2 m_Position;
        protected Vector2 m_Dimensions;
    }
}
