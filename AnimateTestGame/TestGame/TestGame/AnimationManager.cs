﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestGame
{
    public class AnimationManager
    {
        List<AnimatedSprite> animations = new List<AnimatedSprite>();

        public AnimationManager()
        {
        }

        public void AddAnimation(AnimatedSprite animation)
        {
            animations.Add(animation);
        }

        public void PauseAnimation(string name)
        {
            foreach (AnimatedSprite n in animations)
            {
                if (n.Name == name)
                {
                    n.IsPlaying = false;
                }
            }
        }

        public bool GetAnimation(string name, ref AnimatedSprite returned)
        {
            foreach (AnimatedSprite n in animations)
            {
                if (n.Name == name)
                {
                    returned = n;
                    return true;
                }
            }
            return false;
        }
    }
}
