﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TestGame
{
    public class AnimatedSprite : Sprite
    {
        public AnimatedSprite(string a_Name, Texture2D a_Texture, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour, bool a_IsPlaying = true)
            : base(a_Texture, a_Position, a_Dimensions, a_Colour)
        {
            frameTimer = maxFrameTimer;
            frameCount = 0;
            m_Name = a_Name;
            m_IsPlaying = a_IsPlaying;
        }

        public void Update(GameTime a_GT)
        {
            if (m_IsPlaying)
            {
                frameTimer += (float)(a_GT.ElapsedGameTime.TotalSeconds);
                if (frameTimer > maxFrameTimer)
                {
                    frameTimer = 0;
                    frameCount++;
                }

                if (frameCount > maxFrameCount)
                {
                    frameCount = 0;
                }
            }
        }

        public override void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            //sprite rect = rect for sprite to be drawn to - position in screen coord
            a_SB.Draw(m_Texture,
                          SpriteRect,
                          new Rectangle(frameCount * 32, 32, (int)(m_Dimensions.Y), (int)(m_Dimensions.Y)),
                          m_Colour);
        }

        public string Name
        {
            get { return m_Name; }
        }

        public bool IsPlaying
        {
            get { return m_IsPlaying; }
            set { m_IsPlaying = value; }
        }

        private bool m_IsPlaying;

        private float frameTimer;
        
        //time for each frame
        private float maxFrameTimer = 0.1f;

        private int frameCount;

        //max num of frames
        private int maxFrameCount = 3;

        private string m_Name;
    }
}
