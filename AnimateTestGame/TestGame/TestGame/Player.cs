﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TestGame
{
    //
    public class Player : Sprite
    {
        public Player(Texture2D a_Texture, Vector2 a_Position, Vector2 a_Direction, Color a_Colour, Keys[] a_Keys, float a_MovementSpeed)
            : base(a_Texture, a_Position, new Vector2(32, 32), a_Colour)
        {
            m_Keys = a_Keys;
            m_MovementSpeed = a_MovementSpeed;
            isAlive = true;
            animations.AddAnimation(new AnimatedSprite("WALK", a_Texture, m_Position, new Vector2(32, 32), Color.White));
            animations.GetAnimation("WALK", ref currentAnimation);
            currentAnimation.Position = m_Position;
        }

        public void Update(GameTime a_GT)
        {
            currentState = Keyboard.GetState();
            if (IsAlive)
            {
                //w
                if (Keyboard.GetState().IsKeyDown(m_Keys[0]))
                {
                    m_Direction += new Vector2(0, -1);
                }

                //s
                if (Keyboard.GetState().IsKeyDown(m_Keys[1]))
                {
                    m_Direction += new Vector2(0, 1);
                }

                //a
                if (Keyboard.GetState().IsKeyDown(m_Keys[2]))
                {
                    m_Direction += new Vector2(-1, 0);
                }

                //d
                if (Keyboard.GetState().IsKeyDown(m_Keys[3]))
                {
                    m_Direction += new Vector2(1, 0);
                }

                //space
                if (Keyboard.GetState().IsKeyDown(m_Keys[4]) && currentState != prevState)
                {
                    currentAnimation.IsPlaying = !currentAnimation.IsPlaying;
                }

                if (Keyboard.GetState().GetPressedKeys().Length == 0)
                {
                    if (currentState.IsKeyUp(m_Keys[0]) || currentState.IsKeyUp(m_Keys[1]) ||
                        currentState.IsKeyUp(m_Keys[2]) || currentState.IsKeyUp(m_Keys[3]))
                    {
                        m_Direction = Vector2.Zero;
                    }
                }

                if (m_Direction.Length() > 0)
                {
                    m_Direction.Normalize();
                }

                currentAnimation.Position += m_Direction * m_MovementSpeed * (float)a_GT.ElapsedGameTime.TotalMilliseconds;

                if (currentAnimation != null)
                {
                    currentAnimation.Update(a_GT);
                }
            }
            prevState = currentState;
        }

        public override void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            if (IsAlive)
            {
                currentAnimation.Draw(a_SB, a_SF);
            }
        }

        public bool IsAlive
        {
            get { return isAlive; }
        }

        AnimationManager animations = new AnimationManager();
        AnimatedSprite currentAnimation;
        KeyboardState currentState;
        KeyboardState prevState;
        Vector2 m_Direction;
        Keys[] m_Keys;
        bool isAlive;
        float m_MovementSpeed;
    }
}
