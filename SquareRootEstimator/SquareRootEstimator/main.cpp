#include <iostream>

float heronSqrt(float num, float precision = 0.0001f)
{
	//first known is that
	//square root of zero
	//is zero
	if (num <= 0)
	{
		return 0;
	}

	//represents an estimate
	//of the true value
	float guess = 0;

	//represents the last
	//attempt at guessing
	float lastGuess = 0;

	//divide by some number 
	//to start the process
	//off
	guess = num / 3.0f;

	std::cout << "Number passed in: " << num << std::endl << std::endl << "Initial guess: " << guess << std::endl;

	//keep looping until
	//the two guesses are
	//within the precision
	//passed in
	while (true)
	{
		lastGuess = guess;

		//num / guess replicates the previous
		//estimate. 
		//take the average between the previous
		//estimate and the current estimate
		//if x is initial guess and e is the error
		//sqrt(S) can be derived by solving for x
		//S = (x + e) ^ 2 repeatedly until the
		//error is within a certain range
		//x = (x + (S / x)) / 2
		guess = (guess + (num / guess)) * 0.5f;

		std::cout << std::endl << "Progressive guess: " << guess << std::endl;

		//check similarity between the two
		//if its within the precision check
		//then found the right answer
		//absolute value decreases number
		//of checks to one
		if (abs(lastGuess - guess) <= precision)
		{
			break;
		}
	}
	return guess;
}

//square root is a function that finds 
//a number such that the number squared 
//returns the argument passed into the
//function
void main()
{
	std::cout << std::endl << "GUESS IS: " << std::endl << heronSqrt(4646) << std::endl << std::endl;

	system("pause");
}
