#ifndef _MATRIX3_H
#define _MATRIX3_H

#include "Constants.h"
#include "Vector3.h"
#include "Vector4.h"

class mat3
{
public:
	mat3();
	mat3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22);
	mat3(Vector3 a1, Vector3 a2, Vector3 a3);

	mat3 identity();

	//Modifies current matrix
	//But also returns transpose
	mat3& transpose();

	//Calcs transpose of this
	//Returning new mat3
	mat3 getTranspose();

	float* ToArray();

	//Changes the current matrix to be a particular matrix
	mat3& makeRotation(float a_rads);
	mat3& makeScaling(const Vector3& a_Scale);
	mat3& makeTranslation(const Vector3& a_Translation);

	//Generate new matrix and return it
	static mat3 setupRotation(float a_Rads);
	static mat3 setupScaling(const Vector3& a_Scale);
	static mat3 setupTranslation(const Vector3& a_Translation);

	mat3 operator+(const mat3& a);
	mat3 operator-(const mat3& a);
	mat3 operator*(const mat3& a);
	mat3& operator=(const mat3& a);

	mat3& operator+=(const mat3& a);
	mat3& operator-=(const mat3& a);
	mat3& operator*=(const mat3& a);	

	//mat3 x vec3 = vec3
	//treat vec3 as 3 x 1 mat
	Vector3 operator*(const Vector3& a);
	
	friend std::ostream& operator<<(std::ostream& output, const mat3& a_Other) 
	{
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				output << a_Other.m[i][j] << " ";
			}
			output << std::endl;
		}
		return output;
	}

	float m[3][3];
};

#endif