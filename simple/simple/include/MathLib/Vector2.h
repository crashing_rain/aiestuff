#ifndef _VECTOR2_H
#define _VECTOR2_H

#include "Constants.h"

class Vector2
{
public:
	Vector2();
	Vector2(float a_X, float a_Y);
	Vector2(float a_XY);
	Vector2(const Vector2& a_Other);

	Vector2& operator=(const Vector2& a_Other);
	Vector2 operator+(const Vector2& a_Other);
	Vector2 operator-(const Vector2& a_Other);

	//Calculate perpendicular of current vector
	Vector2 Perp();

	Vector2 operator*(float a_Scalar);

	void Normalise();

	Vector2 GetNormal();

	float* ToArray();

	//Dot prod
	float Dot(const Vector2& a_Other);

	//Magnitude - distance from origin
	float SquaredMagnitude();

	float Magnitude();

	//Not square root - dist between two points
	float SquaredDistance(const Vector2& a_Other);

	//Square root
	float Distance(const Vector2& a_Other);

	float AngleBetween(const Vector2& a_Other, bool inRadians = true /*= true*/);

	bool operator==(const Vector2& a_Other);
	Vector2& operator+=(const Vector2& a_Other);
	Vector2& operator-=(const Vector2& a_Other);

	friend std::ostream& operator<<(std::ostream &output, const Vector2& a_Vector)
	{
		output << "X: " << a_Vector.x << " Y: " << a_Vector.y;
		return output;
	}

	float x,y;
};

#endif