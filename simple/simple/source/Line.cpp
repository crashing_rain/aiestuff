#include "Line.h"

Line::Line(const Vector4& a_Centre, unsigned int a_ShaderProgram)
{
	centre = Vector4(0,0,0,1);
	shaderProgram = a_ShaderProgram;

	pointVBO = 0;
	glGenBuffers(1, &pointVBO);

	if (pointVBO != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, pointVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vector4), centre.ToArray(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //Pos

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vector4), 0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		std::cout << "ERROR SETTING UP VBO" << std::endl;
	}

	glUseProgram(shaderProgram);

	projMatID = glGetUniformLocation(shaderProgram, "projection");
	modelMatID = glGetUniformLocation(shaderProgram, "model");

	translation = mat4::setupTranslation(Vector3(a_Centre.x, a_Centre.y, 0));
	localTransform = translation * rotation * scale;

	glUseProgram(0);
}

Line::~Line()
{
	glDeleteBuffers(1, &pointVBO);
}

void Line::Update(float dt)
{
	translation = mat4::setupTranslation( Vector3(translation.m[0][3], translation.m[1][3], translation.m[2][3]) );
}

void Line::Render()
{
	glUseProgram(shaderProgram);

	glUniformMatrix4fv(projMatID, 1, GL_FALSE, mat4::getOrtho(0, Dimensions::GetInstance()->GetDimensions().x, 0, Dimensions::GetInstance()->GetDimensions().y, -1, 1).toArray());
	glUniformMatrix4fv(modelMatID, 1, GL_TRUE, globalTransform.toArray());

	glBindBuffer(GL_ARRAY_BUFFER, pointVBO);

	glDrawArrays(GL_LINES, 0, 1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
}
