#ifndef _PROFILER_H
#define _PROFILER_H

#include "Timer.h"
#include <string>

class Profile
{
public:
	Profile(const char* a_Name)
	{
		profileTimer = Timer();
		name = a_Name;
	}

	const char* GetName()
	{
		return name.c_str();
	}

	void StartProfile()
	{
		profileTimer.SetStartTime();
	}

	void FinishProfile()
	{
		profileTimer.SetEndTime();
		profileTimer.PrintTimers(name.c_str());
	}

	Timer GetTimer()
	{
		return profileTimer;
	}

private:
	Timer profileTimer;
	std::string name;
};

#endif