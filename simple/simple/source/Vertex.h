#ifndef _VERTEX_H
#define _VERTEX_H

#include "MathLib\Vector4.h"
#include "MathLib\Vector2.h"

struct Vertex
{
	Vector4 position;
	Vector4 colour;
	Vector2 coord;
};

#endif