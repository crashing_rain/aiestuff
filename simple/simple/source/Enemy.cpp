#include "Enemy.h"

void Enemy::Update(float dt)
{


	Sprite::Update(dt);
}

void Enemy::Render()
{
	localTransform = translation * rotation * scale;
	Sprite::Render();
}