#include "SceneNode.h"

unsigned int SceneNode::nodeID = 0;

SceneNode::SceneNode()
{
	parent = nullptr;
	localTransform = mat4();
	globalTransform = mat4();
	nodeID++;
}

SceneNode::~SceneNode()
{
	if (parent != nullptr)
	{
		delete parent;
		parent = nullptr;
	}
	children.clear();
}

//Checks if parent is valid
//If already has parent, then removes
//from parent's vector
//and sets the parent to be the current node
void SceneNode::SetParent(SceneNode* a_Parent)
{
	if (parent != nullptr)
	{
		parent->RemoveChild(this);
	}
	parent = a_Parent;
}

//If a child is valid
//If it has no parent, then make this current node
//the parent and add it to its children
//Note that if the child already has a parent then
//it will add the child regardless
void SceneNode::AddChild(SceneNode* child)
{
	//child is valid
	if (child != nullptr)
	{
		//child has no parent
		if (child->parent == nullptr)
		{
			//set child's parent to this
			child->parent = this;
		}
		//add child to vector
		children.push_back(child);
	}
}

//Looks through vector of children and sees if one 
//is equal to another then erases it from the vector
void SceneNode::RemoveChild(SceneNode* child)
{
	if (child != nullptr)
	{
		for (unsigned int i = 0; i < children.size(); ++i)
		{
			if ( *(children.begin() + i) == child)
			{
				children.erase(children.begin() + i);
				break;
			}
		}
	}
}

//To be overwritten by child classes
void SceneNode::Update(float dt) { }

//To be overwritten by child classes
void SceneNode::Render() { }

//Calls update all the children in its own tree
//First step calling update on itself, then looping through
//and recursively calling it on all its children
void SceneNode::UpdateTree(float dt)
{
	this->Update(dt);

	for (unsigned int i = 0; i < children.size(); ++i)
	{
		children[i]->UpdateTree(dt);
	}
}

//Calls render on all the children in its own tree
//First step is to update all its transforms
//Then do the same for all its children, before
//its children render themselves. The parent is 
//rendered last
void SceneNode::RenderTree()
{
	this->UpdateTransforms();

	for (unsigned int i = 0; i < children.size(); ++i)
	{
		children[i]->RenderTree();
	}

	this->Render();
}

//Calculates the correct transform for the current node
//dependent on if it has a parent or not
void SceneNode::UpdateTransforms()
{
	if (parent != nullptr)
	{
		globalTransform = parent->globalTransform * localTransform;
	}
	else
	{
		globalTransform = localTransform;
	}
}

void SceneNode::SetLocalTransform(const mat4& a_LocalTransform)
{
	localTransform = a_LocalTransform;
}

void SceneNode::SetGlobalTransform(const mat4& a_GlobalTransform)
{

}
