#include <GLEW\glew.h>
#include <GLEW\wglew.h>
#include <GLFW\glfw3.h>

#include <iostream>
#include <vector>
#include <time.h>

#include "Scene.h"
#include "LegacyProfileManager.h"
#include "Dimensions.h"
#include "Point.h"
#include "Rect.h"
#include "Sprite.h"
#include "Camera.h"
#include "MovingBackground.h"
#include "PostRectangle.h"
#include "Player.h"
#include "MathLib\Constants.h"
#include "MathLib\Bezier.h"
#include "Line.h"

//////////////////////////////////////////////////////////////////
//																//
//			SCALE * ROTATION * TRANSLATION						//
//																//
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
//																//
//					POSITION - w = 1							//
//																//
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
//																//
//					ORIGIN BOTTOM LEFT							//
//																//
//////////////////////////////////////////////////////////////////

int main()
{
	Dimensions::GetInstance()->SetDimensions(1280, 720);

	bool quit = false;
	srand( (unsigned int)(time(NULL)) );

	if (!glfwInit())
	{
		printf("FAILED TO INIT GLFW");
		return -1;
	}
	
	GLFWwindow* window = glfwCreateWindow(Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y, "Test project", nullptr, nullptr);

	if (!window)
	{
		printf("FAILED TO CREATE WINDOW INSTANCE");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK)
	{
		printf("OPENGL COULDN'T START UP");
		glfwTerminate();
		return -1;
	}
	
	double simulationTime = 0;

	unsigned int uiNormalShaderProgram = CreateProgram("resources/normal.vert", "resources/normal.frag");
	unsigned int uiTexturedShaderProgram = CreateProgram("resources/textured.vert", "resources/textured.frag");
	unsigned int uiPostShaderProgram = CreateProgram("resources/post.vert", "resources/post.frag");

	PostRectangle postRect = PostRectangle(uiPostShaderProgram);

	Scene* mainScene = new Scene();

	SceneNode* background = new Sprite("resources/texture6.jpg", Vector2(Dimensions::GetInstance()->GetDimensions().x * 0.5f, Dimensions::GetInstance()->GetDimensions().y * 0.5f),
															Vector2(Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y),
															Vector4(1),
															uiTexturedShaderProgram);

	SceneNode* koala2 = new Sprite("resources/texture5.jpg", Vector2(20, -1), Vector2(10, 5), Vector4(1), uiTexturedShaderProgram);
	SceneNode* koala3 = new Sprite("resources/texture5.jpg", Vector2(-20, -1), Vector2(10, 5), Vector4(1), uiTexturedShaderProgram);
	SceneNode* test = new Player("resources/texture2.jpg", Vector4(450, 300, 5, 5), Vector4(1), 250, 250, uiTexturedShaderProgram);

	//background, player - camera - turret

	test->AddChild(koala2);
	test->AddChild(koala3);
	mainScene->GetRootNode()->AddChild(test);

//	glfwGetWindowSize(window, &((int)(Dimensions::GetInstance()->dimensions.x)), ((int)(Dimensions::GetInstance()->GetDimensions().y)));

	while (!glfwWindowShouldClose(window) && !quit)
	{
		Timer::GetInstance()->SetStartTime();

		//Clearing the screen
		glClearColor(0.25f, 0.25f, 0.25f, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		double realTime = Timer::GetInstance()->GetTotalTime();

		//update loop
		while (simulationTime < realTime)
		{
			simulationTime += (1 / (float)(60));

			//Update scene
			float t = Timer::GetInstance()->GetElapsedTime();
			mainScene->UpdateScene(Timer::GetInstance()->GetElapsedTime());
			(dynamic_cast<Sprite*>(background))->Update(Timer::GetInstance()->GetElapsedTime());
			(dynamic_cast<Sprite*>(background))->UpdateTransforms();

			if (glfwGetKey(window, GLFW_KEY_ESCAPE))
			{
				quit = true;
			}
		}

		postRect.BindFramebuffer();

		(dynamic_cast<Sprite*>(background))->Render();

		postRect.UnbindFramebuffer();
		postRect.Render();

		mainScene->RenderScene();

		glfwSwapBuffers(window);
		glfwPollEvents();

		Timer::GetInstance()->SetEndTime();
		Timer::GetInstance()->Tick();
	}

	glfwTerminate();

	glDeleteProgram(uiNormalShaderProgram);
	glDeleteProgram(uiTexturedShaderProgram);
	glDeleteProgram(uiPostShaderProgram);

	return 0;
}
