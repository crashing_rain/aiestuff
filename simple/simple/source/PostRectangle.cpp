#include "PostRectangle.h"

PostRectangle::PostRectangle(unsigned int a_ShaderProgram)
{
	shaderProgram = a_ShaderProgram;

	float quadVertices[] = 
	{
		-1, -1, 0, 1,
		-1, 1, 0, 1,
		1, 1, 0, 1,
		1, -1, 0, 1,
	};

	unsigned int quadIBO[] =
	{
		0, 1, 2,
		2, 0, 3
	};

	//passing quad details to gfx card
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 16, quadVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, quadIBO, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//setting up render buffer object
	glGenRenderbuffers(1, &m_RBO);
	glBindRenderbuffer(GL_RENDERBUFFER, m_RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, (GLsizei)(Dimensions::GetInstance()->GetDimensions().x),(GLsizei)(Dimensions::GetInstance()->GetDimensions().y));
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	//setting up frame buffer texture
	glGenTextures(1, &m_FBT);
	glBindTexture(GL_TEXTURE_2D, m_FBT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, (GLsizei)(Dimensions::GetInstance()->GetDimensions().x), 
											   (GLsizei)(Dimensions::GetInstance()->GetDimensions().y),
											   0,
											   GL_RGBA,
											   GL_UNSIGNED_BYTE,
											   0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	//setting up frame buffer object
	glGenFramebuffers(1, &m_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_FBT, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_RBO);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("ERROR CREATING FRAME BUFFER!\n");
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glUseProgram(shaderProgram);
	diffuseTextureID = glGetUniformLocation(shaderProgram, "diffuseTexture");
	glUniform1i(diffuseTextureID, 0);

	dimensionsID = glGetUniformLocation(shaderProgram, "dimensions");
	glUniform2i(dimensionsID, Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y);

	totalTimeID = glGetUniformLocation(shaderProgram, "totalTime");
	glUniform1f(totalTimeID, Timer::GetInstance()->GetTotalTime());
	glUseProgram(0);
}

PostRectangle::~PostRectangle()
{
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_RBO);
	glDeleteBuffers(1, &m_FBT);
	glDeleteBuffers(1, &m_FBO);
	glDeleteProgram(shaderProgram);
}

void PostRectangle::BindFramebuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

	//viewport is across the whole screen
	glViewport(0, 0, Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void PostRectangle::UnbindFramebuffer()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//viewport is across the whole screen
	glViewport(0, 0, Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void PostRectangle::Render()
{
	glUseProgram(shaderProgram);

	//binding frame buffer texture to element number 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_FBT);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, NULL);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
}
