#ifndef _TEXTURE_MANAGER_H
#define _TEXTURE_MANAGER_H

#include <map>
#include <string>
#include <iostream>

#include "SOIL.h"

class TextureManager
{
public:
	static TextureManager* GetInstance()
	{
		if (instance == nullptr)
		{
			instance = new TextureManager();
		}
		return instance;
	}

	//returns false if it fails to insert texture
	//sets texture handle if it already exists in map
	bool Insert(const char* a_szTextureFileName, int* textureHandle)
	{
		//found texture already in map, so don't load it
		if (textureMap.find(a_szTextureFileName) != textureMap.end())
		{
			*textureHandle = textureMap[a_szTextureFileName];
			return false;
		}

		int errorNum = -1;
		int textureID, textureWidth, textureHeight, BPP;
		if (a_szTextureFileName != nullptr)
		{
			unsigned char* imageData = SOIL_load_image(a_szTextureFileName, 
										&textureWidth, 
										&textureHeight, 
										&BPP,
										SOIL_LOAD_AUTO);
			if (imageData)
			{
				textureID = SOIL_create_OGL_texture(imageData, textureWidth, textureHeight, BPP,
					SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
				SOIL_free_image_data(imageData);
			}

			if (textureID == 0)
			{
				std::cerr << "SOIL LOADING ERROR: " << SOIL_last_result() << std::endl;
				return false;
			}

			//texture doesnt exist in map yet, so load it
			if (textureMap.find(a_szTextureFileName) == textureMap.end())
			{
				textureMap[a_szTextureFileName] = textureID;
				*textureHandle = textureID;
				return true;
			}
		}
		return false;
	}

	unsigned int Find(const char* a_szTextureFileName)
	{
		if (textureMap.find(a_szTextureFileName) != textureMap.end())
		{
			return textureMap[a_szTextureFileName];
		}
		std::cout << "TEXTURE NOT FOUND" << std::endl;
		return -1;
	}

	unsigned int MapSize()
	{
		return textureMap.size();
	}

	bool IsEmpty()
	{
		return textureMap.empty();
	}

	void Clear()
	{
		textureMap.clear();
	}

private:
	std::map<const char*, int> textureMap;
	TextureManager() {}

	static TextureManager* instance;
};

#endif