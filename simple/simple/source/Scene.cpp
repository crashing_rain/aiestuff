#include "Scene.h"

Scene::Scene()
{
	rootNode = new SceneNode();
}

Scene::~Scene()
{
	delete rootNode;
}

void Scene::UpdateScene(float dt)
{
	if (rootNode != nullptr)
	{
		rootNode->UpdateTree(dt);
	}
}

void Scene::RenderScene()
{
	if (rootNode != nullptr)
	{
		rootNode->RenderTree();
	}
}
