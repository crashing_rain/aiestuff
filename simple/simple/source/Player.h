#ifndef _PLAYER_H
#define _PLAYER_H

#include "Sprite.h"

class Player : public Sprite
{
public:
	Player(const char* a_FileName, const Vector4& positionDimensions, const Vector4& a_Colour, float a_MovementSpeed, float a_RotationSpeed, unsigned int a_ShaderProgram)
		: Sprite(a_FileName, 
				 Vector2(positionDimensions.x, positionDimensions.y), 
				 Vector2(positionDimensions.z, positionDimensions.w),
				 a_Colour,
				 a_ShaderProgram)
	{
		movementSpeed = a_MovementSpeed;
		rotationSpeed = a_RotationSpeed;
		direction = Vector2(1,1);
	}

	Vector2 direction;

	virtual void Update(float dt);
	//virtual void Render();
private:
	float movementSpeed;
	float rotationSpeed;
};

#endif