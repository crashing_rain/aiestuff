#include "Sprite.h"

Sprite::Sprite(const char* fileName, const Vector2& a_Centre, const Vector2& dimensions, const Vector4& colours, unsigned int a_ShaderProgram)
{
	if (fileName != nullptr)
	{
		TextureManager::GetInstance()->Insert(fileName, &textureID);
	}

	centre = a_Centre;

	shaderProgram = a_ShaderProgram;

	width = dimensions.x;
	height = dimensions.y;

	//setting up vertices
	quad = new Vertex[4];

	//centre is the middle of the quad, from which
	//all the points are referenced - should never change

	quad[0].position = Vector4(-1,-1,0,1);
	quad[1].position = Vector4(-1,1,0,1);
	quad[2].position = Vector4(1,1,0,1);
	quad[3].position = Vector4(1,-1,0,1);

	quad[0].coord = Vector2(0);
	quad[1].coord = Vector2(0,1);
	quad[2].coord = Vector2(1,1);
	quad[3].coord = Vector2(1,0);

	for (int i = 0; i < 4; ++i)
	{
		quad[i].colour = colours;
	}

	//setting up buffers
	quadVBO = quadIBO = 0;
	glGenBuffers(1, &quadVBO);
	glGenBuffers(1, &quadIBO);

	if (quadVBO != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, quad, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //Pos
		glEnableVertexAttribArray(1); //Colours
		glEnableVertexAttribArray(2); //UVs

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)(sizeof(float) * 4));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)((sizeof(float) * 4) + (sizeof(float) * 4)));

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		std::cout << "ERROR SETTING UP VBO" << std::endl;
	}

	if (quadIBO != 0)
	{
		unsigned int indices[6] = 
		{
			0, 1, 2,
			2, 0, 3,
		};

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadIBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indices, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	else
	{
		std::cout << "ERROR SETTING UP IBO" << std::endl;
	}

	glUseProgram(shaderProgram);
	diffuseTextureID = glGetUniformLocation(shaderProgram, "diffuseTexture");
	projMatID = glGetUniformLocation(shaderProgram, "projection");
	modelMatID = glGetUniformLocation(shaderProgram, "model");

	translation = mat4::setupTranslation(Vector3(centre.x, centre.y, 0));

	//scale is measurement outwards from the centre
	scale = mat4::setupScaling(Vector3( width, height, 0));
	glUseProgram(0);
	//Pass in other numbers in gluniform1i for more slots in sampler2d shader
}

Sprite::~Sprite()
{
	delete [] quad;
	glDeleteBuffers(1, &quadIBO);
	glDeleteBuffers(1, &quadVBO);
	glDeleteShader(shaderProgram);
}

void Sprite::Update(float dt)
{
	localTransform = translation * rotation * scale;
}

void Sprite::Render()
{
	glUseProgram(shaderProgram);

	glUniform1i(diffuseTextureID, 0);
	glUniformMatrix4fv(projMatID, 1, GL_FALSE, mat4::getOrtho(0, Dimensions::GetInstance()->GetDimensions().x, 0, Dimensions::GetInstance()->GetDimensions().y, -1, 1).toArray());
	glUniformMatrix4fv(modelMatID, 1, GL_TRUE, globalTransform.toArray());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadIBO);
	glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, NULL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}
