#ifndef _LEGACY_PROFILE_MANAGER_H
#define _LEGACY_PROFILE_MANAGER_H

#include "LegacyTimer.h"
#include <vector>

class LegacyProfileManager
{
public:
	LegacyProfileManager()
	{
	}

	void StartProfile(std::string a_Name)
	{
		for (unsigned int i = 0; i < legacyProfiles.size(); ++i)
		{
			if ( (legacyProfiles.begin() + i)->GetName() == a_Name)
			{
				(legacyProfiles.begin() + i)->StartTimer();
				return;
			}
		}

		LegacyTimer temp = LegacyTimer(a_Name);
		temp.StartTimer();
		legacyProfiles.push_back(temp);
	}

	void FinishProfile(std::string a_Name)
	{
		for (unsigned int i = 0; i < legacyProfiles.size(); ++i)
		{
			if ( (legacyProfiles.begin() + i)->GetName() == a_Name )
			{
				(legacyProfiles.begin() + i)->EndTimer();
				return;
			}
		}
	}

	//Tracks how long the entire game takes, update and draw together
	double GetTotalTime(std::string a_Name)
	{
		if (legacyProfiles.size() == 0)
		{
			std::cout << "NOTHING STORED" << std::endl;
			return 0;
		}
		for (unsigned int i = 0; i < legacyProfiles.size(); ++i)
		{
			if ( (legacyProfiles.begin() + i)->GetName() == a_Name )
			{
				return (legacyProfiles.begin() + i)->GetTotalTime();
			}
		}
		std::cout << "PROFILE NOT FOUND" << std::endl;
		return 0;
	}

	void RemoveTimer(std::string a_Name)
	{
		if (legacyProfiles.size() == 0)
		{
			std::cout << "NOTHING STORED" << std::endl;
			return;
		}

		for (unsigned int i = 0; i < legacyProfiles.size(); ++i)
		{
			if ( (legacyProfiles.begin() + i)->GetName() == a_Name )
			{
				legacyProfiles.erase(legacyProfiles.begin() + i);
				return;
			}
		}
		std::cout << "NAME NOT FOUND" << std::endl;
	}
private:
	std::vector<LegacyTimer> legacyProfiles;
};

#endif