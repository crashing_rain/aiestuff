#include "Timer.h"
#include <string>

using namespace std::chrono;

Timer* Timer::instance = nullptr;

Timer::Timer()
{
	elapsedTime = 0;
	totalTime = 0;

	startTime = high_resolution_clock::now();
	endTime = high_resolution_clock::now();
	currentTime = high_resolution_clock::now();
	previousTime = high_resolution_clock::now();
}

Timer* Timer::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Timer();
	}
	return instance;
}

void Timer::SetStartTime()
{
	startTime = high_resolution_clock::now();
}

void Timer::SetEndTime()
{
	endTime = high_resolution_clock::now();
}

void Timer::Tick()
{
	previousTime = currentTime;
	currentTime = high_resolution_clock::now();
	elapsedTime = duration_cast<duration<double>>(endTime - startTime).count();
	totalTime += elapsedTime;
}

void Timer::PrintTimers(const char* a_Name)
{
	if (a_Name != nullptr)
	{
		std::cout << a_Name << " TOOK " << duration_cast<duration<double>>(endTime - startTime).count() << " SECONDS." << std::endl;
	}
	else
	{
		std::cout << "ELAPSED TIME: " << elapsedTime << std::endl;
		std::cout << "TOTAL TIME: " << totalTime << std::endl;
	}
}

double Timer::GetElapsedTime()
{
	return elapsedTime;
}

double Timer::GetTotalTime()
{
	return totalTime;
}