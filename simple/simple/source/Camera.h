#ifndef _CAMERA_H
#define _CAMERA_H

#include "GLEW\glew.h"
#include "SceneNode.h"
#include "Dimensions.h"

class Camera : public SceneNode
{
public:
	Camera();
	~Camera();

	Vector4 targetPosDim;

	virtual void Update(float dt);

private:
	mat4 translation;
	mat4 rotation;
	mat4 scale;

	Vector2 m_Centre;

	//left, top, right, bottom
	Vector4 m_Dimensions;

	unsigned int modelMatID, projMatID, shaderProgram;
	bool isActive;
};

#endif