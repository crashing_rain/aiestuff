#ifndef _DIMENSIONS_H
#define _DIMENSIONS_H

#include "MathLib/Vector2.h"

class Dimensions
{
public:
	static Dimensions* GetInstance()
	{
		if (instance == nullptr)
		{
			instance = new Dimensions();
		}
		return instance;
	}

	void SetDimensions(unsigned int a_Width, unsigned int a_Height)
	{
		dimensions.x = (float)a_Width;
		dimensions.y = (float)a_Height;
	}

	Vector2 GetDimensions()
	{
		return dimensions;
	}

	float* ToArray()
	{
		float* temp = new float[2];
		temp[0] = dimensions.x;
		temp[1] = dimensions.y;
		return temp;
	}

private:
	Dimensions()
	{
		dimensions = Vector2(100);
	}
	static Dimensions* instance;
	Vector2 dimensions;
};

#endif