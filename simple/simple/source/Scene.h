#ifndef _SCENE_H
#define _SCENE_H

#include "SceneNode.h"

class Scene
{
public:
	Scene();
	~Scene();

	void RenderScene();
	void UpdateScene(float dt);
	SceneNode* GetRootNode() { return rootNode; }

private:
	SceneNode* rootNode;
};

#endif
