#ifndef _UTILITY_H
#define _UTILITY_H

#include <GLEW\glew.h>

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

GLuint CreateShader(GLenum a_ShaderType, const char* a_ShaderFileName);

GLuint CreateProgram(const char* a_VertexFileName, const char* a_FragFileName);

#endif