#ifndef _PROFILE_MANAGER_H
#define _PROFILE_MANAGER_H

#include "Profile.h"
#include <vector>
#include <string>
#include <iostream>

class ProfileManager
{
public:
	ProfileManager()
	{
	}

	void StartProfile(std::string a_Name)
	{
		for (unsigned int i = 0; i < profiles.size(); ++i)
		{
			if ( (profiles.begin() + i)->GetName() == a_Name )
			{
				std::cout << "PROFILE NAME ALREADY EXISTS" << std::endl;
				break;
			}
		}

		Profile temp = Profile(a_Name.c_str());
		temp.StartProfile();
		profiles.push_back(temp);
	}

	void FinishProfile(std::string a_Name)
	{
		for (unsigned int i = 0; i < profiles.size(); ++i)
		{
			if ( (profiles.begin() + i)->GetName() == a_Name )
			{
				(profiles.begin() + i)->FinishProfile();
				profiles.erase((profiles.begin() + i));
				return;
			}
		}
	}

	double GetElapsedTime()
	{
		if (profiles.size() == 0)
		{
			return 0;
		}

		return profiles.at(0).GetTimer().GetElapsedTime();
	}

	double GetTotalTime()
	{
		if (profiles.size() == 0)
		{
			return 0;
		}

		return profiles.at(0).GetTimer().GetTotalTime();
	}
private:
	std::vector<Profile> profiles;
};

#endif