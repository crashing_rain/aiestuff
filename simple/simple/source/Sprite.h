#ifndef _SPRITE_H
#define _SPRITE_H

#include <GLEW\glew.h>
#include <GLEW\wglew.h>
#include <GLFW\glfw3.h>

#include "Utility.h"
#include "MathLib\Matrix4.h"

#include "SceneNode.h"
#include "TextureManager.h"
#include "Dimensions.h"
#include "Vertex.h"

class Sprite : public SceneNode
{
public:
	Sprite(const char* fileName, const Vector2& centre, const Vector2& dimensions, const Vector4& colours, unsigned int a_ShaderProgram);
	~Sprite();

	virtual void Update(float dt);
	virtual void Render();

	int textureID;

	void SetPosition(Vector2 a_Pos)
	{
		centre = a_Pos;
	}
protected:
	Sprite() {}
	Vector2 centre;
	mat4 scale;
	mat4 rotation;
	mat4 translation;
	unsigned int modelMatID, projMatID, quadIBO, quadVBO, shaderProgram;
	int width, height;
	Vertex* quad;

	unsigned int diffuseTextureID;
};

#endif