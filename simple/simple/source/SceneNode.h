#ifndef _SCENE_NODE_H
#define _SCENE_NODE_H

#include "MathLib\Matrix4.h"
#include <vector>

//Update function does nothing, it gets overriden when inheriting
//Update tree recurses, calling its own update, then loops through
//and calls its children's update tree
//Update transform does matrix multiplication stuff on itself only

//Similar idea for the draw calls

class SceneNode
{
public:
	SceneNode();
	~SceneNode();

	void SetParent(SceneNode* a_Parent);
	SceneNode* GetParent() { return parent; }

	unsigned int GetNodeID() { return nodeID; }

	void AddChild(SceneNode* child);
	void RemoveChild(SceneNode* child);

	void SetLocalTransform(const mat4& a_LocalTransform);
	void SetGlobalTransform(const mat4& a_GlobalTransform);

	mat4 GetGlobal() { return globalTransform; }	
	mat4 GetLocal() { return localTransform; }

	Vector4 GetPosition() { return Vector4(localTransform.m[0][3], localTransform.m[1][3], 0, 1); }
	Vector4 GetScale() { return Vector4(localTransform.m[0][0], localTransform.m[1][1], localTransform.m[2][2], localTransform.m[3][3]); }

	std::vector<SceneNode*> GetChildren() { return children; }

	Vector3 GetForward()
	{
		//forward vector - Vector3(m[0][1], m[1][1], 0);
		return Vector3(globalTransform.m[1][3], globalTransform.m[2][3], globalTransform.m[3][3]);
	}

	//recursively moves through children
	void UpdateTree(float dt);
	void RenderTree();

	//matrix multiplication
	void UpdateTransforms();

protected:
	//does nothing, gets overriden
	virtual void Update(float dt);
	virtual void Render();

	mat4 localTransform;
	mat4 globalTransform;

private:
	SceneNode* parent;
	std::vector<SceneNode*> children;
	static unsigned int nodeID;
};

#endif
