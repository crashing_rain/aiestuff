#ifndef _TIMER_H
#define _TIMER_H

#include <chrono>
#include <iostream>
class Profile;

class Timer
{
friend class Profile;
public:
	static Timer* GetInstance();
	void Tick();

	double GetElapsedTime();
	double GetTotalTime();

	void SetStartTime();
	void SetEndTime();

	void PrintTimers(const char* a_Name = nullptr);

private:
	Timer();

	std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> endTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> currentTime;
	std::chrono::time_point<std::chrono::high_resolution_clock> previousTime;

	double elapsedTime;
	double totalTime;

	static Timer* instance;
};

#endif