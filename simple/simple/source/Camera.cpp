#include "Camera.h"

Camera::Camera()
{
	if (GetParent() != nullptr)
	{
		m_Centre = Vector2(GetParent()->GetPosition().x + (GetParent()->GetScale().x * 0.5f) - Dimensions::GetInstance()->GetDimensions().x,
						   GetParent()->GetPosition().y + (GetParent()->GetScale().y * 0.5f) - Dimensions::GetInstance()->GetDimensions().y);
	}
	m_Dimensions = Vector4(0, 0, Dimensions::GetInstance()->GetDimensions().x, Dimensions::GetInstance()->GetDimensions().y);
}

void Camera::Update(float dt)
{
	if (m_Centre.x < 0)
	{
		m_Centre.x = 0;
	}

	if (m_Centre.y < 0)
	{
		m_Centre.y = 0;
	}

	if (m_Centre.x > Dimensions::GetInstance()->GetDimensions().x)
	{

	}

	if (m_Centre.y > Dimensions::GetInstance()->GetDimensions().y)
	{

	}

	if (GetParent() != nullptr)
	{
		m_Centre = Vector2(GetParent()->GetPosition().x + (GetParent()->GetScale().x * 0.5f) - Dimensions::GetInstance()->GetDimensions().x,
					GetParent()->GetPosition().y + (GetParent()->GetScale().y * 0.5f) - Dimensions::GetInstance()->GetDimensions().y);

		translation = mat4::setupTranslation(Vector3(m_Centre.x, m_Centre.y, 0));
		localTransform = translation;
	}
}
