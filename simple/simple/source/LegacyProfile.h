#ifndef _LEGACY_PROFILE_H
#define _LEGACY_PROFILE_H

#include "LegacyTimer.h"
#include <string>

class LegacyProfile
{
public:
	LegacyProfile(const char* a_Name)
	{
		profileTimer = LegacyTimer();
		name = a_Name;
	}

	const char* GetName()
	{
		return name.c_str();
	}

	void StartProfile()
	{
		profileTimer.StartTimer();
	}

	void FinishProfile()
	{
		profileTimer.EndTimer();
		profileTimer.PrintTimers(name.c_str());
	}

	LegacyTimer GetTimer()
	{
		return profileTimer;
	}
private:
	LegacyTimer profileTimer;
	std::string name;
};

#endif