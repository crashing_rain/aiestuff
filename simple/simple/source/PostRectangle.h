#ifndef _POST_RECTANGLE_H
#define _POST_RECTANGLE_H

#include <GLEW\glew.h>
#include <GLEW\wglew.h>
#include <GLFW\glfw3.h>

#include "Dimensions.h"
#include "Timer.h"

class PostRectangle
{
public:
	//use own post processing shader
	PostRectangle(unsigned int a_ShaderProgram);
	~PostRectangle();

	void BindFramebuffer();
	void UnbindFramebuffer();
	void Render();
private:
	unsigned int m_VBO, m_IBO, m_RBO, m_FBT, m_FBO, shaderProgram, dimensionsID, diffuseTextureID, totalTimeID;
};

#endif //_RECTANGLE_H