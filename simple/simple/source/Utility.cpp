#include "Utility.h"

GLuint CreateShader(GLenum a_ShaderType, const char* a_ShaderFileName)
{
	std::string strShaderCode;
	std::ifstream shaderStream(a_ShaderFileName);
	if (shaderStream.is_open())
	{
		std::string line = "";
		while (std::getline(shaderStream, line))
		{
			strShaderCode += "\n" + line;
		}
		shaderStream.close();
	}

	char const* shaderSourcePointer = strShaderCode.c_str();
	GLuint uiShader = glCreateShader(a_ShaderType);
	glShaderSource(uiShader, 1, &shaderSourcePointer, nullptr);
	glCompileShader(uiShader);

	GLint iStatus;
	glGetShaderiv(uiShader, GL_COMPILE_STATUS, &iStatus);
	if (iStatus == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(uiShader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(uiShader, infoLogLength, nullptr, strInfoLog);

		const char* strShaderType = nullptr;
		switch (a_ShaderType)
		{
		case GL_VERTEX_SHADER:
			{
				strShaderType = "vertex";
				break;
			}
		case GL_FRAGMENT_SHADER:
			{
				strShaderType = "fragment";
				break;
			}
		}
		fprintf(stderr, "COMPILATION FAILURE IN %s SHADER: \n%s\n", strShaderType, strInfoLog);
		delete [] strInfoLog;
	}
	return uiShader;
}

GLuint CreateProgram(const char* a_Vertex, const char* a_Frag)
{
	std::vector<GLuint> shaderList;
	shaderList.push_back(CreateShader(GL_VERTEX_SHADER, a_Vertex));
	shaderList.push_back(CreateShader(GL_FRAGMENT_SHADER, a_Frag));

	GLuint uiProgram = glCreateProgram();

	for (auto& shader = shaderList.begin(); shader != shaderList.end(); shader++)
	{
		glAttachShader(uiProgram, *shader);
	}

	glLinkProgram(uiProgram);

	GLint status;
	glGetProgramiv(uiProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetProgramiv(uiProgram, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetProgramInfoLog(uiProgram, infoLogLength, nullptr, strInfoLog);
		fprintf(stderr, "LINKER FAILRE: %s\n", strInfoLog);

		delete [] strInfoLog;
	}

	for (auto& shader = shaderList.begin(); shader != shaderList.end(); shader++)
	{
		glDetachShader(uiProgram, *shader);
		glDeleteShader(*shader);
	}

	return uiProgram;
}

//Polling type - good for keys that are held down
//bool CheckKeyPress(GLFWwindow* currentWindow, int a_KeyPressed) //polling type
//{
//	if (glfwGetKey(currentWindow, a_KeyPressed) == GLFW_PRESS)
//	{
//		return true;
//	}
//	return false;
//}

//Callback type - good for individual key press events
/*
Registering function with glfw
glfwSetKeyCallback(window, KeyCallbackFunc);

defining function
static void KeyCallbackFunc(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//event checking
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		//do something
		game.start();
	}
}
*/
