#ifndef _ENEMY_H
#define _ENEMY_H

#include "Sprite.h"

class Enemy : public Sprite
{
public:
	Enemy(const char* a_FileName, const Vector4& positionDimensions, const Vector4& a_Colour, unsigned int a_ShaderProgram)
		: Sprite(a_FileName, Vector2(positionDimensions.x, positionDimensions.y),
							 Vector2(positionDimensions.z, positionDimensions.w),
							 a_Colour,
							 a_ShaderProgram)
	{

	}

	virtual void Update(float dt);
	virtual void Render();
private:
	Vector4 direction;
};

#endif