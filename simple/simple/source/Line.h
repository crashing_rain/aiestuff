#ifndef _LINE_H
#define _LINE_H

#include <GLEW\glew.h>
#include "SceneNode.h"
#include "MathLib\Vector4.h"
#include "Dimensions.h"

class Line : public SceneNode
{
public:
	Line(const Vector4& a_Centre, unsigned int a_ShaderProgram);
	~Line();

	virtual void Update(float dt);
	virtual void Render();
protected:
	Vector4 centre;
	mat4 scale;
	mat4 rotation;
	mat4 translation;
private:
	unsigned int modelMatID, projMatID, pointVBO, shaderProgram;
};

#endif