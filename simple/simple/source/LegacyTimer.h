#ifndef _LEGACY_TIMER_H
#define _LEGACY_TIMER_H

#include <windows.h>
#include <iostream>
#include <string>

class LegacyTimer
{
public:
	LegacyTimer(std::string a_Name);

	void StartTimer();
	void EndTimer();
	void PrintTimers();
	
	std::string GetName();
	double GetTotalTime();
private:
	LARGE_INTEGER startTime;
	LARGE_INTEGER endTime;
	LARGE_INTEGER frequency;

	double totalTime;
	std::string name;
};

#endif