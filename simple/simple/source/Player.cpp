#include "Player.h"

void Player::Update(float dt)
{
	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_W) == GLFW_PRESS)
	{
		centre.y += movementSpeed * dt;
	}

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_S) == GLFW_PRESS)
	{
		centre.y -= movementSpeed * dt;
	}

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_A) == GLFW_PRESS)
	{
		centre.x -= movementSpeed * dt;
	}

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_D) == GLFW_PRESS)
	{
		centre.x += movementSpeed * dt;
	}

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_E) == GLFW_PRESS)
	{
		rotation *= mat4::setupRotationZ(-rotationSpeed * Rad2Deg * dt);
	}

	if (glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_Q) == GLFW_PRESS)
	{
		rotation *= mat4::setupRotationZ(rotationSpeed * Rad2Deg * dt);
	}

	translation = mat4::setupTranslation(Vector3(centre.x, centre.y, 0));
	Sprite::Update(dt);
}

//void Player::Render()
//{
//	glUseProgram(shaderProgram);
//
//	glUniform1i(diffuseTextureID, 0);
//	glUniformMatrix4fv(projMatID, 1, GL_FALSE, mat4::getOrtho(0, Dimensions::GetInstance()->GetDimensions().x, 0, Dimensions::GetInstance()->GetDimensions().y, -1, 1).toArray());
//	glUniformMatrix4fv(modelMatID, 1, GL_TRUE, globalTransform.toArray());
//
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, textureID);
//	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadIBO);
//	glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, NULL);
//
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//	glBindTexture(GL_TEXTURE_2D, 0);
//	glUseProgram(0);
//}
