#include "LegacyTimer.h"

LegacyTimer::LegacyTimer(std::string a_Name)
{
	name = a_Name;
	totalTime = 0;
	QueryPerformanceFrequency(&frequency);
}

void LegacyTimer::StartTimer()
{
	QueryPerformanceCounter(&startTime);
}

void LegacyTimer::EndTimer()
{
	QueryPerformanceCounter(&endTime);
}

double LegacyTimer::GetTotalTime()
{
	totalTime = ((double)(endTime.QuadPart - startTime.QuadPart) / (double)(frequency.QuadPart));
	return totalTime;
}

void LegacyTimer::PrintTimers()
{
	std::cout << "Total time for " << name << " timer: " << totalTime << std::endl;
}

std::string LegacyTimer::GetName()
{
	return name;
}
