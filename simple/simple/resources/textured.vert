#version 400

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 colour;
layout (location = 2) in vec2 vertexUV;

smooth out vec4 vertColour;
out vec2 UV;

uniform float totalTime;
uniform mat4 projection;
uniform mat4 model;

void main()
{
	vertColour = colour;
	UV = vertexUV;
	vec4 scaledPosition = projection * model * position;
	gl_Position = scaledPosition;
}