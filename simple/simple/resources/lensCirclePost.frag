#version 400

in vec2 vUV;
out vec4 outputColour;

uniform sampler2D diffuseTexture;
vec2 lensRad = vec2(0.45f, 0.38f);

void main()
{
	vec4 colour = texture(diffuseTexture, vUV);
	float dist = distance(vUV, vec2(0.5f, 0.5f));
	colour.rgb *= smoothstep(lensRad.x, lensRad.y, dist);
	outputColour = colour;
}