#version 400

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 colour;

smooth out vec4 vertColour;

uniform mat4 projection;
uniform mat4 model;

void main()
{
	vertColour = colour;
	gl_Position = projection * model * position;
}
