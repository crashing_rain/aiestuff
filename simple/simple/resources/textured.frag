#version 400

smooth in vec4 vertColour;
in vec2 UV;

out vec4 outputColour;

uniform sampler2D diffuseTexture;

void main()
{
	outputColour = texture(diffuseTexture, UV).rgba * vertColour;
}
