#version 400

layout (location = 0) in vec4 position;

out vec2 vUV;

void main()
{
	vUV = position.xy * 0.5f + 0.5f;
	gl_Position = position;
}