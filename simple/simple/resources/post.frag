#version 400

in vec2 vUV;
out vec4 outputColour;

uniform sampler2D diffuseTexture;
uniform float totalTime;
uniform vec2 dimensions;

float pi = 3.14159f;
vec2 PixelSize = 1 / dimensions;
vec4 centrePixel = texture2D(diffuseTexture, vUV);

float edge_Threshold = 0.33f;
float edge_Threshold2 = 4.3f;

vec3 RGBtoHSV( float r, float g, float b)
{
	float minv;
	float maxv;
	float delta;
	vec3 res;

	minv = min(min(r,g), b);
	maxv = max(max(r,g), b);
	res.z = maxv;

	delta = maxv - minv;

	if (maxv != 0.0f)
	{
		res.y = delta / maxv;
	}
	else
	{
		res.y = 0;
		res.x = -1;
		return res;
	}

	if (r == maxv)
	{
		res.x = (g - b) / delta;	//yellow and magenta
	}
	else if (g == maxv)
	{
		res.x = 2.0f + ( b - r ) / delta;	//cyan and yellow
	}
	else
	{
		res.x = 4.0f + ( r - g ) / delta;	//magenta and cyan
	}

	res.x = res.x * 60.0f;
	if (res.x < 0.0f)
	{
		res.x = res.x + 360;	//degrees
	}
	return res;
}

vec3 HSVtoRGB(float h, float s, float v)
{
	int i;
	float f;
	float p;
	float q; 
	float t;
	vec3 res;

	//saturation is zero -> achromatic
	if (s == 0)
	{
		res.x = v;
		res.y = v;
		res.z = v;
		return res;
	}

	h /= 60.0f;
	i = int(floor(h));
	f = h - float(i);
	p = v * (1.0f - s );
	q = v * (1.0f - s * f );
	t = v * (1.0f - s * ( 1.0f - f ) );

	switch (i)
	{
		case 0:
			res.x = v;
			res.y = t;
			res.z = p;
			break;
		case 1:
			res.x = q;
			res.y = v;
			res.z = p;
			break;
		case 2:
			res.x = p;
			res.y = v;
			res.z = t;
			break;
		case 3:
			res.x = p;
			res.y = q;
			res.z = v;
			break;
		case 4:
			res.x = t;
			res.y = p;
			res.z = v;
			break;
		default:
			res.x = v;
			res.y = p;
			res.z = q;
			break;
	}
	return res;
}

float avg_intensity(vec4 pix)
{
	//get rgb channel, divide by 3 for an average
	return (pix.r + pix.g + pix.b) / 3.0f;
}

vec4 get_pixel(vec2 coords, float dx, float dy)
{
	//return a specific pixel on the screen
	return texture(diffuseTexture, coords + vec2(dx, dy));
}

float isEdge(vec2 coords)
{
	float dxTex = 1.0f / float(textureSize(diffuseTexture, 0));
	float dyTex = 1.0f / float(textureSize(diffuseTexture, 0));
	float pix[9];
	int k = -1;
	float delta;

	for (int i = -1; i < 2; ++i)
	{
		for (int j = -1; j < 2; ++j)
		{
			k++;
			pix[k] = avg_intensity(get_pixel(coords, float(i) * dxTex, float(j) * dyTex));
		}
	}

	delta = (abs(pix[1] - pix[7]) +
			 abs(pix[5] - pix[3]) +
			 abs(pix[0] - pix[8]) +
			 abs(pix[2] - pix[6])
			) / 4.0f;
	return clamp(edge_Threshold2 * delta, 0.0f, 1.0f);
}

void main() 
{
	vec2 lensRad = vec2(0.35f, 0.56f);
	vec2 tempUV = vec2(vUV.x, vUV.y);
	vec4 tc;

	vec4 lensCircle = texture(diffuseTexture, vUV);

	vec3 colorOrg = texture(diffuseTexture, tempUV).rgb;
	vec3 vHSV = RGBtoHSV(colorOrg.r, colorOrg.g, colorOrg.b);
	float edge = isEdge(tempUV);
	vec3 vRGB = (edge >= edge_Threshold) ? vec3(1, 0.1f, 0) : HSVtoRGB(vHSV.x, vHSV.y, vHSV.z);
	tc = vec4(vRGB.r, vRGB.g, vRGB.b, 1);

	float dist = distance(vUV, vec2(0.5f,0.5f));
	lensCircle.rgb *= (1 - smoothstep(lensRad.x, lensRad.y, dist));

	outputColour = mix(tc, vec4(lensCircle.rgb, 1), 0.5f);
}
