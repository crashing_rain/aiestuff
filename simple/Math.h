#ifndef _MATH_H
#define _MATH_H

#include <iostream>

const double MATH_PI = 3.14159265359;

//Multiply to get conversion
const float Rad2Deg = 0.01745329251994f;
const float Deg2Rad = 57.2957795130823f;

//Classes are reference type
//Structs are value type
//Reference types contain a pointer to another memory location that holds the data
//These are allocated on the heap
//Value types contain the data within its own memory allocation
//These are allocated on the stack

//Value types are cheaper to allocate and deallocate than reference types
struct Vector2
{
public:
	Vector2()
	{
		x = y = 0;
	}

	Vector2(float a_X, float a_Y)
	{
		x = a_X;
		y = a_Y;
	}

	Vector2(float a_XY)
	{
		x = y = a_XY;
	}

	Vector2(const Vector2& a_Other)
	{
		x = a_Other.x;
		y = a_Other.y;
	}

	Vector2 operator=(const Vector2& a_Other)
	{
		return Vector2(a_Other.x, a_Other.y);
	}

	Vector2 operator+(const Vector2& a_Other)
	{
		return Vector2(x + a_Other.x, y + a_Other.y);
	}

	Vector2 operator-(const Vector2& a_Other)
	{
		return Vector2(x - a_Other.x, y - a_Other.y);
	}

	//Calculate perpendicular of current vector
	Vector2 Perp()
	{
		return Vector2(y, -x);
	}

	Vector2 operator*(float a_Scalar)
	{
		return Vector2(x * a_Scalar, y * a_Scalar);
	}

	void Normalise()
	{
		x /= Magnitude();
		y /= Magnitude();
	}

	Vector2 GetNormal()
	{
		return Vector2(x / Magnitude(), y / Magnitude());
	}

	//Dot prod
	float Dot(const Vector2& a_Other)
	{
		return (x * a_Other.x + y * a_Other.y);
	}

	//Magnitude - distance from origin
	float SquaredMagnitude()
	{
		return (x * x) + (y * y);
	}

	float Magnitude()
	{
		return sqrtf(SquaredMagnitude());
	}

	//Not square root - dist between two points
	float SquaredDistance(const Vector2& a_Other)
	{
		return ( (x - a_Other.x) * (x - a_Other.x) + (y - a_Other.y) * (y - a_Other.y) );
	}

	//Square root
	float Distance(const Vector2& a_Other)
	{
		return sqrtf(SquaredDistance(a_Other));
	}

	float AngleBetween(const Vector2& a_Other, bool inRadians = true /*= true*/)
	{
		float angle = acos(this->Dot(a_Other));
		float perpAngle = this->Perp().Dot(a_Other);
		if (perpAngle < 0)
		{
			angle = -(abs(angle));
		}
		else
		{
			angle = abs(angle);
		}

		if (inRadians)
		{
			return angle;
		}
		else
		{
			return Rad2Deg * angle;
		}
	}

	bool operator||(const Vector2& a_Other)
	{
		if (x - a_Other.x == 0 && y - a_Other.y == 0)
			return true;
		return false;
	}

	Vector2& operator+=(const Vector2& a_Other)
	{
		x += a_Other.x;
		y += a_Other.y;
		return *this;
	}

	Vector2& operator-=(const Vector2& a_Other)
	{
		x -= a_Other.x;
		y -= a_Other.y;
		return *this;
	}

	friend std::ostream& operator<<(std::ostream &output, const Vector2& a_Vector)
	{
		output << "X: " << a_Vector.x << " Y: " << a_Vector.y;
		return output;
	}

	float x,y;
};

//Position in 2d space - z = 1
//Speed, velo - z = 0
struct Vector3 
{ 
    Vector3() 
    { 
        x = y = z = 0; 
    } 
  
    Vector3(float a_XYZ) 
    { 
        x = y = z = a_XYZ; 
    } 
  
    Vector3(float a_X, float a_Y, float a_Z) 
    { 
        x = a_X; 
        y = a_Y; 
        z = a_Z; 
    } 
  
    Vector3(const Vector3& a) 
    { 
        x = a.x; 
        y = a.y;
		z = a.z;
    } 
  
    Vector3 operator=(const Vector3& a) 
    { 
        return Vector3(a.x, a.y, a.z); 
    } 
  
    Vector3 operator+(const Vector3& a) 
    { 
        return Vector3(x + a.x, y + a.y, z + a.z); 
    } 
  
    Vector3 operator-(const Vector3& a) 
    { 
        return Vector3(x - a.x, y - a.y, z - a.z); 
    } 

	Vector3& operator+=(const Vector3& a)
	{
		this->x += a.x;
		this->y += a.y;
		this->z += a.z;
		return *this;
	}
  
	float Dot(const Vector3& a_Other)
	{
		return (x * a_Other.x + y * a_Other.y + z * a_Other.z);
	}

    //dist from origin 
    float Magnitude() 
    { 
        return sqrtf(SquaredMagnitude()); 
    } 
  
    float SquaredMagnitude() 
    { 
        return ( x * x + y * y + z * z); 
    } 
  
    //dist between current and another point 
    float Distance(const Vector3& a) 
    { 
        return sqrtf(SquaredDistance(a)); 
    } 
  
    float SquaredDistance(const Vector3& a) 
    { 
        float xCalc = a.x - x; 
        float yCalc = a.y - y; 
		float zCalc = a.z - z;
  
        return ( xCalc * xCalc + yCalc * yCalc + zCalc * zCalc ); 
    } 
  
    void Normalise() 
    { 
		float mag = Magnitude();
        x /= mag; 
        y /= mag; 
		z /= mag;
    } 
  
    Vector3 GetNormal() 
    { 
        return Vector3(x / Magnitude(), y / Magnitude(), z / Magnitude()); 
    } 

	float AngleBetween(const Vector3& a_Other, bool inRadians = true /*= true*/)
	{
		float angle = acosf(this->Dot(a_Other));
		float perpAngle = (this->Cross(a_Other)).Dot(a_Other);
		if (perpAngle < 0)
		{
			angle = -(abs(angle));
		}
		else
		{
			angle = abs(angle);
		}

		if (inRadians)
		{
			return angle;
		}
		else
		{
			return Rad2Deg * angle;
		}
	}
  
    bool operator==(const Vector3& a_Other) 
    { 
        if ( x - a_Other.x == 0 && y - a_Other.y == 0  && z - a_Other.z == 0) 
            return true; 
        return false; 
    } 
  
    Vector3 operator*(float a) 
    { 
        return Vector3(x * a, y * a, z * a); 
    }

	//a x b != b x a
	//a in this case is the current vector, b is the other vector coming in
	Vector3 Cross(const Vector3& a_Other)
	{
		//			   a2 * b3 - a3 * b2			 a3 * b1 - a1 * b3				a1 * b2 - a2 * b1
		return Vector3(y * a_Other.z - z * a_Other.y, z * a_Other.x - x * a_Other.z, x * a_Other.y - y * a_Other.x);
	}
  
    friend std::ostream& operator<<(std::ostream& output, const Vector3& a_Other) 
    { 
        output << "X: " << a_Other.x << " Y: " << a_Other.y << " Z: " << a_Other.z; 
        return output; 
    }
  
    float x,y,z; 
}; 

struct Vector4
{
	Vector4()
	{
		x = y = z = 0;
		w = 1;
	}

	Vector4(float a_XYZ)
	{
		x = y = z = a_XYZ;
		w = 1;
	}

	Vector4(float a_X, float a_Y, float a_Z)
	{
		x = a_X;
		y = a_Y;
		z = a_Z;
		w = 1;
	}

	Vector4(float a_X, float a_Y, float a_Z, float a_W)
	{
		x = a_X;
		y = a_Y;
		z = a_Z;
		w = a_W;
	}

	Vector4(const Vector2& a_Other, float a_Z, float a_W)
	{
		x = a_Other.x;
		y = a_Other.y;
		z = a_Z;
		w = a_W;
	}

	Vector4(const Vector3& a_Other, float a_W)
	{
		x = a_Other.x;
		y = a_Other.y;
		z = a_Other.z;
		w = a_W;
	}

	Vector4(const Vector4& a_Other)
	{
		x = a_Other.x;
		y = a_Other.y;
		z = a_Other.z;
		w = a_Other.w;
	}

	Vector4 operator*(float a_Scalar)
	{
		return Vector4(x * a_Scalar, y * a_Scalar, z * a_Scalar, w);
	}

	Vector4& operator*=(float a_Scalar)
	{
		this->x *= a_Scalar;
		this->y *= a_Scalar;
		this->z *= a_Scalar;
		return *this;
	}

	bool operator==(const Vector4& a_Other) 
    { 
        if ( x - a_Other.x == 0 && y - a_Other.y == 0 && z - a_Other.z == 0 ) 
            return true; 
        return false; 
    } 

	float Dot(const Vector4& a_Other)
	{
		return (x * a_Other.x + y * a_Other.y + z * a_Other.z);
	}

	float Magnitude()
	{
		return sqrtf( SquaredMagnitude() );
	}

	float SquaredMagnitude()
	{
		return ((x * x) + (y * y) + (z * z));
	}

	float Distance(const Vector4& a_Other)
	{
		return sqrtf(SquaredDistance(a_Other));
	}

	float SquaredDistance(const Vector4& a_Other)
	{
		float xDiff = a_Other.x - x;
		float yDiff = a_Other.y - y;
		float zDiff = a_Other.z - z;
		return ( (xDiff * xDiff) + (yDiff * yDiff) + (zDiff * zDiff) );
	}

	//a x b != b x a
	//a in this case is the current vector, b is the other vector coming in
	Vector4 Cross(const Vector4& a_Other)
	{
		//			   a2 * b3 - a3 * b2			 a3 * b1 - a1 * b3				a1 * b2 - a2 * b1
		return Vector4(y * a_Other.z - z * a_Other.y, z * a_Other.x - x * a_Other.z, x * a_Other.y - y * a_Other.x);
	}

	float AngleBetween(const Vector4& a_Other, bool inRadians = true /*= true*/)
	{
		float angle = acos(this->Dot(a_Other));
		float perpAngle = this->Cross(a_Other).Dot(a_Other);
		if (perpAngle < 0)
		{
			angle = -(abs(angle));
		}
		else
		{
			angle = abs(angle);
		}

		if (inRadians)
		{
			return angle;
		}
		else
		{
			return Rad2Deg * angle;
		}
	}

	friend std::ostream& operator<<(std::ostream& output, const Vector4& a_Other) 
    { 
        output << "X: " << a_Other.x << " Y: " << a_Other.y << " Z: " << a_Other.z << "W: " << a_Other.w; 
        return output; 
    }

	float x, y, z, w;
};

//scale * rotation * translation
struct mat3
{
public:
	mat3()
	{
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				if (i == j)
				{
					m[i][j] = 1;
				}
				else 
				{
					m[i][j] = 0;
				}
			}
		}
	}

	mat3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
	{
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;

		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;

		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
	}

	mat3(Vector3 a1, Vector3 a2, Vector3 a3)
	{
		m[0][0] = a1.x;
		m[0][1] = a1.y;
		m[0][2] = a1.z;

		m[1][0] = a2.x;
		m[1][1] = a2.y;
		m[1][2] = a2.z;

		m[2][0] = a3.x;
		m[2][1] = a3.y;
		m[2][2] = a3.z;
	}

	mat3 identity()
	{
		mat3 temp;
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				if (i == j)
				{
					temp.m[i][j] = 1;
				}
				else 
				{
					temp.m[i][j] = 0;
				}
			}
		}
		return temp;
	}

	//Modifies current matrix
	//But also returns transpose
	mat3& transpose()
	{
		mat3 temp = *this;
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				m[j][i] = temp.m[i][j];
			}
		}
		return *this;
	}

	//Calcs transpose of this
	//Returning new mat3
	mat3 getTranspose()
	{
		mat3 transpose;
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				transpose.m[j][i] = m[i][j];
			}
		}
		return transpose;
	}

	//Changes the current matrix to be a particular matrix
	mat3& makeRotation(float a_rads)
	{
		 m[0][0] = cosf(a_rads);
		 m[0][1] = -sinf(a_rads);
		 m[1][0] = sinf(a_rads);
		 m[1][1] = cosf(a_rads);
		 return *this;
	}

	mat3& makeScaling(const Vector3& a_Scale)
	{
		m[0][0] *= a_Scale.x;
		m[1][0] *= a_Scale.y;
		m[2][0] *= a_Scale.z;

		m[0][1] *= a_Scale.x;
		m[1][1] *= a_Scale.y;
		m[2][1] *= a_Scale.z;

		return *this;
	}

	mat3& makeTranslation(const Vector3& a_Translation)
	{
		m[0][2] = a_Translation.x;
		m[1][2] = a_Translation.y;
		return *this;
	}

	//Generate new matrix and return it
	static mat3 setupRotation(float a_Rads)
	{
		mat3 temp;
		temp.m[0][0] = cosf(a_Rads);
		temp.m[0][1] = -sinf(a_Rads);
		temp.m[1][0] = sinf(a_Rads);
		temp.m[1][1] = cosf(a_Rads);
		return temp;
	}

	static mat3 setupScaling(const Vector3& a_Scale)
	{
		mat3 temp;
		temp.m[0][0] *= a_Scale.x;
		temp.m[1][0] *= a_Scale.y;
		temp.m[2][0] *= a_Scale.z;

		temp.m[0][1] *= a_Scale.x;
		temp.m[1][1] *= a_Scale.y;
		temp.m[2][1] *= a_Scale.z;
		return temp;
	}

	static mat3 setupTranslation(const Vector3& a_Translation)
	{
		mat3 temp;
		temp.m[0][2] = a_Translation.x;
		temp.m[1][2] = a_Translation.y;
		return temp;
	}

	mat3 operator+(const mat3& a)
	{
		return (mat3(m[0][0] + a.m[0][0], m[0][1] + a.m[0][1], m[0][2] + a.m[0][2],
					 m[1][0] + a.m[1][0], m[1][1] + a.m[1][1], m[1][2] + a.m[1][2],
					 m[2][0] + a.m[2][0], m[2][1] + a.m[2][1], m[2][2] + a.m[2][2]));
	}

	mat3 operator-(const mat3& a)
	{
		return (mat3(m[0][0] - a.m[0][0], m[0][1] - a.m[0][1], m[0][2] - a.m[0][2],
					 m[1][0] - a.m[1][0], m[1][1] - a.m[1][1], m[1][2] - a.m[1][2],
					 m[2][0] - a.m[2][0], m[2][1] - a.m[2][1], m[2][2] - a.m[2][2]));
	}

	mat3& operator+=(const mat3& a)
	{
		*this = *this + a;
		return *this;
	}

	mat3& operator-=(const mat3& a)
	{
		*this = *this - a;
		return *this;
	}

	mat3& operator*=(const mat3& a)
	{
		*this = *this * a;
		return *this;
	}

	mat3 operator*(const mat3& a)
	{
		return (mat3(m[0][0] * a.m[0][0] + m[0][1] * a.m[1][0] + m[0][2] * a.m[2][0], m[0][0] * a.m[0][1] + m[0][1] * a.m[1][1] + m[0][2] * a.m[2][1], m[0][0] * a.m[0][2] + m[0][1] * a.m[1][2] + m[0][2] * a.m[2][2],
					 m[1][0] * a.m[0][0] + m[1][1] * a.m[1][0] + m[1][2] * a.m[2][0], m[1][0] * a.m[0][1] + m[1][1] * a.m[1][1] + m[1][2] * a.m[2][1], m[1][0] * a.m[0][2] + m[1][1] * a.m[1][2] + m[1][2] * a.m[2][2],
					 m[2][0] * a.m[0][0] + m[2][1] * a.m[1][0] + m[2][2] * a.m[2][0], m[2][0] * a.m[0][1] + m[2][1] * a.m[1][1] + m[2][2] * a.m[2][1], m[2][0] * a.m[0][2] + m[2][1] * a.m[1][2] + m[2][2] * a.m[2][2]));
	}

	//mat3 x vec3 = vec3
	//treat vec3 as 3 x 1 mat
	Vector3 operator*(const Vector3& a)
	{
		return Vector3(m[0][0] * a.x + m[0][1] * a.y + m[0][2] * a.z,
					   m[1][0] * a.x + m[1][1] * a.y + m[1][2] * a.z,
					   m[2][0] * a.x + m[2][1] * a.y + m[2][2] * a.z);
	}
	
	friend std::ostream& operator<<(std::ostream& output, const mat3& a_Other) 
	{
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				output << a_Other.m[i][j] << " ";
			}
			output << std::endl;
		}
		return output;
	}

	float m[3][3];
};

struct mat4
{
	mat4()
	{
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				if (i == j)
				{
					m[i][j] = 1;
				}
				else 
				{
					m[i][j] = 0;
				}
			}
		}
	}

	mat4(float m00, float m01, float m02, float m03, 
		 float m10, float m11, float m12, float m13, 
		 float m20, float m21, float m22, float m23, 
		 float m30, float m31, float m32, float m33)
	{
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;
		m[0][3] = m03;

		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;
		m[1][3] = m13;

		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
		m[2][3] = m23;

		m[3][0] = m30;
		m[3][1] = m31;
		m[3][2] = m32;
		m[3][3] = m33;
	}

	mat4(Vector4 a1, Vector4 a2, Vector4 a3, Vector4 a4)
	{
		m[0][0] = a1.x;
		m[0][1] = a1.y;
		m[0][2] = a1.z;
		m[0][3] = a1.w;

		m[1][0] = a2.x;
		m[1][1] = a2.y;
		m[1][2] = a2.z;
		m[1][3] = a2.w;

		m[2][0] = a3.x;
		m[2][1] = a3.y;
		m[2][2] = a3.z;
		m[2][3] = a3.w;

		m[3][0] = a4.x;
		m[3][1] = a4.y;
		m[3][2] = a4.z;
		m[3][3] = a4.w;
	}

	mat4 identity()
	{
		mat4 temp;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				if (i == j)
				{
					temp.m[i][j] = 1;
				}
				else 
				{
					temp.m[i][j] = 0;
				}
			}
		}
		return temp;
	}

	//Modifies current matrix
	//But also returns transpose
	mat4& transpose()
	{
		mat4 temp = *this;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				m[j][i] = temp.m[i][j];
			}
		}
		return *this;
	}

	//Calcs transpose of this
	//Returning new mat3
	mat4 getTranspose()
	{
		mat4 transpose;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				transpose.m[j][i] = m[i][j];
			}
		}
		return transpose;
	}

	mat4& makeRotationX(float a_rads)
	{
		 m[1][1] = cosf(a_rads);
		 m[1][2] = -sinf(a_rads);
		 m[2][1] = sinf(a_rads);
		 m[2][2] = cosf(a_rads);
		 return *this;
	}

	mat4& makeRotationY(float a_rads)
	{
		 m[0][0] = cosf(a_rads);
		 m[0][2] = sinf(a_rads);
		 m[2][0] = sinf(a_rads);
		 m[2][2] = cosf(a_rads);
		 return *this;
	}

	mat4& makeRotationZ(float a_rads)
	{
		 m[0][0] = cosf(a_rads);
		 m[0][1] = -sinf(a_rads);
		 m[1][0] = sinf(a_rads);
		 m[1][1] = cosf(a_rads);
		 return *this;
	}

	mat4& makeScaling(const Vector4& a_Scale)
	{
		m[0][0] = a_Scale.x;
		m[1][1] = a_Scale.y;
		m[2][2] = a_Scale.z;

		return *this;
	}

	mat4& makeTranslation(const Vector4& a_Translation)
	{
		m[0][3] = a_Translation.x;
		m[1][3] = a_Translation.y;
		m[2][3] = a_Translation.z;
		return *this;
	}

	//Generate new matrix and return it
	static mat4 setupRotationX(float a_rads)
	{
		mat4 temp;
		
		temp.m[1][1] = cosf(a_rads);
		temp.m[1][2] = -sinf(a_rads);
		temp.m[2][1] = sinf(a_rads);
		temp.m[2][2] = cosf(a_rads);

		return temp;
	}

	static mat4 setupRotationY(float a_rads)
	{
		mat4 temp;
		
		temp.m[0][0] = cosf(a_rads);
		temp.m[0][2] = sinf(a_rads);
		temp.m[2][0] = sinf(a_rads);
		temp.m[2][2] = cosf(a_rads);

		return temp;
	}

	static mat4 setupRotationZ(float a_rads)
	{
		mat4 temp;
		
		temp.m[0][0] = cosf(a_rads);
		temp.m[0][1] = -sinf(a_rads);
		temp.m[1][0] = sinf(a_rads);
		temp.m[1][1] = cosf(a_rads);

		return temp;
	}

	static mat4 setupScaling(const Vector3& a_Scale)
	{
		mat4 temp;
		
		temp.m[0][0] = a_Scale.x;
		temp.m[1][1] = a_Scale.y;
		temp.m[2][2] = a_Scale.z;

		return temp;
	}

	static mat4 setupTranslation(const Vector3& a_Translation)
	{
		mat4 temp;
		
		temp.m[0][3] = a_Translation.x;
		temp.m[1][3] = a_Translation.y;
		temp.m[2][3] = a_Translation.z;

		return temp;
	}

	mat4 operator+(const mat4& a)
	{
		return (mat4(m[0][0] + a.m[0][0], m[0][1] + a.m[0][1], m[0][2] + a.m[0][2], m[0][3] + a.m[0][3],
					 m[1][0] + a.m[1][0], m[1][1] + a.m[1][1], m[1][2] + a.m[1][2], m[1][3] + a.m[1][3],
					 m[2][0] + a.m[2][0], m[2][1] + a.m[2][1], m[2][2] + a.m[2][2], m[2][3] + a.m[2][3],
					 m[3][0] + a.m[3][0], m[3][1] + a.m[3][1], m[3][2] + a.m[3][2], m[3][3] + a.m[3][3]));
	}

	mat4 operator-(const mat4& a)
	{
		return (mat4(m[0][0] - a.m[0][0], m[0][1] - a.m[0][1], m[0][2] - a.m[0][2], m[0][3] - a.m[0][3],
					 m[1][0] - a.m[1][0], m[1][1] - a.m[1][1], m[1][2] - a.m[1][2], m[1][3] - a.m[1][3],
					 m[2][0] - a.m[2][0], m[2][1] - a.m[2][1], m[2][2] - a.m[2][2], m[2][3] - a.m[2][3],
					 m[3][0] - a.m[3][0], m[3][1] - a.m[3][1], m[3][2] - a.m[3][2], m[3][3] - a.m[3][3]));
	}

	mat4 operator*(const mat4& a)
	{
		return (mat4(m[0][0] * a.m[0][0] + m[0][0] * a.m[0][1] + m[0][0] * a.m[0][2] + m[0][0] * a.m[0][3], 
					 m[0][1] * a.m[1][0] + m[0][1] * a.m[1][1] + m[0][1] * a.m[1][2] + m[0][1] * a.m[1][3], 
					 m[0][2] * a.m[2][0] + m[0][2] * a.m[2][1] + m[0][2] * a.m[2][2] + m[0][2] * a.m[2][3],  
					 m[0][3] * a.m[3][0] + m[0][3] * a.m[3][1] + m[0][3] * a.m[3][2] + m[0][3] * a.m[3][3], 

					 m[1][0] * a.m[0][0] + m[1][0] * a.m[0][1] + m[1][0] * a.m[0][2] + m[1][0] * a.m[0][3], 
					 m[1][1] * a.m[1][0] + m[1][1] * a.m[1][1] + m[1][1] * a.m[1][2] + m[1][1] * a.m[1][3], 
					 m[1][2] * a.m[2][0] + m[1][2] * a.m[2][1] + m[1][2] * a.m[2][2] + m[1][2] * a.m[2][3], 
					 m[1][3] * a.m[3][0] + m[1][3] * a.m[3][1] + m[1][3] * a.m[3][2] + m[1][3] * a.m[3][3], 

					 m[2][0] * a.m[0][0] + m[2][0] * a.m[0][1] + m[2][0] * a.m[0][2] + m[2][0] * a.m[0][3], 
					 m[2][1] * a.m[1][0] + m[2][1] * a.m[1][1] + m[2][1] * a.m[1][2] + m[2][1] * a.m[1][3], 
					 m[2][2] * a.m[2][0] + m[2][2] * a.m[2][1] + m[2][2] * a.m[2][2] + m[2][2] * a.m[2][3], 
					 m[2][3] * a.m[3][0] + m[2][3] * a.m[3][1] + m[2][3] * a.m[3][2] + m[2][3] * a.m[3][3], 

					 m[3][0] * a.m[0][0] + m[3][0] * a.m[0][1] + m[3][0] * a.m[0][2] + m[3][0] * a.m[0][3], 
					 m[3][1] * a.m[1][0] + m[3][1] * a.m[1][1] + m[3][1] * a.m[1][2] + m[3][1] * a.m[1][3], 
					 m[3][2] * a.m[2][0] + m[3][2] * a.m[2][1] + m[3][2] * a.m[2][2] + m[3][2] * a.m[2][3], 
					 m[3][3] * a.m[3][0] + m[3][3] * a.m[3][1] + m[3][3] * a.m[3][2] + m[3][3] * a.m[3][3]));
	}

	mat4& operator*=(const mat4& a)
	{
		*this = *this * a;
		return *this;
	}

	mat4& operator+=(const mat4& a)
	{
		*this = *this + a;
		return *this;
	}

	mat4& operator-=(const mat4& a)
	{
		*this = *this - a;
		return *this;
	}

	Vector4 operator*(const Vector4& a)
	{
		return Vector4(m[0][0] * a.x + m[0][1] * a.y + m[0][2] * a.z,
					   m[1][0] * a.x + m[1][1] * a.y + m[1][2] * a.z,
					   m[2][0] * a.x + m[2][1] * a.y + m[2][2] * a.z,
					   a.w);
	}

	friend std::ostream& operator<<(std::ostream& output, const mat4& a_Other)
	{
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				output << a_Other.m[i][j] << " ";
			}
			output << std::endl;
		}
		return output;
	}

	float m[4][4];
};

struct Vertex
{
	Vector4 position;
	Vector4 colour;
	Vector2 uvs;
};

#endif