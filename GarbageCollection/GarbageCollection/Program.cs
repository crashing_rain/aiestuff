﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageCollection
{
    /*
     Stack = managed by operating system
     Heap = handled manually
     GC handles allocation to heap therefore creating managed heap
     Resources not managed by the CLR
     Non-deterministic finalisation
     Non-.NET code
     Unmanaged resources include:
        open files
        open network connections
        XNA - vertex buffers, index buffers, textures, etc.
     */

    class MyObject : IDisposable
    {
        public MyObject()
        {
            Console.WriteLine("CONSTRUCTOR");
        }

        public void Print()
        {
            Console.WriteLine("PRINT FUNCTION");
        }

        //Finalise method
        ~MyObject()
        {
            Dispose();
        }

        //Dispose method
        public void Dispose()
        {
            Console.WriteLine("DISPOSE THINGS");
            GC.SuppressFinalize(this);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<MyObject> objects = new List<MyObject>();

            for (int i = 0; i < 100; ++i)
            {
                objects.Add(new MyObject());
            }

            using (MyObject n = new MyObject())
            {
                n.Print();
            }

            Console.ReadLine();
        }
    }
}
