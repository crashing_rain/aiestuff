﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

#if WINDOWS
namespace TestingMonogameSound
#elif PSM
namespace PSMTestingMonogameSound
#endif
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
		SpriteFont sf;

        //Use sound effect instances to allow for control of specific things about
        //the sound effect, like volume, panning, etc
        SoundEffectInstance testSoundInstance;

        //song has to be wma format
        //xnb file contains relative file path to
        //wma file, so make sure to build xnb file
        //in the right place
        //wma converter
        //http://www.online-convert.com/result/c3e88406591d72b74725c4d3747f5ddc
        Song testSong;
		Song testSong2;
		
		Texture2D blankSquare;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            SoundEffect testSoundEffect = Content.Load<SoundEffect>("Explosion");
            testSoundInstance = testSoundEffect.CreateInstance();
			blankSquare = Content.Load<Texture2D>("blankSquare");
			sf = Content.Load<SpriteFont>("spritefont1");
#if WINDOWS
            testSong = Content.Load<Song>("scattle");
#elif PSM
			testSong = Content.Load<Song>("daisuke");
			testSong2 = Content.Load<Song>("crush");
#endif
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
#if WINDOWS
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                testSoundInstance.Play();
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                MediaPlayer.IsRepeating = false;
                MediaPlayer.Play(testSong);
            }
#elif PSM
			var pressedButtons = Sce.PlayStation.Core.Input.GamePad.GetData(0);
			if ((pressedButtons.ButtonsDown & Sce.PlayStation.Core.Input.GamePadButtons.Square) == Sce.PlayStation.Core.Input.GamePadButtons.Square)
			{
				MediaPlayer.Play(testSong);
			}
			
			if ((pressedButtons.ButtonsDown & Sce.PlayStation.Core.Input.GamePadButtons.Circle) == Sce.PlayStation.Core.Input.GamePadButtons.Circle)
			{
				MediaPlayer.Play(testSong2);
			}
#endif

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
			
			spriteBatch.Begin();
			
			spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
