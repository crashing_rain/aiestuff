#include <iostream>
#include <vector>
#include <time.h>

#include <Matrix4.h>
#include <Bezier.h>
#include <Vector2.h>
#include <Constants.h>

void main()
{
	srand((unsigned int)(time(NULL)));
	std::vector<Vector2> pointsList;

	for (int i = 0; i < 10; i++)
	{
		pointsList.push_back(Vector2(rand() % 100, rand() % 100));
	}

	float time = 0;
	
	while (time < 1)
	{
		std::cout << Vector2::Bezier(pointsList, time) << std::endl;
		time += 0.01f;
	}

	system("pause");
}
