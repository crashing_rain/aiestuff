#include "Matrix3.h"

mat3::mat3()
{
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (i == j)
			{
				m[i][j] = 1;
			}
			else 
			{
				m[i][j] = 0;
			}
		}
	}
}

float* mat3::ToArray()
{
	float* temp = new float[9];
	temp[0] = m[0][0];
	temp[1] = m[0][1];
	temp[2] = m[0][2];
	temp[3] = m[0][3];
	temp[4] = m[1][0];
	temp[5] = m[1][1];
	temp[6] = m[1][2];
	temp[7] = m[1][3];
	temp[8] = m[2][0];
	return temp;
}

mat3::mat3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
{
	m[0][0] = m00;
	m[0][1] = m01;
	m[0][2] = m02;

	m[1][0] = m10;
	m[1][1] = m11;
	m[1][2] = m12;

	m[2][0] = m20;
	m[2][1] = m21;
	m[2][2] = m22;
}

mat3::mat3(Vector3 a1, Vector3 a2, Vector3 a3)
{
	m[0][0] = a1.x;
	m[0][1] = a1.y;
	m[0][2] = a1.z;

	m[1][0] = a2.x;
	m[1][1] = a2.y;
	m[1][2] = a2.z;

	m[2][0] = a3.x;
	m[2][1] = a3.y;
	m[2][2] = a3.z;
}

mat3& mat3::operator=(const mat3& a)
{
	m[0][0] = a.m[0][0];
	m[0][1] = a.m[0][1];
	m[0][2] = a.m[0][2];
						
	m[1][0] = a.m[1][0];
	m[1][1] = a.m[1][1];
	m[1][2] = a.m[1][2];
						
	m[2][0] = a.m[2][0];
	m[2][1] = a.m[2][1];
	m[2][2] = a.m[2][2];
	return *this;
}

mat3 mat3::identity()
{
	mat3 temp;
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (i == j)
			{
				temp.m[i][j] = 1;
			}
			else 
			{
				temp.m[i][j] = 0;
			}
		}
	}
	return temp;
}

//Modifies current matrix
//But also returns transpose
mat3& mat3::transpose()
{
	mat3 temp = *this;
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			m[j][i] = temp.m[i][j];
		}
	}
	return *this;
}

//Calcs transpose of this
//Returning new mat3
mat3 mat3::getTranspose()
{
	mat3 transpose;
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			transpose.m[j][i] = m[i][j];
		}
	}
	return transpose;
}

//Changes the current matrix to be a particular matrix
mat3& mat3::makeRotation(float a_rads)
{
		m[0][0] = cosf(a_rads);
		m[0][1] = -sinf(a_rads);
		m[1][0] = sinf(a_rads);
		m[1][1] = cosf(a_rads);
		return *this;
}

mat3& mat3::makeScaling(const Vector3& a_Scale)
{
	m[0][0] *= a_Scale.x;
	m[1][0] *= a_Scale.y;
	m[2][0] *= a_Scale.z;

	m[0][1] *= a_Scale.x;
	m[1][1] *= a_Scale.y;
	m[2][1] *= a_Scale.z;

	return *this;
}

mat3& mat3::makeTranslation(const Vector3& a_Translation)
{
	m[0][2] = a_Translation.x;
	m[1][2] = a_Translation.y;
	return *this;
}

//Generate new matrix and return it
mat3 mat3::setupRotation(float a_Rads)
{
	mat3 temp;
	temp.m[0][0] = cosf(a_Rads);
	temp.m[0][1] = -sinf(a_Rads);
	temp.m[1][0] = sinf(a_Rads);
	temp.m[1][1] = cosf(a_Rads);
	return temp;
}

mat3 mat3::setupScaling(const Vector3& a_Scale)
{
	mat3 temp;
	temp.m[0][0] *= a_Scale.x;
	temp.m[1][0] *= a_Scale.y;
	temp.m[2][0] *= a_Scale.z;

	temp.m[0][1] *= a_Scale.x;
	temp.m[1][1] *= a_Scale.y;
	temp.m[2][1] *= a_Scale.z;
	return temp;
}

mat3 mat3::setupTranslation(const Vector3& a_Translation)
{
	mat3 temp;
	temp.m[0][2] = a_Translation.x;
	temp.m[1][2] = a_Translation.y;
	return temp;
}

mat3 mat3::operator+(const mat3& a)
{
	return (mat3(m[0][0] + a.m[0][0], m[0][1] + a.m[0][1], m[0][2] + a.m[0][2],
					m[1][0] + a.m[1][0], m[1][1] + a.m[1][1], m[1][2] + a.m[1][2],
					m[2][0] + a.m[2][0], m[2][1] + a.m[2][1], m[2][2] + a.m[2][2]));
}

mat3 mat3::operator-(const mat3& a)
{
	return (mat3(m[0][0] - a.m[0][0], m[0][1] - a.m[0][1], m[0][2] - a.m[0][2],
					m[1][0] - a.m[1][0], m[1][1] - a.m[1][1], m[1][2] - a.m[1][2],
					m[2][0] - a.m[2][0], m[2][1] - a.m[2][1], m[2][2] - a.m[2][2]));
}

mat3& mat3::operator+=(const mat3& a)
{
	*this = *this + a;
	return *this;
}

mat3& mat3::operator-=(const mat3& a)
{
	*this = *this - a;
	return *this;
}

mat3& mat3::operator*=(const mat3& a)
{
	*this = *this * a;
	return *this;
}

mat3 mat3::operator*(const mat3& a)
{
	return (mat3(m[0][0] * a.m[0][0] + m[0][1] * a.m[1][0] + m[0][2] * a.m[2][0], m[0][0] * a.m[0][1] + m[0][1] * a.m[1][1] + m[0][2] * a.m[2][1], m[0][0] * a.m[0][2] + m[0][1] * a.m[1][2] + m[0][2] * a.m[2][2],
					m[1][0] * a.m[0][0] + m[1][1] * a.m[1][0] + m[1][2] * a.m[2][0], m[1][0] * a.m[0][1] + m[1][1] * a.m[1][1] + m[1][2] * a.m[2][1], m[1][0] * a.m[0][2] + m[1][1] * a.m[1][2] + m[1][2] * a.m[2][2],
					m[2][0] * a.m[0][0] + m[2][1] * a.m[1][0] + m[2][2] * a.m[2][0], m[2][0] * a.m[0][1] + m[2][1] * a.m[1][1] + m[2][2] * a.m[2][1], m[2][0] * a.m[0][2] + m[2][1] * a.m[1][2] + m[2][2] * a.m[2][2]));
}

//mat3 x vec3 = vec3
//treat vec3 as 3 x 1 mat
Vector3 mat3::operator*(const Vector3& a)
{
	return Vector3(m[0][0] * a.x + m[0][1] * a.y + m[0][2] * a.z,
					m[1][0] * a.x + m[1][1] * a.y + m[1][2] * a.z,
					m[2][0] * a.x + m[2][1] * a.y + m[2][2] * a.z);
}