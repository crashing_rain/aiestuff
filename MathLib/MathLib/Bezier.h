#ifndef _BEZIER_H
#define _BEZIER_H

#include <vector>
#include <iostream>

#include "Vector3.h"

class Bezier
{
public:
	Bezier();
	~Bezier();

	//deletes point array memory allocated to points and tangents
	//called by destructor and StartCreation
	void Destroy();

	//pushes a point and a tangent onto the vector<Vector3> variables
	void AddPoint(const Vector3& point, const Vector3& tangent);

	//deletes any memory created for the Vector3* arrays
	void StartCreation();

	//sets up pointer array using the vector<Vector3> variables
	void EndCreation();

	//pass in a number and this function returns a point that is on
	//the line - uses points and tangents pointer arrays, so those
	//should be filled in first, before this function is used.
	void GetPoint(float t, Vector3* result);

	//points on curve get filled with the data in points on curve
	Vector3* points;

	//tangents on curve get filled with the data in tangents on curve
	Vector3* tangents;

	void PrintData();
private:
	std::vector<Vector3> pointsOnCurve;
	std::vector<Vector3> tangentsOnCurve;
	int count;
};

#endif