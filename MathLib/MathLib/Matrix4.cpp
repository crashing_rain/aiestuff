#include "Matrix4.h"

mat4::mat4()
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (i == j)
			{
				m[i][j] = 1;
			}
			else 
			{
				m[i][j] = 0;
			}
		}
	}
}

mat4::mat4(float m00, float m01, float m02, float m03, 
		 float m10, float m11, float m12, float m13, 
		 float m20, float m21, float m22, float m23, 
		 float m30, float m31, float m32, float m33)
{
	m[0][0] = m00;
	m[0][1] = m01;
	m[0][2] = m02;
	m[0][3] = m03;

	m[1][0] = m10;
	m[1][1] = m11;
	m[1][2] = m12;
	m[1][3] = m13;

	m[2][0] = m20;
	m[2][1] = m21;
	m[2][2] = m22;
	m[2][3] = m23;

	m[3][0] = m30;
	m[3][1] = m31;
	m[3][2] = m32;
	m[3][3] = m33;
}

mat4::mat4(Vector4 a1, Vector4 a2, Vector4 a3, Vector4 a4)
{
	m[0][0] = a1.x;
	m[0][1] = a1.y;
	m[0][2] = a1.z;
	m[0][3] = a1.w;

	m[1][0] = a2.x;
	m[1][1] = a2.y;
	m[1][2] = a2.z;
	m[1][3] = a2.w;

	m[2][0] = a3.x;
	m[2][1] = a3.y;
	m[2][2] = a3.z;
	m[2][3] = a3.w;

	m[3][0] = a4.x;
	m[3][1] = a4.y;
	m[3][2] = a4.z;
	m[3][3] = a4.w;
}

mat4& mat4::operator=(const mat4& a)
{
	m[0][0] = a.m[0][0];
	m[0][1] = a.m[0][1];
	m[0][2] = a.m[0][2];
	m[0][3] = a.m[0][3];
	
	m[1][0] = a.m[1][0];
	m[1][1] = a.m[1][1];
	m[1][2] = a.m[1][2];
	m[1][3] = a.m[1][3];
	
	m[2][0] = a.m[2][0];
	m[2][1] = a.m[2][1];
	m[2][2] = a.m[2][2];
	m[2][3] = a.m[2][3];

	m[3][0] = a.m[3][0];
	m[3][1] = a.m[3][1];
	m[3][2] = a.m[3][2];
	m[3][3] = a.m[3][3];
	return *this;
}

mat4 mat4::identity()
{
	mat4 temp;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (i == j)
			{
				temp.m[i][j] = 1;
			}
			else 
			{
				temp.m[i][j] = 0;
			}
		}
	}
	return temp;
}

mat4& mat4::transpose()
{
	mat4 temp = *this;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			m[i][j] = temp.m[j][i];
		}
	}
	return *this;
}

mat4 mat4::getTranspose()
{
	mat4 transpose;
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			transpose.m[j][i] = m[i][j];
		}
	}
	return transpose;
}

float* mat4::toArray()
{
	float* temp = new float[16];
	temp[0] = m[0][0];
	temp[1] = m[0][1];
	temp[2] = m[0][2];
	temp[3] = m[0][3];
	temp[4] = m[1][0];
	temp[5] = m[1][1];
	temp[6] = m[1][2];
	temp[7] = m[1][3];
	temp[8] = m[2][0];
	temp[9] = m[2][1];
	temp[10] = m[2][2];
	temp[11] = m[2][3];
	temp[12] = m[3][0];
	temp[13] = m[3][1];
	temp[14] = m[3][2];
	temp[15] = m[3][3];
	return temp;
}

mat4 mat4::getOrtho(float left, float right, float bottom, float top, float near, float far)
{
	mat4 ortho;

	ortho.m[0][0] = 2.0 / (right - left);
	ortho.m[0][1] = ortho.m[0][2] = ortho.m[0][3] = ortho.m[1][0] = 0;
	ortho.m[1][1] = 2.0 / (top - bottom);
	ortho.m[1][2] = ortho.m[1][3] = ortho.m[2][0] = ortho.m[2][1] = 0;
	ortho.m[2][2] = 2.0 / (near - far);
	ortho.m[2][3] = 0;
	ortho.m[3][0] = -1 * ((right + left) / (right - left));
	ortho.m[3][1] = -1 * ((top + bottom) / (top - bottom));
	ortho.m[3][2] = -1 * ((far + near) / (far - near));
	ortho.m[3][3] = 1;

	return ortho;
}

mat4& mat4::makeRotationX(float a_rads)
{
	m[1][1] = cosf(a_rads);
	m[1][2] = -sinf(a_rads);
	m[2][1] = sinf(a_rads);
	m[2][2] = cosf(a_rads);
	return *this;
}

mat4& mat4::makeRotationY(float a_rads)
{
	m[0][0] = cosf(a_rads);
	m[0][2] = sinf(a_rads);
	m[2][0] = sinf(a_rads);
	m[2][2] = cosf(a_rads);
	return *this;
}

mat4& mat4::makeRotationZ(float a_rads)
{
	m[0][0] = cosf(a_rads);
	m[0][1] = -sinf(a_rads);
	m[1][0] = sinf(a_rads);
	m[1][1] = cosf(a_rads);
	return *this;
}

mat4& mat4::makeScaling(const Vector4& a_Scale)
{
	m[0][0] = a_Scale.x;
	m[1][1] = a_Scale.y;
	m[2][2] = a_Scale.z;

	return *this;
}

mat4& mat4::makeTranslation(const Vector4& a_Translation)
{
	m[0][3] = a_Translation.x;
	m[1][3] = a_Translation.y;
	m[2][3] = a_Translation.z;
	return *this;
}

//Generate new matrix and return it
mat4 mat4::setupRotationX(float a_rads)
{
	mat4 temp;
		
	temp.m[1][1] = cosf(a_rads);
	temp.m[1][2] = -sinf(a_rads);
	temp.m[2][1] = sinf(a_rads);
	temp.m[2][2] = cosf(a_rads);

	return temp;
}

mat4 mat4::setupRotationY(float a_rads)
{
	mat4 temp;
		
	temp.m[0][0] = cosf(a_rads);
	temp.m[0][2] = sinf(a_rads);
	temp.m[2][0] = sinf(a_rads);
	temp.m[2][2] = cosf(a_rads);

	return temp;
}

mat4 mat4::setupRotationZ(float a_rads)
{
	mat4 temp;
		
	temp.m[0][0] = cosf(a_rads);
	temp.m[0][1] = -sinf(a_rads);
	temp.m[1][0] = sinf(a_rads);
	temp.m[1][1] = cosf(a_rads);

	return temp;
}

mat4 mat4::setupScaling(const Vector3& a_Scale)
{
	mat4 temp;
		
	temp.m[0][0] = a_Scale.x;
	temp.m[1][1] = a_Scale.y;
	temp.m[2][2] = a_Scale.z;

	return temp;
}

mat4 mat4::setupTranslation(const Vector3& a_Translation)
{
	mat4 temp;
		
	temp.m[0][3] = a_Translation.x;
	temp.m[1][3] = a_Translation.y;
	temp.m[2][3] = a_Translation.z;

	return temp;
}

mat4 mat4::operator+(const mat4& a)
{
	return (mat4(m[0][0] + a.m[0][0], m[0][1] + a.m[0][1], m[0][2] + a.m[0][2], m[0][3] + a.m[0][3],
					m[1][0] + a.m[1][0], m[1][1] + a.m[1][1], m[1][2] + a.m[1][2], m[1][3] + a.m[1][3],
					m[2][0] + a.m[2][0], m[2][1] + a.m[2][1], m[2][2] + a.m[2][2], m[2][3] + a.m[2][3],
					m[3][0] + a.m[3][0], m[3][1] + a.m[3][1], m[3][2] + a.m[3][2], m[3][3] + a.m[3][3]));
}

mat4 mat4::operator-(const mat4& a)
{
	return (mat4(m[0][0] - a.m[0][0], m[0][1] - a.m[0][1], m[0][2] - a.m[0][2], m[0][3] - a.m[0][3],
					m[1][0] - a.m[1][0], m[1][1] - a.m[1][1], m[1][2] - a.m[1][2], m[1][3] - a.m[1][3],
					m[2][0] - a.m[2][0], m[2][1] - a.m[2][1], m[2][2] - a.m[2][2], m[2][3] - a.m[2][3],
					m[3][0] - a.m[3][0], m[3][1] - a.m[3][1], m[3][2] - a.m[3][2], m[3][3] - a.m[3][3]));
}

mat4 mat4::operator*(const mat4& a)
{
	return (mat4(m[0][0] * a.m[0][0] + m[0][1] * a.m[1][0] + m[0][2] * a.m[2][0] + m[0][3] * a.m[3][0], /*||||||*/ m[0][0] * a.m[0][1] + m[0][1] * a.m[1][1] + m[0][2] * a.m[2][1] + m[0][3] * a.m[3][1], /*||||||*/ m[0][0] * a.m[0][2] + m[0][1] * a.m[1][2] + m[0][2] * a.m[2][2] + m[0][3] * a.m[2][3], /*||||||*/ m[0][0] * a.m[0][3] + m[0][1] * a.m[1][3] + m[0][2] * a.m[2][3] + m[0][3] * a.m[3][3],
				 m[1][0] * a.m[0][0] + m[1][1] * a.m[1][0] + m[1][2] * a.m[2][0] + m[1][3] * a.m[3][0], /*||||||*/ m[1][0] * a.m[0][1] + m[1][1] * a.m[1][1] + m[1][2] * a.m[2][1] + m[1][3] * a.m[3][1], /*||||||*/ m[1][0] * a.m[0][2] + m[1][1] * a.m[1][2] + m[1][2] * a.m[2][2] + m[1][3] * a.m[2][3], /*||||||*/ m[1][0] * a.m[0][3] + m[1][1] * a.m[1][3] + m[1][2] * a.m[2][3] + m[1][3] * a.m[3][3], 
				 m[2][0] * a.m[0][0] + m[2][1] * a.m[1][0] + m[2][2] * a.m[2][0] + m[2][3] * a.m[3][0], /*||||||*/ m[2][0] * a.m[0][1] + m[2][1] * a.m[1][1] + m[2][2] * a.m[2][1] + m[2][3] * a.m[3][1], /*||||||*/ m[2][0] * a.m[0][2] + m[2][1] * a.m[1][2] + m[2][2] * a.m[2][2] + m[2][3] * a.m[2][3], /*||||||*/ m[2][0] * a.m[0][3] + m[2][1] * a.m[1][3] + m[2][2] * a.m[2][3] + m[2][3] * a.m[3][3], 				 
				 m[3][0] * a.m[0][0] + m[3][1] * a.m[1][0] + m[3][2] * a.m[2][0] + m[3][3] * a.m[3][0], /*||||||*/ m[3][0] * a.m[0][1] + m[3][1] * a.m[1][1] + m[3][2] * a.m[2][1] + m[3][3] * a.m[3][1], /*||||||*/ m[3][0] * a.m[0][2] + m[3][1] * a.m[1][2] + m[3][2] * a.m[2][2] + m[3][3] * a.m[2][3], /*||||||*/ m[3][0] * a.m[0][3] + m[3][1] * a.m[1][3] + m[3][2] * a.m[2][3] + m[3][3] * a.m[3][3]));
}

mat4& mat4::operator*=(const mat4& a)
{
	*this = *this * a;
	return *this;
}

mat4& mat4::operator+=(const mat4& a)
{
	*this = *this + a;
	return *this;
}

mat4& mat4::operator-=(const mat4& a)
{
	*this = *this - a;
	return *this;
}

Vector4 mat4::operator*(const Vector4& a)
{
	return Vector4(m[0][0] * a.x + m[0][1] * a.y + m[0][2] * a.z + m[0][3] * a.w,
				   m[1][0] * a.x + m[1][1] * a.y + m[1][2] * a.z + m[1][3] * a.w,
				   m[2][0] * a.x + m[2][1] * a.y + m[2][2] * a.z + m[2][3] * a.w,
				   m[3][0] * a.x + m[3][1] * a.y + m[3][2] * a.z + m[3][3] * a.w);
}
