#include "Vector3.h"

Vector3::Vector3() 
{ 
    x = y = z = 0; 
} 
  
Vector3::Vector3(float a_XYZ) 
{ 
    x = y = z = a_XYZ; 
} 
  
Vector3::Vector3(float a_X, float a_Y, float a_Z) 
{ 
    x = a_X; 
    y = a_Y; 
    z = a_Z; 
} 
  
Vector3::Vector3(const Vector3& a) 
{ 
    x = a.x; 
    y = a.y;
	z = a.z;
} 
  
Vector3& Vector3::operator=(const Vector3& a) 
{ 
    x = a.x;
	y = a.y;
	z = a.z;
	return *this;
} 
  
Vector3 Vector3::operator+(const Vector3& a) 
{ 
    return Vector3(x + a.x, y + a.y, z + a.z); 
} 
  
Vector3 Vector3::operator-(const Vector3& a) 
{ 
    return Vector3(x - a.x, y - a.y, z - a.z); 
} 

Vector3& Vector3::operator+=(const Vector3& a)
{
	*this = *this + a;
	return *this;
}

Vector3& Vector3::operator-=(const Vector3& a)
{
	*this = *this - a;
	return *this;
}
 
float* Vector3::ToArray()
{
	float* temp = new float[3];
	temp[0] = x;
	temp[1] = y;
	temp[2] = z;
	return temp;
}

float Vector3::Dot(const Vector3& a_Other)
{
	return (x * a_Other.x + y * a_Other.y + z * a_Other.z);
}

//dist from origin 
float Vector3::Magnitude() 
{ 
    return sqrtf(SquaredMagnitude()); 
} 
  
float Vector3::SquaredMagnitude() 
{ 
    return ( x * x + y * y + z * z); 
} 
  
//dist between current and another point 
float Vector3::Distance(const Vector3& a) 
{ 
    return sqrtf(SquaredDistance(a)); 
} 
  
float Vector3::SquaredDistance(const Vector3& a) 
{ 
    float xCalc = a.x - x; 
    float yCalc = a.y - y; 
	float zCalc = a.z - z;
  
    return ( xCalc * xCalc + yCalc * yCalc + zCalc * zCalc ); 
} 
  
void Vector3::Normalise() 
{ 
	float mag = Magnitude();
    x /= mag; 
    y /= mag; 
	z /= mag;
} 
  
Vector3 Vector3::GetNormal() 
{ 
    return Vector3(x / Magnitude(), y / Magnitude(), z / Magnitude()); 
} 

float Vector3::AngleBetween(const Vector3& a_Other, bool inRadians /*= true*/)
{
	float angle = acosf(this->Dot(a_Other));
	float perpAngle = (this->Cross(a_Other)).Dot(a_Other);
	if (perpAngle < 0)
	{
		angle = -(abs(angle));
	}
	else
	{
		angle = abs(angle);
	}

	if (inRadians)
	{
		return angle;
	}
	else
	{
		return Rad2Deg * angle;
	}
}
  
bool Vector3::operator==(const Vector3& a_Other) 
{ 
    if ( x - a_Other.x == 0 && y - a_Other.y == 0  && z - a_Other.z == 0) 
        return true; 
    return false; 
} 
  
Vector3 Vector3::operator*(float a) 
{ 
    return Vector3(x * a, y * a, z * a); 
}

//a x b != b x a
//a in this case is the current vector, b is the other vector coming in
Vector3 Vector3::Cross(const Vector3& a_Other)
{
	//			   a2 * b3 - a3 * b2			 a3 * b1 - a1 * b3				a1 * b2 - a2 * b1
	return Vector3(y * a_Other.z - z * a_Other.y, z * a_Other.x - x * a_Other.z, x * a_Other.y - y * a_Other.x);
}
