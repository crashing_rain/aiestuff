#include "Vector2.h"

Vector2::Vector2()
{
	x = y = 0;
}

Vector2::Vector2(float a_X, float a_Y)
{
	x = a_X;
	y = a_Y;
}

Vector2::Vector2(float a_XY)
{
	x = y = a_XY;
}

Vector2::Vector2(const Vector2& a_Other)
{
	x = a_Other.x;
	y = a_Other.y;
}

Vector2& Vector2::operator=(const Vector2& a_Other)
{
	x = a_Other.x;
	y = a_Other.y;
	return *this;
}

Vector2 Vector2::operator+(const Vector2& a_Other)
{
	return Vector2(x + a_Other.x, y + a_Other.y);
}

Vector2 Vector2::operator-(const Vector2& a_Other)
{
	return Vector2(x - a_Other.x, y - a_Other.y);
}

//Calculate perpendicular of current vector
Vector2 Vector2::Perp()
{
	return Vector2(y, -x);
}

Vector2 Vector2::operator*(float a_Scalar)
{
	return Vector2(x * a_Scalar, y * a_Scalar);
}

void Vector2::Normalise()
{
	x /= Magnitude();
	y /= Magnitude();
}

Vector2 Vector2::GetNormal()
{
	return Vector2(x / Magnitude(), y / Magnitude());
}

float* Vector2::ToArray()
{
	float* temp = new float[2];
	temp[0] = x;
	temp[1] = y;
	return temp;
}

//Dot prod
float Vector2::Dot(const Vector2& a_Other)
{
	return (x * a_Other.x + y * a_Other.y);
}

//Magnitude - distance from origin
float Vector2::SquaredMagnitude()
{
	return (x * x) + (y * y);
}

float Vector2::Magnitude()
{
	return sqrtf(SquaredMagnitude());
}

//Not square root - dist between two points
float Vector2::SquaredDistance(const Vector2& a_Other)
{
	return ( (x - a_Other.x) * (x - a_Other.x) + (y - a_Other.y) * (y - a_Other.y) );
}

//Square root
float Vector2::Distance(const Vector2& a_Other)
{
	return sqrtf(SquaredDistance(a_Other));
}

float Vector2::AngleBetween(const Vector2& a_Other, bool inRadians /*= true*/)
{
	float angle = acos(this->Dot(a_Other));
	float perpAngle = this->Perp().Dot(a_Other);
	if (perpAngle < 0)
	{
		angle = -(abs(angle));
	}
	else
	{
		angle = abs(angle);
	}

	if (inRadians)
	{
		return angle;
	}
	else
	{
		return Rad2Deg * angle;
	}
}

bool Vector2::operator==(const Vector2& a_Other)
{
	if (x - a_Other.x == 0 && y - a_Other.y == 0)
		return true;
	return false;
}

Vector2& Vector2::operator+=(const Vector2& a_Other)
{
	*this = *this + a_Other;
	return *this;
}

Vector2& Vector2::operator-=(const Vector2& a_Other)
{
	*this = *this - a_Other;
	return *this;
}

Vector2 Vector2::Lerp(Vector2 a_Start, Vector2 a_End, float a_Time)
{
	if (a_Time >= 1)
	{
		return a_End;
	}
	else if (a_Time <= 0)
	{
		return a_Start;
	}
	return (a_Start + (a_End - a_Start) * a_Time);
}

Vector2 Vector2::CubeBezier(Vector2 a_Start, Vector2 a_Mid1, Vector2 a_Mid2, Vector2 a_End, float a_Time)
{
	Vector2 mid1 = Lerp(a_Start, a_Mid1, a_Time);
	Vector2 mid2 = Lerp(a_Mid1, a_Mid2, a_Time);
	Vector2 mid3 = Lerp(a_Mid2, a_End, a_Time);

	return Lerp(Lerp(mid1, mid2, a_Time), mid3, a_Time);
}

Vector2 Vector2::QuadBezier(Vector2 a_Start, Vector2 a_Mid, Vector2 a_End, float a_Time)
{
	Vector2 mid1 = Lerp(a_Start, a_Mid, a_Time);
	Vector2 mid2 = Lerp(a_Mid, a_End, a_Time);

	return Lerp(mid1, mid2, a_Time);
}

Vector2 Vector2::Bezier(std::vector<Vector2> points, float a_Time)
{
	if (points.size() == 1)
	{
		return points[0];
	}
	else
	{
		std::vector<Vector2> midPoints;
		for (unsigned int i = 0; i < points.size() - 1; ++i)
		{
			Vector2 tempMid = Lerp(points[i], points[i + 1], a_Time);
			midPoints.push_back(tempMid);
		}
		return Bezier(midPoints, a_Time);
	}
}

Vector2 Vector2::HermiteSpline(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Tangent1, Vector2 a_Tangent2, float a_Time)
{
	float tsq = a_Time * a_Time;
	float tcub = tsq * a_Time;

	float h00 = 2 * tcub - 3 * tsq + 1;
	float h01 = -2 * tcub + 3 * tsq;

	float h10 = tcub - 2 * tsq + a_Time;
	float h11 = tcub - tsq;

	return (a_Point1 * h00 + a_Tangent1 * h10 + a_Point2 * h01 + a_Tangent2 * h11);
} 

//tightness controls how sharp the curves are
Vector2 Vector2::CardinalSpline(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Point3, float a_Tightness, float a_Time)
{
	Vector2 tangent1 = (a_Point2 - a_Point1) * a_Tightness;
	Vector2 tangent2 = (a_Point3 - a_Point2) * a_Tightness;

	float tsq = a_Time * a_Time;
	float tcub = tsq * a_Time;

	float h00 = 2 * tcub - 3 * tsq + 1;
	float h01 = -2 * tcub + 3 * tsq;
	float h10 = tcub - 2 * tsq + a_Time;
	float h11 = tcub - tsq;

	return (a_Point1 * h00 + tangent1 * h10 + a_Point2 * h01 + tangent2 * h11);
}

//tightness is locked at 0.5f
Vector2 Vector2::CatmullRom(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Point3, float a_Time)
{
	return CardinalSpline(a_Point1, a_Point2, a_Point3, 0.5f, a_Time);
}
