#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#include <math.h>
#include <iostream>

const double MATH_PI = 3.14159265359;

//Multiply to get conversion
const float Rad2Deg = 0.01745329251994f;
const float Deg2Rad = 57.2957795130823f;

class MathHelper
{
public:
	MathHelper(){}
	~MathHelper(){}

	template <class T> static T lerp(const T& a, const T& b, const float t)
	{
		return a + (b - a) * t;
	}

	template <class T> static T clamp(const T& val, const T& min, const T& max)
	{
		if (val < min)
			return min;
		if (val > max)
			return max;
		else return val;
	}

	float static smoothstep(float val, float lim1, float lim2)
	{
		if (val < lim1) return 0;
		if (val >= lim2) return 1;
		float x = clamp<float>( (val - lim1)/(lim2 - lim1), 0, 1);
		return (x * x * (3 - 2 * x));
	}

	float static RandomFloatInRange(float min, float max)
	{
		float random = (float)(rand()) / (float)(RAND_MAX);
		if (min < 0 && max < 0)
		{
			float diff = abs(max) - abs(min);
			float r = random * diff;
			return -(min + r);
		}
		else
		{
			float diff = (max) - (min);
			float r = random * diff;
			return (min + r);
		}
	}

	float static RandFloat(float max = 1.0f)
	{
		return (float)((rand() % RAND_MAX) / (float)(RAND_MAX)) * max;
	}

	float static RandBetween(float min, float max)
	{
		return min + RandFloat() * (max - min);
	}
};

#endif