#include "Vector4.h"

Vector4::Vector4()
{
	x = y = z = 0;
	w = 1;
}

Vector4::Vector4(float a_XYZ)
{
	x = y = z = a_XYZ;
	w = 1;
}

Vector4::Vector4(float a_X, float a_Y, float a_Z)
{
	x = a_X;
	y = a_Y;
	z = a_Z;
	w = 1;
}

Vector4::Vector4(float a_X, float a_Y, float a_Z, float a_W)
{
	x = a_X;
	y = a_Y;
	z = a_Z;
	w = a_W;
}

Vector4::Vector4(const Vector2& a_Other, float a_Z, float a_W)
{
	x = a_Other.x;
	y = a_Other.y;
	z = a_Z;
	w = a_W;
}

Vector4::Vector4(const Vector3& a_Other, float a_W)
{
	x = a_Other.x;
	y = a_Other.y;
	z = a_Other.z;
	w = a_W;
}

Vector4::Vector4(const Vector4& a_Other)
{
	x = a_Other.x;
	y = a_Other.y;
	z = a_Other.z;
	w = a_Other.w;
}

Vector4 Vector4::operator+(const Vector4& a)
{
	return Vector4(x + a.x, y + a.y, z + a.z, 1);
}

Vector4& Vector4::operator+=(const Vector4& a)
{
	*this = *this + a;
	return *this;
}

Vector4 Vector4::operator-(const Vector4& a)
{
	return Vector4(x - a.x, y - a.y, z - a.z, 1);
}

Vector4& Vector4::operator-=(const Vector4& a)
{
	*this = *this - a;
	return *this;
}

Vector4& Vector4::operator=(const Vector4& a)
{
	x = a.x;
	y = a.y;
	z = a.z;
	w = a.w;
	return *this;
}

Vector4 Vector4::operator*(float a_Scalar)
{
	return Vector4(x * a_Scalar, y * a_Scalar, z * a_Scalar, w);
}

Vector4& Vector4::operator*=(float a_Scalar)
{
	this->x *= a_Scalar;
	this->y *= a_Scalar;
	this->z *= a_Scalar;
	return *this;
}

bool Vector4::operator==(const Vector4& a_Other) 
{ 
    if ( x - a_Other.x == 0 && y - a_Other.y == 0 && z - a_Other.z == 0 ) 
        return true; 
    return false; 
} 

float* Vector4::ToArray()
{
	float* temp = new float[4];
	temp[0] = x;
	temp[1] = y;
	temp[2] = z;
	temp[3] = w;
	return temp;
}

float Vector4::Dot(const Vector4& a_Other)
{
	return (x * a_Other.x + y * a_Other.y + z * a_Other.z);
}

float Vector4::Magnitude()
{
	return sqrtf( SquaredMagnitude() );
}

float Vector4::SquaredMagnitude()
{
	return ((x * x) + (y * y) + (z * z));
}

float Vector4::Distance(const Vector4& a_Other)
{
	return sqrtf(SquaredDistance(a_Other));
}

float Vector4::SquaredDistance(const Vector4& a_Other)
{
	float xDiff = a_Other.x - x;
	float yDiff = a_Other.y - y;
	float zDiff = a_Other.z - z;
	return ( (xDiff * xDiff) + (yDiff * yDiff) + (zDiff * zDiff) );
}

//a x b != b x a
//a in this case is the current vector, b is the other vector coming in
Vector4 Vector4::Cross(const Vector4& a_Other)
{
	//			   a2 * b3 - a3 * b2			 a3 * b1 - a1 * b3				a1 * b2 - a2 * b1
	return Vector4(y * a_Other.z - z * a_Other.y, z * a_Other.x - x * a_Other.z, x * a_Other.y - y * a_Other.x);
}

float Vector4::AngleBetween(const Vector4& a_Other, bool inRadians /*= true*/)
{
	float angle = acos(this->Dot(a_Other));
	float perpAngle = this->Cross(a_Other).Dot(a_Other);
	if (perpAngle < 0)
	{
		angle = -(abs(angle));
	}
	else
	{
		angle = abs(angle);
	}

	if (inRadians)
	{
		return angle;
	}
	else
	{
		return Rad2Deg * angle;
	}
}

Vector4 Vector4::Lerp(Vector4 a_Start, Vector4 a_End, float a_Time)
{
	if (a_Time > 1)
	{
		return a_End;
	}
	else if (a_Time < 0)
	{
		return a_Start;
	}
	return (a_Start + (a_End - a_Start) * a_Time);
}

Vector4 Vector4::CubeBezier(Vector4 a_Start, Vector4 a_Mid, Vector4 a_End, float a_Time)
{
	Vector4 mid1 = Lerp(a_Start, a_Mid, a_Time);
	Vector4 mid2 = Lerp(a_Mid, a_End, a_Time);

	return Lerp(mid1, mid2, a_Time);
}

Vector4 Vector4::HermiteSpline(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Tangent1, Vector4 a_Tangent2, float a_Time)
{
	float tsq = a_Time * a_Time;
	float tcub = tsq * a_Time;

	float h00 = 2 * tcub - 3 * tsq + 1;
	float h01 = -2 * tcub + 3 * tsq;

	float h10 = tcub - 2 * tsq + a_Time;
	float h11 = tcub - tsq;

	return (a_Point1 * h00 + a_Tangent1 * h10 + a_Point2 * h01 + a_Tangent2 * h11);
}

//tightness controls how sharp the curves are
Vector4 Vector4::CardinalSpline(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Point3, float a_Tightness, float a_Time)
{
	Vector4 tangent1 = (a_Point2 - a_Point1) * a_Tightness;
	Vector4 tangent2 = (a_Point3 - a_Point2) * a_Tightness;

	float tsq = a_Time * a_Time;
	float tcub = tsq * a_Time;

	float h00 = 2 * tcub - 3 * tsq + 1;
	float h01 = -2 * tcub + 3 * tsq;
	float h10 = tcub - 2 * tsq + a_Time;
	float h11 = tcub - tsq;

	return (a_Point1 * h00 + tangent1 * h10 + a_Point2 * h01 + tangent2 * h11);
}

Vector4 Vector4::CatmullRom(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Point3, float a_Time)
{
	return CardinalSpline(a_Point1, a_Point2, a_Point3, 0.5f, a_Time);
}
