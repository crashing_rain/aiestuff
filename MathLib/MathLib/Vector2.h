#ifndef _VECTOR2_H
#define _VECTOR2_H

#include "Constants.h"
#include <vector>

class Vector2
{
public:
	Vector2();
	Vector2(float a_X, float a_Y);
	Vector2(float a_XY);
	Vector2(const Vector2& a_Other);

	Vector2& operator=(const Vector2& a_Other);
	Vector2 operator+(const Vector2& a_Other);
	Vector2 operator-(const Vector2& a_Other);

	//Calculate perpendicular of current vector
	Vector2 Perp();

	Vector2 operator*(float a_Scalar);

	void Normalise();

	Vector2 GetNormal();

	float* ToArray();

	//Dot prod
	float Dot(const Vector2& a_Other);

	//Magnitude - distance from origin
	float SquaredMagnitude();

	float Magnitude();

	//Not square root - dist between two points
	float SquaredDistance(const Vector2& a_Other);

	//Square root
	float Distance(const Vector2& a_Other);

	float AngleBetween(const Vector2& a_Other, bool inRadians = true /*= true*/);

	bool operator==(const Vector2& a_Other);
	Vector2& operator+=(const Vector2& a_Other);
	Vector2& operator-=(const Vector2& a_Other);

	static Vector2 Lerp(Vector2 a_Start, Vector2 a_End, float a_Time);
	static Vector2 QuadBezier(Vector2 a_Start, Vector2 a_Mid, Vector2 a_End, float a_Time);
	static Vector2 CubeBezier(Vector2 a_Start, Vector2 a_Mid1, Vector2 a_Mid2, Vector2 a_End, float a_Time);
	static Vector2 HermiteSpline(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Tangent1, Vector2 a_Tangent2, float a_Time);
	static Vector2 CardinalSpline(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Point3, float a_Tightness, float a_Time);
	static Vector2 CatmullRom(Vector2 a_Point1, Vector2 a_Point2, Vector2 a_Point3, float a_Time);
	static Vector2 Bezier(std::vector<Vector2> points, float a_Time);

	friend std::ostream& operator<<(std::ostream &output, const Vector2& a_Vector)
	{
		output << "X: " << a_Vector.x << " Y: " << a_Vector.y;
		return output;
	}

	float x,y;
};

#endif