#include "Bezier.h"

Bezier::Bezier()
{
	points = nullptr;
	tangents = nullptr;
}

void Bezier::Destroy()
{
	if (points != nullptr)
	{
		delete [] points;
	}

	if (tangents != nullptr)
	{
		delete [] tangents;
	}
}

void Bezier::AddPoint(const Vector3& point, const Vector3& tangent)
{
	pointsOnCurve.push_back(point);
	tangentsOnCurve.push_back(tangent);
}

Bezier::~Bezier()
{
	Destroy();
}

void Bezier::StartCreation()
{
	Destroy();
}

void Bezier::EndCreation()
{
	count = pointsOnCurve.size();
	points = new Vector3[count];
	tangents = new Vector3[count];

	int pointOffset = 0;

	for (pointOffset; pointOffset < count; ++pointOffset)
	{
		points[pointOffset] = *(pointsOnCurve.begin() + pointOffset);
	}

	pointOffset = 0;
	for (pointOffset; pointOffset < count; ++pointOffset)
	{
		tangents[pointOffset] = *(tangentsOnCurve.begin() + pointOffset);
	}

	pointsOnCurve.clear();
	tangentsOnCurve.clear();
}

void Bezier::GetPoint(float position, Vector3* result)
{
	if (position > 1.0f)
	{
		*result = Vector3(0);
		return;
	}

	float currVal = position * (count - 1);
	const int index = (int)(floorf(position * (count - 1)));

	float t = currVal - index;
	float t2 = t * t;
	float t3 = t2 * t;

	Vector3 p0 = points[index];
	Vector3 p1 = tangents[index];
	Vector3 p2 = tangents[index + 1];
	Vector3 p3 = points[index + 1];

	*result = Vector3(p0 * (1 - t) * (1 - t) * (1 - t) + p1 * 3 * t * (1 - t) * (1 - t) + p2 * 3 * t2 * (1 - t) + p3 * t3);
}

void Bezier::PrintData()
{
	if (pointsOnCurve.size() > 0 && tangentsOnCurve.size() > 0)
	{
		std::vector<Vector3>::iterator vIter;
		int pointOffset = 0;
		for (vIter = pointsOnCurve.begin(); vIter != pointsOnCurve.end(); ++vIter)
		{
			std::cout << "POINT: " << pointOffset++ << *vIter << std::endl;
		}
		pointOffset = 0;
		for (vIter = tangentsOnCurve.begin(); vIter != tangentsOnCurve.end(); ++vIter)
		{
			std::cout << "TANGENT: " << pointOffset++ << *vIter << std::endl;
		}
	}
}
