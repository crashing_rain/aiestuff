#ifndef _VECTOR4_H
#define _VECTOR4_H

#include "Constants.h"
#include "Vector2.h"
#include "Vector3.h"

class Vector4
{
public:
	Vector4();
	Vector4(float a_XYZ);
	Vector4(float a_X, float a_Y, float a_Z);
	Vector4(float a_X, float a_Y, float a_Z, float a_W);
	Vector4(const Vector2& a_Other, float a_Z, float a_W);
	Vector4(const Vector3& a_Other, float a_W);
	Vector4(const Vector4& a_Other);

	Vector4 operator+(const Vector4& a);
	Vector4& operator+=(const Vector4& a);
	Vector4 operator-(const Vector4& a_Other);
	Vector4& operator-=(const Vector4& a);

	Vector4 operator*(float a_Scalar);
	Vector4& operator*=(float a_Scalar);

	Vector4& operator=(const Vector4& a);

	bool operator==(const Vector4& a_Other);

	float Dot(const Vector4& a_Other);

	float Magnitude();
	float SquaredMagnitude();

	float Distance(const Vector4& a_Other);
	float SquaredDistance(const Vector4& a_Other);

	float* ToArray();

	//a x b != b x a
	//a in this case is the current vector, b is the other vector coming in
	Vector4 Cross(const Vector4& a_Other);
	float AngleBetween(const Vector4& a_Other, bool inRadians = true /*= true*/);

	static Vector4 Lerp(Vector4 a_Start, Vector4 a_End, float a_Time);
	static Vector4 CubeBezier(Vector4 a_Start, Vector4 a_Mid, Vector4 a_End, float a_Time);
	static Vector4 HermiteSpline(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Tangent1, Vector4 a_Tangent2, float a_Time);
	static Vector4 CardinalSpline(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Point3, float a_Tightness, float a_Time);
	static Vector4 CatmullRom(Vector4 a_Point1, Vector4 a_Point2, Vector4 a_Point3, float a_Time);

	friend std::ostream& operator<<(std::ostream& output, const Vector4& a_Other) 
    { 
        output << "X: " << a_Other.x << " Y: " << a_Other.y << " Z: " << a_Other.z << " W: " << a_Other.w; 
        return output; 
    }

	float x, y, z, w;
};

#endif