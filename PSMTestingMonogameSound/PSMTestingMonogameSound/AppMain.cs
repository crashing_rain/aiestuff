using System;
using System.Collections.Generic;

namespace PSMTestingMonogameSound
{
	public class AppMain
	{		
		public static void Main (string[] args)
		{
			using (var game = new Game1())
			{
				game.Run();
			}
		}
	}
}
