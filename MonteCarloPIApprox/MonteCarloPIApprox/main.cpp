#include <iostream>
#include <time.h>
#include <random>
#include <functional>

//Square 2 x 2
//Area = 2 * r * r
//Circle inscribed within
//Radius is 1
//Area = pi * r * r
//Proportion of points inside the circle
//will converge to the area of the circle
//relative to the area of the square
void main()
{
	bool naive = false;
	srand((unsigned int)(time(NULL)));
	auto r = std::bind(std::uniform_real_distribution<double>(-1.0000, 1.0000), std::mt19937_64((unsigned int)(time(NULL))));

	int totalPoints = 25000000;
	int pointsInside = 0;

	for (int i = 0; i < totalPoints; ++i)
	{
		double randPointX = 0.0;
		double randPointY = 0.0;

		if (naive)
		{
			randPointX = (((rand() / (double)RAND_MAX) * 2.0) - 1.0);
			randPointY = (((rand() / (double)RAND_MAX) * 2.0) - 1.0);
		}
		else
		{
			randPointX = r();
			randPointY = r();
		}

		double dist = randPointX * randPointX + randPointY * randPointY;

		//if (dist <= 1.0000)
		if (dist - 1.0000 <= -0.0001 && 1.0000 - dist >= 0.0001)
		{
			pointsInside++;
		}
	}

	double pi = 4.0 * (pointsInside / (double)(totalPoints));
	std::cout << pi << std::endl;

	system("pause");
}