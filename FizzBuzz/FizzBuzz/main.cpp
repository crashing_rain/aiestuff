#include <iostream>

void main()
{
	//1 - 100
	//multiples of 3, print fizz
	//multiples of 5, print buzz
	//multiples of 3 & 5, print fizz buzz
	//otherwise print number

	int* numbers = new int[100];
	for (int i = 0; i < 100; ++i)
	{
		numbers[i] = i + 1;
	}

	for (int i = 0; i < 100; ++i)
	{
		if (numbers[i] % 3 == 0 && numbers[i] % 5 == 0)
		{
			std::cout << "FizzBuzz" << std::endl;
		}
		else
		{
			if (numbers[i] % 3 == 0)
			{
				std::cout << "Fizz" << std::endl;
			}
			else if (numbers[i] % 5 == 0)
			{
				std::cout << "Buzz" << std::endl;
			}
			else
			{
				std::cout << numbers[i] << std::endl;
			}
		}
	}

	delete numbers;

	system("pause");
}