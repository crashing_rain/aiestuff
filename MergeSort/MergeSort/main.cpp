#include <Windows.h>
#include <iostream>
#include <time.h>
#include <math.h>

void Swap(int* numbers, int i, int j)
{
	int temp = numbers[i];
	numbers[i] = numbers[j];
	numbers[j] = temp;
}

//assume upper and lower bounds are correctly specified when function is used
//high is also number of elements in array passed in
//merges lo -> mid with mid + 1 -> high
void Merge(int* numbers, int lo, int mid, int high)
{
	//assignging temp variables to hold
	//arguments passed in
	int min = lo;
	int midP = mid + 1;
	
	//array for temp data
	int* temp = new int[high];

	//copy everything into temp array
	for (int i = lo; i <= high; ++i)
	{
		temp[i] = numbers[i];
	}

	//go from specified beginning to specified end
	for (int i = lo; i <= high; ++i)
	{
		//lower bounds to mid checks
		if (min > mid)
		{
			//increase middle bounds
			numbers[i] = temp[midP++];
		}
		//middle to upper bounds
		else if (midP > high)
		{
			numbers[i] = temp[min++];
		}
		//current on right vs current on left less than
		else if (temp[midP] < temp[min])
		{
			numbers[i] = temp[midP++];
		}
		//current on right vs current on left greater than
		else
		{
			numbers[i] = temp[min++];
		}
	}
}

//compareTo: 0 = the same
//			<0 = lexographically greater
//			>0 = lexographically smaller
void InsertionSort(int* numbers, int size)
{
	for (int i = 0; i < size; ++i)
	{
		//for (int j = i; j > 0 && numbers[j] == numbers[j - 1]; --j)
		for (int j = i; j > 0 && numbers[j] < numbers[j - 1]; --j)
		{
			Swap(numbers, j, j - 1);
		}
	}
}

//travels down to base case, before merging
void TDSort(int* numbers, int lo, int hi)
{
	//base case to finish recursive call
	if (hi <= lo) return;

	int mid = (int)(lo + (hi - lo) * 0.5f);
	int size = hi - lo;

	//if (size > 7)
	//{
	//	//division of large problem into sub problems
	//	//first half
	//	TDSort(numbers, lo, mid);
	//	//second half
	//	TDSort(numbers, mid + 1, hi);
	//}
	//else
	//{
	//	//smaller array, so use simple
	//	//sort to speed things up
	//	InsertionSort(numbers, size);
	//}
	
	//division of large problem into sub problems
	//first half
	TDSort(numbers, lo, mid);
	//second half
	TDSort(numbers, mid + 1, hi);

	//look at two halves and merge them,
	//sorting along the way
	Merge(numbers, lo, mid, hi);
}

//performs merge on groups
void BUSort(int* numbers, int size)
{
	//passes over whole array
	for (int i = 1; i < size; i *= 2) //sub array size
	{
		for (int j = 0; j < size - i; j += i * 2) //sub array index
		{
			Merge(numbers, j, i + j - 1, (j + 2 * i - 1 < size - 1) ? (j + 2 * i - 1) : (size - 1));
		}
	}
}

void main()
{
	using namespace std;

	srand((unsigned int)(time(NULL)));

	LARGE_INTEGER freq;
	LARGE_INTEGER startTime;
	LARGE_INTEGER endTime;

	QueryPerformanceFrequency(&freq);

	const int nums = 100;
	int* numbers = new int[nums];

	for (int i = 0; i < nums; ++i)
	{
		numbers[i] = (int)((rand() / (float)(RAND_MAX)) * nums);
	}

	QueryPerformanceCounter(&startTime);
	TDSort(numbers, 0, nums);
	QueryPerformanceCounter(&endTime);
	std::cout << "Total time for TD Sort: " << ((double)(endTime.QuadPart - startTime.QuadPart) / (double)(freq.QuadPart)) << std::endl;

	system("pause");

	for (int i = 0; i < nums; ++i)
	{
		numbers[i] = (int)(rand() / (float)(RAND_MAX) * nums);
	}

	QueryPerformanceCounter(&startTime);
	BUSort(numbers, nums);
	QueryPerformanceCounter(&endTime);
	std::cout << "Total time for BU Sort: " << ((double)(endTime.QuadPart - startTime.QuadPart) / (double)(freq.QuadPart)) << std::endl;

	system("pause");
}