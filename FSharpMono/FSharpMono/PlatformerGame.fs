﻿module PlatformerGame

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Input
open PlatformerActor
open PlatformerPhysics
open PlatformerInput

type Game1() as x =
   inherit Game()

   do x.Content.RootDirectory <- "Content"
   let graphics = new GraphicsDeviceManager(x)
   let screenWidth = graphics.PreferredBackBufferWidth
   let screenHeight = graphics.PreferredBackBufferHeight
   let mutable spriteBatch = Unchecked.defaultof<SpriteBatch>
   let CreateActor' = CreateActor x.Content

   //lazy - not evaluated immediately, evaluated only when result is needed, useful, since texture loading doesn't occur until later
   //WorldObjects = list of WorldActor objects
   let mutable WorldObjects = lazy ([("blankSquare.png", Player(Nothing), Vector2(100.0f, 28.0f), Vector2(32.0f, 32.0f), false, Color.Red);
                                     ("blankSquare.png", Obstacle, Vector2(100.0f, (float32 screenHeight) - 32.0f), Vector2((float32 screenWidth), 32.0f), true, Color.Black);
                                     ("blankSquare.png", Obstacle, Vector2(42.0f, 60.0f), Vector2(32.0f, 32.0f), true, Color.Yellow);]
                                     |> List.map CreateActor')

   override x.Initialize() =
      do spriteBatch <- new SpriteBatch(x.GraphicsDevice)
      do base.Initialize()
      ()

   override x.LoadContent() =
      //force calculation of the values
      //throw away results of calc
      do WorldObjects.Force () |> ignore
      ()

   override x.Update(gameTime) =
      //assign function with args passed in
      let thisAddGravity = AddGravity gameTime

      //save current objs to list
      let currentWorldObjects = WorldObjects.Value
      let currentHandleInput = HandleInput (Keyboard.GetState())

      //save values performed on current world objects to the new list of world objects resulting from calculations
      do WorldObjects <- lazy (currentWorldObjects |> List.map currentHandleInput |> List.map thisAddGravity |> HandleCollision |> List.map ResolveVelocities)
      do WorldObjects.Force () |> ignore
      ()

   override x.Draw(gameTime) =
      do x.GraphicsDevice.Clear Color.CornflowerBlue

      do spriteBatch.Begin()

      //get values of world objects list, pipe to list iterator
      //which executes lambda on all elements of list
      //check to see if the texture is something
      //draw it if it exists
      WorldObjects.Value |> List.iter (fun actor -> if actor.Texture.IsSome then do spriteBatch.Draw(actor.Texture.Value, actor.CurrentBounds, actor.Colour))
      
      do spriteBatch.End()
      ()
