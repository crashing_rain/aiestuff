﻿module PlatformerInput

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Input
open PlatformerActor

let HandleInput (kbState:KeyboardState) (actor:WorldActor) =
    let rec HandleKeys keys (currentVelocity:Vector2, state) =
        match keys with
        | [] -> currentVelocity
        | first :: rest -> match first with
                           | Keys.Left -> let newSpeed = if currentVelocity.X - 0.1f < -1.0f then
                                                           -1.f
                                                         else
                                                            currentVelocity.Y - 0.1f
                                          let newV = Vector2(newSpeed, currentVelocity.Y)
                                          HandleKeys rest (newV, state)
                           | Keys.Right -> let newSpeed = if currentVelocity.X + 0.1f > 1.0f then
                                                            1.0f
                                                          else
                                                            currentVelocity.X + 0.1f
                                           let newV = Vector2(newSpeed, currentVelocity.Y)
                                           HandleKeys rest (newV, state)
                           | _ -> HandleKeys rest (currentVelocity, state)
    match actor.ActorType with
    | Player s -> let initialVelocity = match actor.BodyType with
                                        | Dynamic v -> v
                                        | _ -> Vector2()
                  let velocity = HandleKeys (kbState.GetPressedKeys() |> Array.toList) (initialVelocity, s)
                  { actor with BodyType = Dynamic(velocity); ActorType = Player(Jumping) }
    | _ -> actor