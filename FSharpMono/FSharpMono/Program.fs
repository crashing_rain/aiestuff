﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open PlatformerGame

[<EntryPoint>]
let main argv = 
    use g = new Game1()
    g.Run()
    0 // return an integer exit code
