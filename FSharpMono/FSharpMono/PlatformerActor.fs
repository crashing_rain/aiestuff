﻿module PlatformerActor

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Content

//Discriminated unions
type BodyType =
| Static
| Dynamic of Vector2

type PlayerState =
| Nothing
| Jumping

type ActorType =
| Player of PlayerState
| Obstacle

//Represents an object in the world
type WorldActor =
   {
        //member variables
        ActorType : ActorType;
        Position : Vector2;
        Size : Vector2;
        Texture : Texture2D option;
        BodyType : BodyType;
        Colour : Color;
   }
   //member functions
   member this.CurrentBounds
      with get () = Rectangle((int this.Position.X), (int this.Position.Y), (int this.Size.X), (int this.Size.Y))

   //get desired bounds based on the body type
   //if it is dynamic, then the position is increased
   //by a specific value, forming a rectangle with those values
   member this.DesiredBounds
      with get () = let desiredPos = match this.BodyType with
                                        | Dynamic s -> this.Position + s
                                        | _ -> this.Position
                    Rectangle ((int desiredPos.X), (int desiredPos.Y), (int this.Size.X), (int this.Size.Y))

//function to create actors
let CreateActor (content:ContentManager) (textureName, actorType, position, size, isStatic, a_Colour) =
   //make sure that texture name passed in is valid
   let tex = if not (System.String.IsNullOrEmpty textureName) then
                Some(content.Load textureName)
             else
                None
   //assign body type information
   let bt = if isStatic then
               Static
            else
               Dynamic(Vector2(0.0f, 0.0f))
   //create world actor, assigning values across
   //simply assigning based on the type defined
   {
      ActorType = actorType;
      Position = position;
      Size = size;
      Texture = tex;
      BodyType = bt;
      Colour = a_Colour;
   }


