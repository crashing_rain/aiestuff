﻿module PlatformerPhysics

open Microsoft.Xna.Framework
open PlatformerActor

//args are game time and actor
let AddGravity (gameTime:GameTime) (actor:WorldActor) =
   let ms = gameTime.ElapsedGameTime.TotalMilliseconds
   let g = ms * 0.01

   match actor.BodyType with
   | Dynamic (s:Vector2) -> let d = Vector2(s.X, s.Y + (float32 g))
                            { actor with BodyType = Dynamic(d); }
   | _ -> actor

let ResolveVelocities (actor:WorldActor) =
   match actor.BodyType with
   //if the body type is dynamic, then, modify the actor's pos
   //by a vector2
   | Dynamic s -> { actor with Position = actor.Position + s * 0.5f }
   //in any other case, do nothing to the actor
   | _ -> actor

let IsActorStatic (actor:WorldActor) =
   match actor.BodyType with
   | Static -> true
   | _ -> false

//takes list splits up based on static-ness
//returns true in first list, false in second list
let PartitionWorldObjects a_WorldObjects =
     a_WorldObjects 
     |> List.partition IsActorStatic

//handle collisions
let HandleCollision worldObjects =
    //get static and non static objects in the world
    let stc, dyn = PartitionWorldObjects worldObjects
    
    let FindNewVelocity rect1 rect2 velocity =
        //check two rects against each other, intersection returns a 
        //rectangle representing the intersection of a and b
        let inter = Rectangle.Intersect(rect1,rect2)
        let mutable (newVel:Vector2) = velocity

        //height > width, colliding on X axis
        if inter.Height > inter.Width then
            do newVel.X <- 0.f
        //width > height, colliding on Y axis
        if inter.Width > inter.Height then
            do newVel.Y <- 0.f
        newVel
    
    //check two actors together
    let FindOptimumCollision (firstActor:WorldActor) (secondActor:WorldActor) =
        //look at actor types
        match firstActor.ActorType, secondActor.ActorType with
        //if it's player and obstacle and dynamic and static, then recreate first actor with a dynamic body type
        | Player(h), Obstacle -> match firstActor.BodyType, secondActor.BodyType with
                                                                                          //create dynamic type, find resulting collision velocity using current velocity s
                                    | Dynamic (s), Static -> { firstActor with BodyType = Dynamic((FindNewVelocity firstActor.DesiredBounds firstActor.CurrentBounds s)) }
                                    //any other case, leave first actor unmodified0
                                    | _ -> firstActor
        //any other case, leave first actor unmodified0
        | _ -> firstActor

    //check actor against sorted list of actors
    let rec FigureCollisions (actor:WorldActor) (sortedActors:WorldActor list) =
        match sortedActors with
        //case for empty array
        | [] -> actor
        //case for array with at least one element
        //does actor's desired path intersect with
        //anything in sorted list? If does, perf calcs
        //otherwise do nothing, recursive call
        | x :: xs -> let a = if actor.DesiredBounds.Intersects(x.DesiredBounds) then
                                 FindOptimumCollision actor x
                             else
                                 actor
                     //execute this function again, passing in a, which can either be
                     //the result of a collision, or the actor itself, without any modification
                     FigureCollisions a xs
    
    //look at collisions to fix and collisions that have already been fixed
    let rec FixCollisions (toFix:WorldActor list) (alreadyFixed:WorldActor list) =
        //match elements to be fixed
        match toFix with
        //empty array, return already fixed list
        | [] -> alreadyFixed
        //at least one elem, recursive call, call figure collisions func
        //passing in first elem of to fix list with those that have already
        //been fixed
        | x :: xs -> let a = FigureCollisions x alreadyFixed
                     FixCollisions xs (a::alreadyFixed)
    
    //call fix collisions to start off recursive call,
    //passing in dynamic and static objects
    FixCollisions dyn stc
   
