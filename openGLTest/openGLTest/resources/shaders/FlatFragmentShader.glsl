#version 400

smooth in vec4 vertColour;

out vec4 outputColour;

uniform float totalTime;

void main()
{
	outputColour = vertColour;
	//outputColour = vec4(vertColour.r, sin(vertColour.g) * totalTime, vertColour.b, 1);
}