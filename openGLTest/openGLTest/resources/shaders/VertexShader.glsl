#version 400

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 colour;
layout(location = 2) in vec2 vertexUV;

uniform mat4 MVP;

smooth out vec4 vertColour;
out vec2 UV;

uniform float totalTime;

void main()
{
	vertColour = colour;
    UV = vertexUV;
	vec4 offset = position;
	offset.y += 100 * sin(totalTime) - 10;
	offset.x -= 10 * cos(totalTime) - 100;
	mat4 translation = mat4(1,0,0,10,
							0,1,0,10,
							0,0,1,0,
							0,0,0,1);
	mat4 rotation = mat4(cos(totalTime), -sin(totalTime), 0, 0,
						  sin(totalTime), cos(totalTime), 0, 0,
						  0, 0, 1, 0,
						  0, 0, 0, 1);
	mat4 scale = mat4(sin(totalTime), 0,0,0,
					  0, sin(totalTime),0,0,
					  0,0, 1, 0,
					  0,0,0, 1);

    vec4 scaledPosition = vec4(vec2(MVP * scale * translation * rotation * offset), 0, 1);

	gl_Position = scaledPosition;
}
