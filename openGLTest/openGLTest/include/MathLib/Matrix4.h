#ifndef _MATRIX4_H
#define _MATRIX4_H

#include "Constants.h"
#include "Vector4.h"

class mat4
{
public:
	mat4();

	mat4(float m00, float m01, float m02, float m03, 
		 float m10, float m11, float m12, float m13, 
		 float m20, float m21, float m22, float m23, 
		 float m30, float m31, float m32, float m33);

	mat4(Vector4 a1, Vector4 a2, Vector4 a3, Vector4 a4);

	mat4 identity();

	//Modifies current matrix
	//But also returns transpose
	mat4& transpose();

	//Calcs transpose of this
	//Returning new mat3
	mat4 getTranspose();

	mat4& makeRotationX(float a_rads);
	mat4& makeRotationY(float a_rads);
	mat4& makeRotationZ(float a_rads);
	mat4& makeScaling(const Vector4& a_Scale);
	mat4& makeTranslation(const Vector4& a_Translation);

	//Generate new matrix and return it
	static mat4 setupRotationX(float a_rads);
	static mat4 setupRotationY(float a_rads);
	static mat4 setupRotationZ(float a_rads);
	static mat4 setupScaling(const Vector3& a_Scale);
	static mat4 setupTranslation(const Vector3& a_Translation);

	mat4 operator+(const mat4& a);
	mat4 operator-(const mat4& a);
	mat4 operator*(const mat4& a);

	mat4& operator*=(const mat4& a);
	mat4& operator+=(const mat4& a);
	mat4& operator-=(const mat4& a);

	Vector4 operator*(const Vector4& a);

	friend std::ostream& operator<<(std::ostream& output, const mat4& a_Other)
	{
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				output << a_Other.m[i][j] << " ";
			}
			output << std::endl;
		}
		return output;
	}

	float m[4][4];
};

#endif