#ifndef _VECTOR3_H
#define _VECTOR3_H

#include "Constants.h"

class Vector3
{
public:
	Vector3();
    Vector3(float a_XYZ);
    Vector3(float a_X, float a_Y, float a_Z);
    Vector3(const Vector3& a);
  
    Vector3 operator=(const Vector3& a);
    Vector3 operator+(const Vector3& a);  
    Vector3 operator-(const Vector3& a);

	Vector3& operator+=(const Vector3& a);
	Vector3& operator-=(const Vector3& a);
  
	float Dot(const Vector3& a_Other);

    //dist from origin 
    float Magnitude();
    float SquaredMagnitude();
  
    //dist between current and another point 
    float Distance(const Vector3& a);
    float SquaredDistance(const Vector3& a);
  
    void Normalise();
    Vector3 GetNormal();

	float AngleBetween(const Vector3& a_Other, bool inRadians = true /*= true*/);
  
    bool operator==(const Vector3& a_Other);
  
    Vector3 operator*(float a);

	//a x b != b x a
	//a in this case is the current vector, b is the other vector coming in
	Vector3 Cross(const Vector3& a_Other);
  
    friend std::ostream& operator<<(std::ostream& output, const Vector3& a_Other) 
    { 
        output << "X: " << a_Other.x << " Y: " << a_Other.y << " Z: " << a_Other.z; 
        return output; 
    }
  
    float x,y,z;
};

#endif