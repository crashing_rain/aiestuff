#include <iostream>
#include <glew.h>
#include <glfw3.h>
#include <vector>
#include <string>
#include <fstream>

struct Vertex
{
	float positions[4];
	float colours[4];
};

GLuint CreateShader(GLenum a_eShaderType, const char* a_strShaderFile)
{
	std::string strShaderCode;
	//open shader file
	std::ifstream shaderStream(a_strShaderFile);
	//if that worked ok, load file line by line
	if(shaderStream.is_open())
	{
		std::string Line = "";
		while(std::getline(shaderStream, Line))
		{
			strShaderCode += "\n" + Line;
		}
		shaderStream.close();
	}

	//convert to cstring
	char const *szShaderSourcePointer = strShaderCode.c_str();
	
	//create shader ID
	GLuint uiShader = glCreateShader(a_eShaderType);
	//load source code
	glShaderSource(uiShader, 1, &szShaderSourcePointer, NULL);

	//compile shader
	glCompileShader(uiShader);

	//check for compilation errors and output them
	GLint iStatus;
	glGetShaderiv(uiShader, GL_COMPILE_STATUS, &iStatus);
	if (iStatus == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(uiShader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(uiShader, infoLogLength, NULL, strInfoLog);

		const char *strShaderType = NULL;
		switch(a_eShaderType)
		{
		case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
		case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
		}

		fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
		delete[] strInfoLog;
	}

	return uiShader;
}

GLuint CreateProgram(const char *a_vertex, const char *a_frag)
{
	std::vector<GLuint> shaderList;

	shaderList.push_back(CreateShader(GL_VERTEX_SHADER, a_vertex));
	shaderList.push_back(CreateShader(GL_FRAGMENT_SHADER, a_frag));

	//create shader program ID
	GLuint uiProgram = glCreateProgram();

	//attach shaders
	for(auto shader = shaderList.begin(); shader != shaderList.end(); shader++)
		glAttachShader(uiProgram, *shader);

	//link program
	glLinkProgram(uiProgram);

	//check for link errors and output them
	GLint status;
	glGetProgramiv (uiProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetProgramiv(uiProgram, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetProgramInfoLog(uiProgram, infoLogLength, NULL, strInfoLog);
		fprintf(stderr, "Linker failure: %s\n", strInfoLog);
		delete[] strInfoLog;
	}

	for(auto shader = shaderList.begin(); shader != shaderList.end(); shader++)
	{
		glDetachShader(uiProgram, *shader);
		glDeleteShader(*shader);
	}

	return uiProgram;
}

float* getOrtho(float left, float right, float bottom, float top, float a_fNear, float a_fFar)
{
	//to correspond with mat4 in the shader
	//ideally this function would be part of your matrix class
	//however I wasn't willing to write your matrix class for you just to show you this
	//so here we are in array format!
	//add this to your matrix class as a challenge if you like!
	float* toReturn = new float[16];
	toReturn[0] = 2.0/(right - left);
	toReturn[1] = toReturn[2] = toReturn[3] = toReturn[4] = 0;
	toReturn[5] = 2.0/(top - bottom);
	toReturn[6] = toReturn[7] = toReturn[8] = toReturn[9] = 0;
	toReturn[10] = 2.0/(a_fFar - a_fNear);
	toReturn[11] = 0;
	toReturn[12] = -1*((right + left)/(right - left));
	toReturn[13] = -1*((top + bottom)/(top - bottom));
	toReturn[14] = -1*((a_fFar + a_fNear)/(a_fFar - a_fNear));
	toReturn[15] = 1;
	return toReturn;
}

int main()
{
	bool hasQuit = false;

	const int screenWidth = 1280;
	const int screenHeight = 720;

	//Points of the triangle
	const float trianglePositions[] =
	{
		100, 100, 0.0f, 1.0f,
		50, 250, 0.0f, 1.0f,
		100, 250, 0.0f, 1.0f,

		500, 100, 0, 1,
		600, 250, 0, 1,
		450, 700, 0, 1,
		230, 150, 0, 1,
		30, 340, 0, 1,
		300, 480, 0, 1,
	};

	//Colours of each point of the triangle
	const float triangleColours[] =
	{
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,

		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
	};

	//Initialise GLFW
	if(!glfwInit())
	{
		return -1;
	}

	//create a windowed mode window and it's OpenGL context
	GLFWwindow* window;
	window = glfwCreateWindow(screenWidth, screenHeight, "Hello World", NULL, NULL);
	if(!window)
	{
		glfwTerminate();
		return -1;
	}

	//make the window's context current
	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK)
	{
		// OpenGL didn't start-up! shutdown GLFW and return an error code
		glfwTerminate();
		return -1;
	}

	GLuint uiProgramFlat = CreateProgram("resources\\shaders\\VertexShader.glsl", "resources/shaders/FlatFragmentShader.glsl");
	GLuint MatrixIDFlat = glGetUniformLocation(uiProgramFlat, "MVP");
	float *orthographicProjection = getOrtho(0, screenWidth, 0, screenHeight, 0, 100);

	Vertex* triangleShape = new Vertex[3];
	triangleShape[0].positions[0] = 100;
	triangleShape[0].positions[1] = 200;
	triangleShape[1].positions[0] = 300;
	triangleShape[1].positions[1] = 400;
	triangleShape[2].positions[0] = 250;
	triangleShape[2].positions[1] = 250;
	for (int i = 0; i < 3; ++i)
	{
		triangleShape[i].positions[2] = 0;
		triangleShape[i].positions[3] = 1;
		triangleShape[i].colours[0] = 0;
		triangleShape[i].colours[1] = 1;
		triangleShape[i].colours[2] = 1;
		triangleShape[i].colours[3] = 1;
	}

	GLuint triangleVBO;
	glGenBuffers(1, &triangleVBO);

	if (triangleVBO != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, triangleVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 3, nullptr, GL_STATIC_DRAW);
		GLvoid* vBuffer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		memcpy(vBuffer, triangleShape, sizeof(Vertex) * 3);
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//loop until the user closes the window
	while(!glfwWindowShouldClose(window) && !hasQuit)
	{
		//draw code goes here
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			hasQuit = true;
		}

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//enable shaders
		glUseProgram(uiProgramFlat);

		//send our orthographic projection info to the shader
		glUniformMatrix4fv(MatrixIDFlat, 1, GL_FALSE, orthographicProjection);
		glUniform1f(glGetUniformLocation(uiProgramFlat, "totalTime"), glfwGetTime());

		//enable the vertex array state, since we're sending in an array of vertices
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		//specify where our vertex array is, how many components each vertex has, 
		//the data type of each component and whether the data is normalised or not
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, trianglePositions);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, triangleColours);

		//draw to the screen
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_TRIANGLE_FAN, 3, 6);

		glBindBuffer(GL_ARRAY_BUFFER, triangleVBO);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		//specify where our vertex array is, how many components each vertex has, 
		//the data type of each component and whether the data is normalised or not
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)(sizeof(float) * 4));

		glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//swap front and back buffers
		glfwSwapBuffers(window);

		//poll for and process events
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}