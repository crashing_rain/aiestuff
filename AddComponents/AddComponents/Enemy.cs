﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddComponents
{
    public class Enemy : Entity
    {
        public Enemy(Game t, Vector2 a_Position, Vector2 a_Dimensions, float a_MovementSpeed)
            : base(t, a_Position, a_Dimensions, Color.Black, 50, "blankSquare")
        {
            
        }
    }
}
