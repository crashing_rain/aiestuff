﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddComponents
{
    public class Player : Entity
    {
        public Player(Game t, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour, float a_MovementSpeed)
            : base(t, a_Position, a_Dimensions, a_Colour, 50, "blankSquare")
        {
            
        }
    }
}
