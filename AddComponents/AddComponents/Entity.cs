﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddComponents
{
    public class Entity : DrawableGameComponent
    {
        protected Vector2 m_Position;
        protected Vector2 m_StartingPos;
        protected Vector2 m_Direction;
        protected Vector2 m_Dimensions;
        protected Texture2D m_Texture;
        protected Color m_Colour;
        protected float m_MovementSpeed;
        protected string m_TextureName;

        public Entity(Game a_Game, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour, float a_MovementSpeed, string a_TextureName)
            : base(a_Game)
        {
            m_Position = a_Position;
            m_Dimensions = a_Dimensions;
            m_Colour = a_Colour;
            m_MovementSpeed = Game1.cryptoRand.Next(50, 100);
            m_Direction = new Vector2((float)Game1.cryptoRand.NextDouble(), (float)Game1.cryptoRand.NextDouble());
            m_TextureName = a_TextureName;
            m_StartingPos = a_Position;
        }

        protected override void LoadContent()
        {
            m_Texture = Game.Content.Load<Texture2D>(m_TextureName);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (m_Position.X < 0 || m_Position.Y < 0 || m_Position.X > Game1.SCREEN_WIDTH || m_Position.Y > Game1.SCREEN_HEIGHT)
            {
                ResetPosition();
            }

            m_Position += m_Direction * m_MovementSpeed * (float)(gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Game1.spriteBatch.Begin();
            Game1.spriteBatch.Draw(m_Texture, GetRectangle, m_Colour);
            Game1.spriteBatch.End();

            base.Draw(gameTime);
        }

        public Rectangle GetRectangle
        {
            get { return new Rectangle((int)(m_Position.X), (int)m_Position.Y, (int)m_Dimensions.X, (int)m_Dimensions.Y); }
        }

        public void ResetPosition()
        {
            m_Direction = new Vector2((float)Game1.cryptoRand.NextDouble(), (float)Game1.cryptoRand.NextDouble()) * ((float)Game1.cryptoRand.NextDouble() > 0.5f ? -1 : 1);
            m_MovementSpeed = Game1.cryptoRand.Next(50, 100);
            m_Position = m_StartingPos + new Vector2((float)Game1.cryptoRand.NextDouble(), (float)Game1.cryptoRand.NextDouble()) * m_MovementSpeed;
        }
    }
}
