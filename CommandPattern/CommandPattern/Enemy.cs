﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    public class Enemy : MovableEntity
    {
        static int ID = 0;

        public Enemy(Texture2D a_Texture, Vector2 a_Position)
            : base(a_Texture, a_Position, new Vector2(28, 28), Color.Red)
        {
            m_TurnSpeed = 75;
            m_MaxEnergyLevel = m_TurnSpeed * 5.0f;
            m_Name = "Enemy" + ID++;
        }

        public override void Update(float gt)
        {
            m_Velocity = Vector2.Zero;

            List<Command> commands = InputHandler.Instance.GenerateEnemyInputs();
            if (commands.Count > 0)
            {
                foreach (Command n in commands)
                {
                    n.Execute(this);
                }
            }

            base.Update(gt);
        }

        public override void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            a_SB.DrawString(a_SF, m_Name, m_Position + new Vector2(0, -20), Color.Black);
            base.Draw(a_SB, a_SF);
        }
    }
}
