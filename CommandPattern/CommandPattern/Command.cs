﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    public abstract class Command
    {
        public abstract void Execute(MovableEntity a_DrawableEntity);
    }

    public class MoveLeftCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveLeft();
        }
    }

    public class MoveRightCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveRight();
        }
    }

    public class MoveUpCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveUp();
        }
    }

    public class MoveDownCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveDown();
        }
    }

    public class MoveRandomCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveRandom();
        }
    }

    public class MoveTowardTarget : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            a_DrawableEntity.MoveToTarget();
        }
    }

    public class ShootCommand : Command
    {
        public override void Execute(MovableEntity a_DrawableEntity)
        {
            //a_DrawableEntity.Shoot();
        }
    }
}
