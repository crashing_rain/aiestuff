﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    public class MovableEntity : DrawableEntity
    {
        public Vector2 currentTarget;
        protected float m_TurnSpeed;
        protected float m_MaxTurnSpeed = 10;

        protected float m_CurrentEnergyLevel = 0;
        protected float m_MaxEnergyLevel;

        public MovableEntity(Texture2D a_Texture, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour)
            : base(a_Texture, a_Position, a_Dimensions, a_Colour, 25)
        {
            currentTarget = new Vector2((float)Game1.rand.NextDouble() * Game1.g_ScreenWidth, (float)Game1.rand.NextDouble() * Game1.g_ScreenHeight);
        }

        public override void Update(float gt)
        {
            if (Vector2.Distance(m_Position, currentTarget) < GetRectangle.Width)
            {
                currentTarget = new Vector2((float)Game1.rand.NextDouble() * Game1.g_ScreenWidth, (float)Game1.rand.NextDouble() * Game1.g_ScreenHeight);
            }

            m_Position += m_Velocity * m_CurrentMovementSpeed;

            base.Update(gt);
        }

        public override void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            base.Draw(a_SB, a_SF);
        }

        public void ResetVelocity()
        {
            m_Velocity = Vector2.Zero;
        }

        public float EnergyLevel
        {
            get { return m_CurrentEnergyLevel; }
            set { m_CurrentEnergyLevel = value; }
        }

        public float MaxEnergyLevel
        {
            get { return m_MaxEnergyLevel; }
            set { m_MaxEnergyLevel = value; }
        }

        public float TurnSpeed
        {
            get { return m_TurnSpeed; }
        }

        public void MoveLeft()
        {
            m_Velocity += new Vector2(-1, 0);
        }

        public void MoveRight()
        {
            m_Velocity += new Vector2(1, 0);
        }

        public void MoveUp()
        {
            m_Velocity += new Vector2(0, -1);
        }

        public void MoveDown()
        {
            m_Velocity += new Vector2(0, 1);
        }

        public void MoveRandom()
        {
            m_Velocity += new Vector2((float)Game1.rand.NextDouble() * 2 - 1, (float)Game1.rand.NextDouble() * 2 - 1);
        }

        public void MoveToTarget()
        {
            Vector2 dir = currentTarget - m_Position;
            dir.Normalize();

            m_Velocity += dir;
        }
    }
}
