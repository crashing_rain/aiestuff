﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    //No input for movement here, can be moved manually
    public class DrawableEntity : Entity
    {
        protected Texture2D m_Texture;
        protected Color m_Colour;

        protected Vector2 m_Velocity;
        protected float m_CurrentMovementSpeed;

        public DrawableEntity(Texture2D a_Texture, Vector2 a_Position, Vector2 a_Dimensions, Color a_Colour, float a_MovementSpeed)
            : base(a_Position, a_Dimensions)
        {
            m_Texture = a_Texture;
            m_Colour = a_Colour;
            m_CurrentMovementSpeed = a_MovementSpeed;
            m_Velocity = Vector2.Zero;
        }

        public override void Update(float gt)
        {
            base.Update(gt);
        }

        public virtual void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            if (IsActive)
            {
                a_SB.Draw(m_Texture, GetRectangle, m_Colour);
            }
        }
    }
}
