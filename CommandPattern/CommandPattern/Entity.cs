﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    //Non-drawable entity
    public class Entity
    {
        protected Vector2 m_Position;
        protected Vector2 m_Dimensions;
        protected Vector2 m_StartingPosition;
        protected bool m_IsActive;

        protected string m_Name;

        public Entity(Vector2 a_Position, Vector2 a_Dimensions)
        {
            m_Position = a_Position;
            m_Dimensions = a_Dimensions;
            m_StartingPosition = m_Position;
            m_IsActive = true;
            m_Name = "Default";
        }

        public virtual void Update(float gt)
        {

        }

        public Vector2 Position
        {
            get { return m_Position; }
        }

        public bool IsActive
        {
            get { return m_IsActive; }
        }

        public Rectangle GetRectangle
        {
            get { return new Rectangle((int)m_Position.X, (int)m_Position.Y, (int)m_Dimensions.X, (int)m_Dimensions.Y); }
        }

        public string Name
        {
            get { return m_Name; }
        }
    }
}
