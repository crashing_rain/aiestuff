﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CommandPattern
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch sb;
        SpriteFont sf;

        public static Random rand = new Random();
        public static int g_ScreenWidth = 1280;
        public static int g_ScreenHeight = 720;
        public static Texture2D blankSquare;

        public static KeyboardState currentKeyboardState;
        public static KeyboardState previousKeyboardState;

        Player mainPlayer;

        List<MovableEntity> movingEntities = new List<MovableEntity>();

        int currentEntity = 0;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferWidth = g_ScreenWidth;
            graphics.PreferredBackBufferHeight = g_ScreenHeight;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            sb = new SpriteBatch(GraphicsDevice);
            sf = Content.Load<SpriteFont>("SpriteFont1");

            // TODO: use this.Content to load your game content here
            blankSquare = Content.Load<Texture2D>("blankSquare");

            for (int i = 0; i < 15; ++i)
            {
                movingEntities.Add(new Enemy(blankSquare, new Vector2((float)rand.NextDouble() * g_ScreenWidth, (float)rand.NextDouble() * g_ScreenHeight)));
            }

            //mainPlayer and Player variable in movingEntities points to same object
            mainPlayer = new Player(blankSquare, new Vector2(350, 350));
            movingEntities.Add(mainPlayer);

            movingEntities.Sort( (first, second) =>
                {
                    return second.TurnSpeed.CompareTo(first.TurnSpeed);
                }
            );
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            currentKeyboardState = Keyboard.GetState();

            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //can only act if energy level is right
            if (movingEntities[currentEntity].EnergyLevel > movingEntities[currentEntity].MaxEnergyLevel)
            {
                double t = rand.NextDouble();

                if (movingEntities[currentEntity].GetType() == typeof(Enemy))
                {
                    if (t > 0.9)
                    {
                        movingEntities[currentEntity].currentTarget = mainPlayer.Position;
                    }
                    else
                    {
                        movingEntities[currentEntity].currentTarget = new Vector2((float)rand.NextDouble() * g_ScreenWidth, (float)rand.NextDouble() * g_ScreenHeight);
                    }
                }

                movingEntities[currentEntity].Update(dt);

                if (!InputHandler.Instance.PlayerHasMoved)
                {
                    return;
                }

                movingEntities[currentEntity].EnergyLevel = 0;
            }

            movingEntities[currentEntity].EnergyLevel += (float)(movingEntities[currentEntity].TurnSpeed);

            currentEntity = (currentEntity + 1) % movingEntities.Count;

            previousKeyboardState = currentKeyboardState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            sb.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            foreach (MovableEntity n in movingEntities)
            {
                n.Draw(sb, sf);
            }

            sb.DrawString(sf, movingEntities[currentEntity].Name + "'s turn", new Vector2(50, 50), Color.Yellow);

            sb.Draw(blankSquare, new Rectangle(50, 100, (int)(((movingEntities[currentEntity].EnergyLevel / movingEntities[currentEntity].MaxEnergyLevel)) * 100), 25), Color.Black);            

            sb.End();

            base.Draw(gameTime);
        }
    }
}
