﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    public class Player : MovableEntity
    {
        public Player(Texture2D a_Texture, Vector2 a_Position)
            : base(a_Texture, a_Position, new Vector2(32, 32), Color.Blue)
        {
            m_TurnSpeed = 100;
            m_MaxEnergyLevel = m_TurnSpeed * 2.5f;
            m_Name = "Player";
        }

        public override void Update(float gt)
        {
            m_Velocity = Vector2.Zero;

            List<Command> commands = InputHandler.Instance.HandlePlayerInput();
            if (commands.Count > 0)
            {
                foreach (Command n in commands)
                {
                    n.Execute(this);
                }
            }

            base.Update(gt);
        }

        public override void Draw(SpriteBatch a_SB, SpriteFont a_SF)
        {
            a_SB.DrawString(a_SF, m_Name, m_Position + new Vector2(0, -20), Color.Black);
            base.Draw(a_SB, a_SF);
        }
    }
}
