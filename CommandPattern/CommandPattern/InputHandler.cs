﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandPattern
{
    public class InputHandler
    {
        Dictionary<Keys, Command> inputs = new Dictionary<Keys, Command>();

        private static InputHandler instance;

        public bool PlayerHasMoved
        {
            get;
            private set;
        }

        public static InputHandler Instance
        {
            get { return instance == null ? instance = new InputHandler() : instance; }
        }

        private InputHandler()
        {
            inputs.Add(Keys.W, new MoveUpCommand());
            inputs.Add(Keys.S, new MoveDownCommand());
            inputs.Add(Keys.A, new MoveLeftCommand());
            inputs.Add(Keys.D, new MoveRightCommand());

            inputs.Add(Keys.Space, new ShootCommand());
        }

        public List<Command> HandlePlayerInput()
        {
            PlayerHasMoved = false;
            List<Command> actions = new List<Command>();

            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                actions.Add(inputs[Keys.W]);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                actions.Add(inputs[Keys.S]);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                actions.Add(inputs[Keys.A]);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                actions.Add(inputs[Keys.D]);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                actions.Add(inputs[Keys.Space]);
            }

            if (actions.Count() > 0)
            {
                PlayerHasMoved = true;
            }

            return actions;
        }

        public List<Command> GenerateEnemyInputs()
        {
            List<Command> actions = new List<Command>();

            double t = Game1.rand.NextDouble();

            if (t < 0.1)
            {
                actions.Add(new MoveRandomCommand());
            }

            if (t >= 0.1)
            {
                actions.Add(new MoveTowardTarget());
            }

            return actions;
        }
    }
}
