﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>();
            Random rand = new Random();

            for (int i = 0; i < 100; ++i)
            {
                numbers.Add(rand.Next(0, 10000));
            }

            //query looks in numbers where abitrary variable mod 2 is zero
            var query = numbers.Where(num => (num + 1) % 2 == 0).OrderByDescending(num => num);

            int even = query.Count();

            foreach (int n in query)
            {
                Console.WriteLine(n.ToString());
            }

            Console.ReadLine();
        }
    }
}
