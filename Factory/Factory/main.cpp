#include <stdio.h>
#include <map>
#include <vector>

class Weapon
{
public:
	virtual void Shoot() = 0;
};

typedef Weapon* (*CreateFnPtr)();

class MachineGun : public Weapon
{
public:
    void Shoot() { //shoots the machine gun
            printf("BANGARANG!\n");
    }

	static Weapon* Create()
	{
		return new MachineGun();
	}
};
 
class RocketLauncher : public Weapon
{
public:
    void Shoot() { //fires the rocket launcher
		printf("KABOOOM!!\n");
    }

	static Weapon* Create()
	{
		return new RocketLauncher();
	}
};
 
class LazerGun : public Weapon
{
public:
    void Shoot() { //fires the lazer gun    
		printf("PEW PEW\n");
    }

	static Weapon* Create()
	{
		return new LazerGun();
	}
};
 
class SniperRifle : public Weapon
{
public:
	void Shoot() {
		printf("LAWD BANG\n");
	}

	static Weapon* Create()
	{
		return new SniperRifle();
	}
};

class RailGun : public Weapon
{
public:
	void Shoot() {
		printf("RAILING AWAWY\n");
	}

	static Weapon* Create()
	{
		return new RailGun();
	}
};

class WeaponFactory
{
public:
	static WeaponFactory& GetInstance()
	{
		static WeaponFactory instance;
		return instance;
	}

	Weapon* CreateWeapon(const char* a_WeaponName, CreateFnPtr a_FnPtr)
	{
		std::map<const char*, CreateFnPtr>::iterator it = FactoryMap.find(a_WeaponName);
		if (it != FactoryMap.end())
		{
			return it->second();
		}
		//weapon doesnt exist yet. add it to map and return it.
		FactoryMap[a_WeaponName] = a_FnPtr;
		it = FactoryMap.find(a_WeaponName);
		return it->second();
	}
private:
	WeaponFactory() {}
	~WeaponFactory() {}

	std::map<const char*, CreateFnPtr> FactoryMap;
};

class Player
{
public:
        //just sets weapon pointers to null
	Player () {}
       
    ~Player() { }
       
	void AttachWeapon(Weapon* a_Weapon)
	{
		m_pWeapons.push_back(a_Weapon);
	}

	void ShootAll()
	{
		for (Weapon* i : m_pWeapons)
			i->Shoot();
	} 
private:
	std::vector<Weapon*> m_pWeapons;
};
 
int main()
{
	Player oPlayer;

	oPlayer.AttachWeapon(WeaponFactory::GetInstance().CreateWeapon("RailGun", RailGun::Create));
	oPlayer.AttachWeapon(WeaponFactory::GetInstance().CreateWeapon("RailGun", RailGun::Create));
	oPlayer.AttachWeapon(WeaponFactory::GetInstance().CreateWeapon("SniperRifle", SniperRifle::Create));
	oPlayer.AttachWeapon(WeaponFactory::GetInstance().CreateWeapon("RocketLauncher", RocketLauncher::Create));

	oPlayer.ShootAll();

	getchar();
 
	return 0;
}
