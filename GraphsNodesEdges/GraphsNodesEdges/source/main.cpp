#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "Graph.h"
#include <time.h>

void main()
{
	srand(1);

	std::vector<GraphNode*> dfs;
	std::vector<GraphNode*> dfsRand;

	std::vector<GraphNode*> bfs;
	std::vector<GraphNode*> bfsRand;

	std::vector<GraphNode*> dijkstras;
	std::vector<GraphNode*> dijkstrasRand;

	std::vector<GraphNode*> astar;
	std::vector<GraphNode*> astarRand;

	Graph* testG = new Graph();
	GraphNode* testNode1 = new GraphNode(1, Vector2(rand() % 500));
	GraphNode* testNode2 = new GraphNode(2, Vector2(rand() % 500));
	GraphNode* testNode3 = new GraphNode(3, Vector2(rand() % 500));
	GraphNode* testNode4 = new GraphNode(4, Vector2(rand() % 500));
	GraphNode* testNode5 = new GraphNode(5, Vector2(rand() % 500));

	testNode1->AddEdge(testNode2, 10);
	testNode1->AddEdge(testNode3, 20);

	testNode2->AddEdge(testNode4, 30);

	testNode3->AddEdge(testNode1, 50);
	testNode3->AddEdge(testNode5, 50);

	testNode4->AddEdge(testNode2, 80);
	testNode4->AddEdge(testNode1, 40);
	testNode4->AddEdge(testNode3, 20);

	testNode5->AddEdge(testNode3, 90);

	testG->AddNode(testNode1);
	testG->AddNode(testNode2);
	testG->AddNode(testNode3);
	testG->AddNode(testNode4);
	testG->AddNode(testNode5);

	std::cout << "DEPTH" << std::endl;
	testG->DepthFirstSearch(testNode2, testNode5, dfs);

	std::cout << "BREADTH" << std::endl;
	testG->BreadthFirstSearch(testNode2, testNode5, bfs);

	std::cout << "DIJKSTRAS" << std::endl;
	testG->Dijkstra(testNode2, testNode5, dijkstras);

	std::cout << "ASTAR" << std::endl;
	testG->AStar(testNode2, testNode5, astar);

	Graph* randGraph = new Graph(20);

	std::cout << "DEPTH RAND" << std::endl;
	randGraph->DepthFirstSearch(randGraph->At(1), randGraph->At(15), dfsRand);

	for (unsigned int i = 0; i < dfsRand.size(); ++i)
	{
		std::cout << dfsRand[i] << std::endl;
	}

	std::cout << "BREADTH RAND" << std::endl;
	randGraph->BreadthFirstSearch(randGraph->At(1), randGraph->At(15), bfsRand);

	for (unsigned int i = 0; i < bfsRand.size(); ++i)
	{
		std::cout << bfsRand[i] << std::endl;
	}

	std::cout << "DIJKSTRAS RAND" << std::endl;
	randGraph->Dijkstra(randGraph->At(1), randGraph->At(15), dijkstrasRand);

	for (unsigned int i = 0; i < dijkstrasRand.size(); ++i)
	{
		std::cout << dijkstrasRand[i] << std::endl;
	}

	std::cout << "ASTAR RAND" << std::endl;
	randGraph->AStar(randGraph->At(1), randGraph->At(15), astarRand);

	for (unsigned int i = 0; i < astarRand.size(); ++i)
	{
		std::cout << astarRand[i] << std::endl;
	}

	delete randGraph;
	delete testG;

	system("pause");

	_CrtDumpMemoryLeaks();
}