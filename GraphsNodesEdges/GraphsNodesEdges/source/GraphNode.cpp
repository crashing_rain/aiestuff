#include "GraphNode.h"

GraphNode::GraphNode(int a_iNumber, Vector2 a_Position)
	: m_PreviousNode(nullptr), m_Goal(nullptr), m_iNodeNumber(a_iNumber), visited(false), m_Position(a_Position)
{
	
}

bool GraphNode::HasEdge(GraphNode* a_OtherNode)
{
	for (unsigned int i = 0; i < m_aEdges.size(); ++i)
	{
		if (m_aEdges[i].m_pEnd == a_OtherNode)
		{
			return true;
		}
	}
	return false;
}

//Connecting this node to the one passed in
void GraphNode::AddEdge(GraphNode* a_OtherNode, float a_fCost)
{
	if (HasEdge(a_OtherNode))
	{
		std::cout << "Current node already has edge to node " << a_OtherNode->m_iNodeNumber << std::endl;
		return;
	}

	Edge temp;

	temp.m_pStart = this;
	temp.m_pEnd = a_OtherNode;
	temp.m_fCost = a_fCost;

	m_aEdges.push_back(temp);
}

void GraphNode::AddEdge(GraphNode* a_OtherNode)
{
	if (HasEdge(a_OtherNode))
	{
		std::cout << "Current node already has edge to node " << a_OtherNode->m_iNodeNumber << std::endl;
		return;
	}

	Edge temp;

	temp.m_pStart = this;
	temp.m_pEnd = a_OtherNode;
	temp.m_fCost = temp.m_pEnd->m_Position.Distance(m_Position);

	m_aEdges.push_back(temp);
}

//Removing the connection from this node to the node passed in
void GraphNode::RemoveEdge(GraphNode* a_OtherNode)
{
	if (!HasEdge(a_OtherNode))
	{
		std::cout << "Current node " << m_iNodeNumber << " doesn't have an edge to node " << a_OtherNode->m_iNodeNumber << std::endl;
		return;
	}

	for (unsigned int i = 0; i < m_aEdges.size(); ++i)
	{
		//nodes don't know about what connects to it
		if (m_aEdges[i].m_pEnd == a_OtherNode)
		{
			m_aEdges.erase(m_aEdges.begin() + i);
			return;
		}
	}
}

unsigned int GraphNode::NumOfNeighbours()
{
	return m_aEdges.size();
}
