#ifndef _GRAPH_NODE
#define _GRAPH_NODE

#include <vector>
#include <iostream>
#include <Vector2.h>

class GraphNode;

struct Edge
{
	GraphNode* m_pStart;
	GraphNode* m_pEnd;

	float m_fCost;

	friend std::ostream& operator<<(std::ostream& output, const Edge* a_Other)
	{
		output << " Edge cost: " << a_Other->m_fCost;
		return output;
	}
};

class GraphNode
{
typedef std::vector<Edge> EdgeList;

public:
	GraphNode(int a_iNum, Vector2 a_Position);

	GraphNode* m_PreviousNode;
	GraphNode* m_Goal;

	Vector2 m_Position;

	int m_iNodeNumber;

	//cost to get to this node
	int gCost;

	//heuristic Cost - a guess for how far it is from the goal
	float hCost;

	//final Cost - sum of g cost and h cost
	float fCost;

	EdgeList m_aEdges;
	bool visited;

	float GetDistanceHeuristic()
	{
		if (m_Goal != nullptr)
		{
			return m_Position.Distance(m_Goal->m_Position);
		}
		return FLT_MAX;
	}

	float GetManhattanHeuristic()
	{
		if (m_Goal != nullptr)
		{
			float xDiff = m_Goal->m_Position.x - m_Position.x;
			float yDiff = m_Goal->m_Position.y - m_Position.y;
			return (xDiff + yDiff);
		}
		return FLT_MAX;
	}

	//Checks to see if there is a link between current node
	//and the one that's been passed in
	bool HasEdge(GraphNode* a_OtherNode);

	//Connecting this node to the one passed in
	void AddEdge(GraphNode* a_OtherNode, float a_fCost);

	//add edge with cost based on distance to other node
	void AddEdge(GraphNode* a_OtherNode);

	//Removing the connection from the node passed in
	void RemoveEdge(GraphNode* a_OtherNode);

	//returns num of connected nodes
	unsigned int NumOfNeighbours();

	friend std::ostream& operator<<(std::ostream& output, const GraphNode* a_Other)
	{
		output << " Node number: " << a_Other->m_iNodeNumber;

		return output;
	}
};

#endif