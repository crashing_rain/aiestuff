#ifndef _GRAPH_H
#define _GRAPH_H

#include "GraphNode.h"
#include <stack>
#include <queue>
#include <list>
#include <assert.h>

class Graph
{
	typedef std::vector<GraphNode*> NodeList;
public:
	Graph();
	Graph(unsigned int a_uiNodeCount);

	~Graph();

	GraphNode* At(unsigned int i)
	{
		assert(i < m_aNodes.size());
		assert(i >= 0);
		return m_aNodes[i];
	}

	unsigned int GetNumOfNodes()
	{
		return m_aNodes.size();
	}

	void AddNode(GraphNode* a_pNode);
	void RemoveNode(GraphNode* a_pNode);

	void AddEdge(GraphNode* a_pNode, GraphNode* a_pOtherNode, float a_fCost);
	void AddEdge(GraphNode* a_pNode, GraphNode* a_pOtherNode);
	void RemoveEdge(GraphNode* a_pStartNode, GraphNode* a_EndNode);

	void PrintNeighbours(GraphNode* a_pNode);
	bool IsNeighbour(GraphNode* a_pNode, GraphNode* a_pOtherNode);
	bool IsInNodeList(GraphNode* a_pNode);

	void ResetVisited();
	void ResetPrevNodes();
	void ResetCosts();

	bool DepthFirstSearch(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path);
	bool BreadthFirstSearch(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path);
	bool Dijkstra(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path);
	bool AStar(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path, bool useManhattanHeuristic = true);

	friend std::ostream& operator<<(std::ostream& output, const Graph* a_Other)
	{
		for (unsigned int i = 0; i < a_Other->m_aNodes.size(); ++i)
		{
			output << "Nodes connected: " << a_Other->m_aNodes[i];
			for (unsigned int j = 0; j < a_Other->m_aNodes[i]->m_aEdges.size(); ++j)
			{
				output << " End: "		<< a_Other->m_aNodes[i]->m_aEdges[j].m_pEnd		<<
					      " Cost: "		<< a_Other->m_aNodes[i]->m_aEdges[j].m_fCost;
			}
			output << std::endl;
		}
		return output;
	}

private:
	NodeList m_aNodes;
};

#endif