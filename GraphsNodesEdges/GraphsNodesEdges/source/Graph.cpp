#include "Graph.h"

Graph::Graph()
{
}

Graph::Graph(unsigned int a_uiNodeCount)
{
	//Creating nodes, adding to graph's node list
	for (unsigned int i = 0; i < a_uiNodeCount; ++i)
	{
		GraphNode* temp = new GraphNode(i, Vector2(rand() % 500));
		m_aNodes.push_back(temp);
	}

	//Looping through nodes and randomly setting up edges
	//with random values. If the random value is the same
	//as the index, then there won't be an edge out of
	//that node.
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		int randNumEdges = rand() % m_aNodes.size();
		for (int j = 0; j < randNumEdges; ++j)
		{
			//choose random number
			int randNum = rand() % m_aNodes.size();

			//if the random number is the same as
			//the current edge, then continue
			//not allowing for an edge to the
			//current node - not going any where
			if (randNum == i)
			{
				continue;
			}
			m_aNodes[i]->AddEdge(m_aNodes[randNum]);
		}
	}
}

Graph::~Graph()
{
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		delete m_aNodes[i];
	}
	m_aNodes.clear();
}

void Graph::PrintNeighbours(GraphNode* a_pNode)
{
	//if the size of the edges are zero, then
	//node itself has no neighbours
	if (a_pNode->m_aEdges.size() == 0)
	{
		std::cout << "Node has no neighbours" << std::endl;
		return;
	}

	std::cout << "Neighbours for node ID " << a_pNode->m_iNodeNumber << " are: ";

	//Loop through all edges of the node
	for (unsigned int i = 0; i < a_pNode->m_aEdges.size(); ++i)
	{
		//As long as the end node attached is valid
		if (a_pNode->m_aEdges[i].m_pEnd != nullptr)
		{
			//Print it out
			std::cout << a_pNode->m_aEdges[i].m_pEnd;
		}
	}
	std::cout << std::endl;
}

void Graph::AddNode(GraphNode* a_pNode)
{
	//can only add nodes with unique node numbers
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pNode)
		{
			std::cout << "Node already exists" << std::endl;
			return;
		}
	}
	m_aNodes.push_back(a_pNode);
}

void Graph::RemoveNode(GraphNode* a_pNode)
{
	//all nodes in vector
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		//all edges in each node
		for (unsigned int j = 0; j < m_aNodes[i]->m_aEdges.size(); ++j)
		{
			//need to find all instances where
			//a node has a_pNode as an end of an edge
			if (m_aNodes[i]->m_aEdges[j].m_pEnd == a_pNode)
			{
				//removing edge
				m_aNodes[i]->m_aEdges.erase(m_aNodes[i]->m_aEdges.begin() + j);
			}
		}
	}
	
	//node is now safe to remove, with all edges to it removed,
	//so loop through again, and remove specific node
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pNode)
		{
			m_aNodes.erase(m_aNodes.begin() + i);
			return;
		}
	}
}

bool Graph::IsNeighbour(GraphNode* a_pNode, GraphNode* a_pOtherNode)
{	
	//check to see if end node in edges has something
	//that points to a_pOtherNode
	for (unsigned int i = 0; i < a_pNode->m_aEdges.size(); ++i)
	{
		if (a_pNode->m_aEdges[i].m_pEnd == a_pOtherNode)
		{
			return true;
		}
	}
	return false;
}

bool Graph::IsInNodeList(GraphNode* a_pNode)
{
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pNode)
		{
			return true;
		}
	}
	return false;
}

//Start node must be in graph already
void Graph::AddEdge(GraphNode* a_pStartNode, GraphNode* a_pEndNode, float a_fCost)
{
	if (!IsInNodeList(a_pStartNode))
	{
		std::cout << "Node " << a_pStartNode->m_iNodeNumber << " is not in graph" << std::endl;
		return;
	}

	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pStartNode)
		{
			m_aNodes[i]->AddEdge(a_pEndNode, a_fCost);
		}
	}
}

void Graph::AddEdge(GraphNode* a_pStartNode, GraphNode* a_pEndNode)
{
	if (!IsInNodeList(a_pStartNode))
	{
		std::cout << "Node " << a_pStartNode->m_iNodeNumber << " is not in graph" << std::endl;
		return;
	}

	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pStartNode)
		{
			m_aNodes[i]->AddEdge(a_pEndNode);
			return;
		}
	}
}

//start node must be in graph already
void Graph::RemoveEdge(GraphNode* a_pStartNode, GraphNode* a_EndNode)
{
	if (!IsInNodeList(a_pStartNode))
	{
		std::cout << "Node " << a_pStartNode->m_iNodeNumber << " is not in graph" << std::endl;
		return;
	}

	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		if (m_aNodes[i] == a_pStartNode)
		{
			m_aNodes[i]->RemoveEdge(a_EndNode);
		}
	}
}

bool DijkstraNodeCompare(const GraphNode* a_Node, const GraphNode* a_OtherNode)
{
	return (a_Node->gCost < a_OtherNode->gCost);
}

bool AStarNodeCompare(GraphNode* a_Node, GraphNode* a_OtherNode)
{
	assert(a_Node->m_Goal != nullptr && a_OtherNode->m_Goal != nullptr);
	float leftF = a_Node->gCost + a_Node->GetDistanceHeuristic();
	float rightF = a_OtherNode->gCost + a_OtherNode->GetDistanceHeuristic();

	return (leftF < rightF);
}

void Graph::ResetVisited()
{
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		m_aNodes[i]->visited = false;
	}
}

void Graph::ResetCosts()
{
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		m_aNodes[i]->gCost = INT_MAX;
		m_aNodes[i]->fCost = FLT_MAX;
	}
}

void Graph::ResetPrevNodes()
{
	for (unsigned int i = 0; i < m_aNodes.size(); ++i)
	{
		m_aNodes[i]->m_PreviousNode = nullptr;
	}
}

bool Graph::DepthFirstSearch(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path)
{
	ResetVisited();
	std::stack<GraphNode*> nodeStack;

	nodeStack.push(a_Start);
	while (!nodeStack.empty())
	{
		GraphNode* currentNode = nodeStack.top();
		nodeStack.pop();

		//Make sure current node hasn't been visited
		if (!currentNode->visited)
		{
			//do things to node here
			a_Path.push_back(currentNode);

			//mark current node as being visited
			currentNode->visited = true;

			//the current node is the same as the end node, so
			//a path from start to end has been found
			if (currentNode == a_End)
			{
				return true;
			}

			//add all the current node's edges to the node stack
			for (unsigned int i = 0; i < currentNode->m_aEdges.size(); ++i)
			{
				//check if the edge itself has been visited yet
				if (!currentNode->m_aEdges[i].m_pEnd->visited)
				{
					nodeStack.push(currentNode->m_aEdges[i].m_pEnd);
				}
			}
		}
	}
	return false;
}

bool Graph::BreadthFirstSearch(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path)
{
	ResetVisited();
	std::queue<GraphNode*> nodeQueue;

	//start here
	nodeQueue.push(a_Start);
	while (!nodeQueue.empty())
	{
		GraphNode* currentNode = nodeQueue.back();
		nodeQueue.pop();

		if (!currentNode->visited)
		{
			//do things to node here
			a_Path.push_back(currentNode);

			currentNode->visited = true;
			if (currentNode == a_End)
			{
				return true;
			}

			for (unsigned int i = 0; i < currentNode->m_aEdges.size(); ++i)
			{
				if (!currentNode->m_aEdges[i].m_pEnd->visited)
				{
					nodeQueue.push(currentNode->m_aEdges[i].m_pEnd);
				}
			}
		}
	}
	return false;
}

bool Graph::Dijkstra(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path)
{
	ResetCosts();
	ResetVisited();
	ResetPrevNodes();

	std::list<GraphNode*> nodeList;

	a_Start->m_PreviousNode = a_Start;
	a_Start->gCost = 0;

	nodeList.push_back(a_Start);

	while (!nodeList.empty())
	{
		GraphNode* currentNode = nodeList.back();
		nodeList.pop_back();

		//make sure current node hasn't been visited
		if (!currentNode->visited)
		{
			a_Path.push_back(currentNode);

			//we've found the end, so return true
			if (currentNode == a_End)
			{
				return true;
			}

			//mark as visited
			currentNode->visited = true;

			//looping through all edges of current node
			for (unsigned int i = 0; i < currentNode->m_aEdges.size(); ++i)
			{
				//sort nodes
				nodeList.sort(DijkstraNodeCompare);

				//Make sure that the edge hasn't been visited
				if (!currentNode->m_aEdges[i].m_pEnd->visited)
				{
					//calculate the cost to travel to the next node
					float currentGCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost;

					//check if the cost to get to the next node is less than
					//the gcost of the node on the end of the edge
					if (currentGCost <= currentNode->m_aEdges[i].m_pEnd->gCost)
					{
						//passed tests
						//change the end node's previous node to be the current one
						currentNode->m_aEdges[i].m_pEnd->m_PreviousNode = currentNode;

						//change the gcost of the node on the end of the current edge
						//to be the current node's gcost + edge cost to get to that node
						currentNode->m_aEdges[i].m_pEnd->gCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost;

						nodeList.push_back(currentNode->m_aEdges[i].m_pEnd);
					}
				}
			}
		}
	}
	return false;
}

bool Graph::AStar(GraphNode* a_Start, GraphNode* a_End, std::vector<GraphNode*>& a_Path, bool useManhattanHeuristic)
{
	ResetCosts();
	ResetVisited();
	ResetPrevNodes();

	a_Start->m_PreviousNode = a_Start;
	a_Start->gCost = 0;
	a_Start->fCost = 0;

	std::list<GraphNode*> nodeList;
	nodeList.push_back(a_Start);

	while (!nodeList.empty())
	{
		GraphNode* currentNode = nodeList.back();
		nodeList.pop_back();

		if (!currentNode->visited)
		{
			a_Path.push_back(currentNode);

			if (currentNode == a_End)
			{
				return true;
			}

			currentNode->visited = true;
			currentNode->m_Goal = a_End;

			for (unsigned int i = 0; i < currentNode->m_aEdges.size(); ++i)
			{
				//sort nodes
				nodeList.sort(AStarNodeCompare);

				//Make sure that the end node of the current edge hasn't been visited
				if (!currentNode->m_aEdges[i].m_pEnd->visited)
				{
					currentNode->m_aEdges[i].m_pEnd->m_Goal = a_End;

					//used to calculate the cost to travel to the next node
					float currentHeuristic = 0;
					float currentFCost = 0;

					if (useManhattanHeuristic)
					{
						currentHeuristic = currentNode->m_aEdges[i].m_pEnd->GetManhattanHeuristic();
						currentFCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost + currentHeuristic;
					}
					else
					{
						currentHeuristic = currentNode->m_aEdges[i].m_pEnd->GetDistanceHeuristic();
						currentFCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost + currentHeuristic;
					}

					//check if the cost to get to the next node is less than
					//the gcost of the node on the end of the edge
					if (currentFCost < currentNode->m_aEdges[i].m_pEnd->fCost)
					{
						//passed tests
						//change the end node's previous node to be the current one
						currentNode->m_aEdges[i].m_pEnd->m_PreviousNode = currentNode;

						if (useManhattanHeuristic)
						{
							//change the gcost of the node on the end of the current edge
							//to be the current node's gcost + edge cost to get to that node
							currentNode->m_aEdges[i].m_pEnd->fCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost + currentNode->m_aEdges[i].m_pEnd->GetManhattanHeuristic();
						}
						else
						{
							//change the gcost of the node on the end of the current edge
							//to be the current node's gcost + edge cost to get to that node
							currentNode->m_aEdges[i].m_pEnd->fCost = currentNode->gCost + currentNode->m_aEdges[i].m_fCost + currentNode->m_aEdges[i].m_pEnd->GetDistanceHeuristic();
						}

						nodeList.push_back(currentNode->m_aEdges[i].m_pEnd);
					}
				}
			}
		}
	}

	return false;
}
