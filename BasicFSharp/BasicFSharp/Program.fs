﻿//An expression is a combination of values, variables and operators
//Doesn't have to contain all of the elements
//Value by itself is considered an expression
//Always performing operations on the data itself?

//A statement is an instruction to be executed
//They do something
//They are the actions that a program takes, declaring vars, assigning values

//Syntax for variable assignment is 3 spaces after new line

//Needs to be defined outside of main
//Used to group pieces of data together
//Giving each piece a name
//Instances of Book type now
//have three strings and an int
type Book = { Name: string; AuthorName: string; Rating: int; ISBN: string }

//since types are inferred, anything with the same set of fields
//will be inferred as the last type defined, in this case it is Bluray
//this can be fixed by explicitly denoting type when creating an instance
// of that type
type Bluray = { Name: string; AuthorName: string; Rating: string; ISBN: string }

//using the keyword option, we can denote a piece of data within the type
//that is optional. With an option type, we can denote that the information
//exists with Some, and that it doesn't exist with None. This is used when
//creating an instance of the type
type CD = { AlbumName: string; ArtistName: string; ISBN: string; Rating: string option }

//Discriminated union - for computations that yield non-uniform data structures
//Represents data that can take different forms
//Similar to enum, but each case can contain differing data types
//These unions are often used with pattern matching
type MushroomColour =
| Red
| Blue
| Green
| Yellow

type Powerup =
| FireFlower
| Mushroom of MushroomColour
| Star
| FlyingSuit

//Defines a shape type, within it contains different kinds of shapes
//each with their own way of dealing with information
type Shape =
//single int defines a circle
| Circle of int
//two ints define a rect
| Rectangle of int * int
//two ints forming a list
| Polygon of (int * int) list
| Point of (int * int)

//overloading operators within a type
type Point (x:float, y:float) =
    member this.X = x
    member this.Y = y

    //operators can be prefix or infix
    //prefix - placed in front of an operand
    //infix - placed between two operands
    //place operator in parentheses
    //place parameter list in parentheses
    static member (+) (p1:Point, p2:Point) =
       new Point(p1.X + p2.X, p1.Y + p2.Y)

[<EntryPoint>]
let main argv = 
    let p1, p2 = new Point(0., 1.), new Point(1., 1.)
    let p3 = p1 + p2

    printfn "p1 + p2 = %f %f" p3.X p3.Y

    //evaluates to val add : x:int -> y:int -> int
    //"add is a function that, given an integer, returns
    //a function, which given an integer, returns an integer"
    //BTS below function is converted to:
    //let add = (fun x -> (fun y -> x + y))
    let add (x) (y) = 
       x + y

    //functions can be split up, and assigned to variables
    //point inc to be a function, notice missing second arg
    //calling add with no arguments returns: 
    //(fun x -> (fun y -> x + y))
    //when applying one argument, it returns:
    //fun y -> 1 + y
    //it fills in the first argument, while leaving the second
    //unfilled, abstract, anonymous
    //Currying is built on the principle that each argument actually
    //returns a separate function
    let inc = add 1
    //anotherInc is a function, takes in an arg
    //calls add function, passing in 1 and its own arg
    //the ordering of arguments is important, unfilled arguments 
    //can only be in the last place of the parameter list
    let anotherInc x = add 1 x
    printfn "%d is the same as %d" (inc 2) (anotherInc 1)

    let sub x y = x - y

    //evaluated to be:
    //val swapArgs : f:('a -> 'b -> 'c) -> x:'b -> y:'a -> 'c
    //swapArgs is a function that takes in a function and the two
    //args for that particular function. It then swaps the two args
    //around, such that the first is now second, and the second is
    //now first
    let swapArgs funct arg1 arg2 = funct arg2 arg1

    //the dec is assigned the swapArgs function, which takes in 
    //the subtraction function and an arg of 1
    //the dec function is then called, and passed an arg of 10
    //subtracting 1 from 10, resulting in the printing out of 9
    let dec = swapArgs sub 1
    printfn "Before 10 there is %d" (dec 10)

    //Operator overloading can be performed on existing operators
    //or defined for new ones. Since all functions in F# take in
    //one argument (as a result of currying) this overload can apply
    //to all functions
    //the argument comes before the function that should receive it
    //with arg on left, |> transforms is so that the arg is on the right
    //passing that data into the function to the left of it
    let (|>) arg funct = funct arg

    let lucky = 3 + 4 //by default, variables are not mutable, like using const
    let unlucky = lucky + 6
    let mutable modifiable = "original"
    modifiable <- "new" //<- assignment operator for modifying mutable variables

    //Using the Shape type
    let draw shape =
       match shape with
       | Circle radius 
                -> printfn "Circle has rad %d" radius
       | Rectangle (height, width) 
                -> printfn "Rect is %d high and %d wide" height width
       | Polygon points 
                -> printfn "The polygon is made up of %A" points
       | _ 
                -> printfn "I don't recog this shape"

    //using the shape types defined previously,
    //we can create things and pass in the
    //specified parameters
    let circle1 = Circle(10)
    let rect1 = Rectangle(20, 30)

    //inner elements are two ints, specified using the parentheses, 
    //the container is a list, specified using square brackets
    //semicolons separate each element within the list
    let poly1 = Polygon([(1, 1); (2, 2); (3, 3); (4, 4);])

    //arrange elements in a list, piping it into the
    //list iteration function, passing each element
    //into the draw function
    [circle1; rect1; poly1] |> List.iter draw

    //Set variable to one of the types specified in the union
    let powerUp = FlyingSuit

    //the mushroom field is of a particular data type
    //as such, the pattern matching must also account
    //for the cases within the MushroomColour data type
    match powerUp with
     | FireFlower -> printfn "FLOWERS"
     //the mushroom field is now a specific data type
     //need to specify the different options for 
     //this variable too
     | Mushroom color -> match color with
                            | Red -> printfn "RED SHROOM"
                            | Blue -> printfn "BLUE SHROOM"
                            | Green -> printfn "GREEN SHROOM"
                            | Yellow -> printfn "YELLOW SHROOM"
     | Star -> printfn "STARS"
     | FlyingSuit -> printfn "SUIT"
     | _ -> printfn "NOTHING SELECTED"

    //Type is inferred looking at the fields used
    let expertFSharp =
      { 
        Name = "Expert";
        AuthorName = "DONE";
        //explicitly denote which type
        Book.Rating = 5;
        ISBN = "8765656787" 
      }

    //using the option None
    let doppelganger = 
       {
            AlbumName = "Doppelganger";
            ArtistName = "Fall of Troy";
            ISBN = "ISJIJSF";
            Rating = None;
       }

    //using the option Some, since it
    //is a string, need to specify a
    //string for the rating field
    let sempiternal =
       {
            CD.AlbumName = "Sempiternal";
            CD.ArtistName = "Bring the Horizon";
            CD.ISBN = "AIFJAIF";
            CD.Rating = Some "Good";
       }

    //using a previous instance, can create
    //another and modify it slightly
    let anotherBook =
        {
            expertFSharp with AuthorName = "JOHN SMITH"
        }

    //Define function, piping results from
    //one function into another
    //Functions always return the value of
    //the last expression used
    let sumOfSquares num =
        //create a list from
        //1 to specified number
        [1..num] 
        //fun is a keyword used similarly to a lambda
        //list.map creates a new list
        //with the specified elements
        |> List.map (fun x -> x * x) 
        //sum everything up in the list
        |> List.sum
    
    //using .. syntax, we can abbreviate
    //a list and include a range of values instead
    //lists are singly linked lists
    let fHun = [1..100]

    let doubled =
        //map creates a new list
        //using the function specified
        //and elements from the list specified
        List.map (fun x y -> x * y)
            //filter looks at the elements in the 
            //list provided, comparing them
            //and returning a list
            (List.filter (fun x -> x % 2 = 0) fHun)

    let b = sumOfSquares 6

    let first = 1
    let numbers = [5;6;7;8;9;10]
    let first = first::numbers

    //short version
    let rec shortQuickSort = function
        | [] -> [] //empty array, return empty array
        | first::rest -> //non empty array
            //splits list based on predicate
            //returns two lists, true first, false second
            let smaller, larger = List.partition ((>=) first) rest
            //concatentate, while calling shortQuickSort function agian
            List.concat [ shortQuickSort smaller; [first]; shortQuickSort larger]

    //verbose version
    //keyword rec denotes a recursive function
    let rec quicksort list =
        //switch statement
        match list with
        | [] -> //check for empty array - where the recursion stops
             []
        //tries to match a non-empty list, creating values - firstElem and otherElem - automatically
        //the double colon creates a list from 
        //matches if the list is not empty
        //then creates two new variables, to use later on
        | firstElem::otherElems ->
            let smallerElems = 
                otherElems //take the rest of the elements
                |> List.filter (fun e -> e < firstElem) //filter it checking elems against first, less than
                |> quicksort //call function again
            let largerElems = 
                otherElems //take the rest of the elements
                |> List.filter (fun e -> e >= firstElem) //filter it checking elems against first, greater or equal to
                |> quicksort //call function again
            //put the lists together, squeezing the first element in between the two lists
            //everything less than first elem, the first elem, everything greater than the first elem
            List.concat [smallerElems; [firstElem]; largerElems]

    //"Date,Open,High,Low,Close,Volume,Adj Close"
    let stockData =
        [ 
          "2012-03-30,32.40,32.41,32.04,32.26,31749400,32.26";
          "2012-03-29,32.06,32.19,31.81,32.12,37038500,32.12";
          "2012-03-28,32.52,32.70,32.04,32.19,41344800,32.19";
          "2012-03-27,32.65,32.70,32.40,32.52,36274900,32.52";
          "2012-03-26,32.19,32.61,32.15,32.59,36758300,32.59";
          "2012-03-23,32.10,32.11,31.72,32.01,35912200,32.01";
          "2012-03-22,31.81,32.09,31.79,32.00,31749500,32.00";
          "2012-03-21,31.96,32.15,31.82,31.91,37928600,31.91";
          "2012-03-20,32.10,32.15,31.74,31.99,41566800,31.99";
          "2012-03-19,32.54,32.61,32.15,32.20,44789200,32.20";
          "2012-03-16,32.91,32.95,32.50,32.60,65626400,32.60";
          "2012-03-15,32.79,32.94,32.58,32.85,49068300,32.85";
          "2012-03-14,32.53,32.88,32.49,32.77,41986900,32.77";
          "2012-03-13,32.24,32.69,32.15,32.67,48951700,32.67";
          "2012-03-12,31.97,32.20,31.82,32.04,34073600,32.04";
          "2012-03-09,32.10,32.16,31.92,31.99,34628400,31.99";
          "2012-03-08,32.04,32.21,31.90,32.01,36747400,32.01";
          "2012-03-07,31.67,31.92,31.53,31.84,34340400,31.84";
          "2012-03-06,31.54,31.98,31.49,31.56,51932900,31.56";
          "2012-03-05,32.01,32.05,31.62,31.80,45240000,31.80";
          "2012-03-02,32.31,32.44,32.00,32.08,47314200,32.08";
          "2012-03-01,31.93,32.39,31.85,32.29,77344100,32.29";
          "2012-02-29,31.89,32.00,31.61,31.74,59323600,31.74"; ]
    
    //split strings based on commas
    let splitCommas (x:string) =
        x.Split([|','|])

    //look for date with the lowest open and close prices
    //print out the date with the lowest open and close prices
    let lowest = 
        stockData
        |> List.map splitCommas
        |> List.maxBy (fun x -> abs(float x.[1] - float x.[4]))
        |> (fun x -> x.[0])

    System.Console.WriteLine(shortQuickSort [6;7;3;4;6;8;5;2;45;7;8;9;5;4;3;32;5;7;9;5;3;2;5;87;9;7;4;2;])
    System.Console.ReadKey()
    0 // return an integer exit code
