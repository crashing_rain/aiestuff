using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CharacterDrawing
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;
        SpriteFont sf;

        public static int screenWidth = 1280;
        public static int screenHeight = 720;

        DrawnCharacterSpawner characterSpawner1;
        DrawnCharacterSpawner characterSpawner2;
        DrawnCharacterSpawner characterSpawner3;
        Texture2D blankSquare;

        Vector2 pos1 = new Vector2(250, 250);
        Vector2 pos2 = new Vector2(150, 550);
        Vector2 pos3 = new Vector2(450, 350);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            device = graphics.GraphicsDevice;
            blankSquare = Content.Load<Texture2D>("blankSquare");
            characterSpawner1 = new DrawnCharacterSpawner(blankSquare, new Vector2(screenWidth * 0.5f, screenHeight * 0.5f), new Vector2(screenWidth, screenHeight));
            characterSpawner2 = new DrawnCharacterSpawner(blankSquare, new Vector2(screenWidth * 0.75f, screenHeight * 0.5f), new Vector2(screenWidth * 0.5f, screenHeight));
            characterSpawner3 = new DrawnCharacterSpawner(blankSquare, new Vector2(screenWidth * 0.25f, screenHeight * 0.5f), new Vector2(screenWidth * 0.25f, screenHeight));
            // TODO: use this.Content to load your game content here
            sf = Content.Load<SpriteFont>("spritefont1");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            characterSpawner1.Update(gameTime);
            characterSpawner2.Update(gameTime);
            characterSpawner3.Update(gameTime);

            Vector2 mouse = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if (Vector2.Distance(mouse, pos1) < 50)
                {
                    pos1 = mouse;
                }
                else if (Vector2.Distance(mouse, pos2) < 50)
                {
                    pos2 = mouse;
                }
            }

            pos3.X = (Vector2.Dot(pos1, pos2) / (pos2.X * pos2.X + pos2.Y * pos2.Y)) * pos2.X;
            pos3.Y = (Vector2.Dot(pos1, pos2) / (pos2.X * pos2.X + pos2.Y * pos2.Y)) * pos2.Y;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);

            characterSpawner1.Draw(spriteBatch, sf);
            characterSpawner2.Draw(spriteBatch, sf);
            characterSpawner3.Draw(spriteBatch, sf);

            spriteBatch.Draw(blankSquare, pos1, Color.Red);
            spriteBatch.Draw(blankSquare, pos2, Color.Blue);
            spriteBatch.Draw(blankSquare, pos3, Color.Yellow);
            DrawLine(pos1, pos2, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void DrawLine(Vector2 begin, Vector2 end, Color color, int width = 1)
        {
            Rectangle r = new Rectangle((int)begin.X, (int)begin.Y, (int)(end - begin).Length()+width, width);
            Vector2 v = Vector2.Normalize(begin - end);
            float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
            if (begin.Y > end.Y) angle = MathHelper.TwoPi - angle;
            spriteBatch.Draw(blankSquare, r, null, color, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}
