﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CharacterDrawing
{
    //characters will spawn around the position of the spawner
    public class DrawnCharacterSpawner
    {
        List<DrawnCharacter> characterList = new List<DrawnCharacter>();
        Random rand = new Random();
        Texture2D backgroundTexture;
        Vector2 position;
        Vector2 range;
        float spawnTimer = 0;
        float spawnReady = 0.01f;
        char [] explosion = { 'E', 'X', 'P', 'L', 'O', 'S', 'I', 'O', 'N', 'e', 'x', 'p', 'l', 'o', 's', 'i', 'o', 'n' };

        public DrawnCharacterSpawner(Texture2D texture, Vector2 a_Position, Vector2 a_Range)
        {
            position = a_Position;
            range = a_Range;
            backgroundTexture = texture;
        }

        public void Update(GameTime gt)
        {
            spawnTimer += (float)gt.ElapsedGameTime.TotalMilliseconds;
            if (spawnTimer > spawnReady)
            {
                char randChar = (char)(rand.Next(33, 126));
                int randType = rand.Next(0, 2000) % 2;
                AddCharacter(randChar, new Vector2(rand.Next(-5,5), 1), GenerateRandomColour(), randType);
                spawnTimer = 0;
            }

            foreach (DrawnCharacter n in characterList)
            {
                n.Update(gt);
            }

            for (int i = 0; i < characterList.Count; ++i)
            {
                if (!characterList[i].IsAlive)
                {
                    if (characterList[i].CharType == CharacterType.EXPLODING)
                    {
                        for (int j = 0; j < 6; ++j)
                        {
                            int randNum = rand.Next(0, explosion.Length);
                            char randChar = explosion[randNum];
                            AddCharacter(randChar, characterList[i].Position, new Vector2(rand.Next(-5, 5),rand.Next(-5, -1)), new Color(rand.Next(0, 65025) % 255, rand.Next(0, 65025) % 255, 0, 255), 1);
                        }
                    }

                    characterList.Remove(characterList[i]);
                }
            }
        }

        public void Draw(SpriteBatch sb, SpriteFont sf)
        {
            foreach (DrawnCharacter n in characterList)
            {
                n.Draw(sb, sf);
            }
            //sb.Draw(backgroundTexture, position, Color.White);
        }

        public void AddCharacter(char a_Value, Vector2 direction, Color a_Color, int a_CharType)
        {
            DrawnCharacter temp = new DrawnCharacter(a_Value, a_CharType, position + GenerateRandomPosition(range), direction, rand.Next(1, 2000) * 0.0001f, 1, a_Color);            
            characterList.Add(temp);
        }

        public void AddCharacter(char a_Value, Vector2 pos, Vector2 dir, Color a_Colour, int a_CharType)
        {
            DrawnCharacter temp = new DrawnCharacter(a_Value, a_CharType, pos + new Vector2(0, rand.Next(0, 2000) % range.Y), dir, rand.Next(1, 2000) * 0.0001f, 3, a_Colour);
            characterList.Add(temp);
        }

        public Vector2 GenerateRandomPosition(Vector2 range)
        {
            return new Vector2(rand.Next((int)-range.X, (int)range.X), rand.Next((int)-range.Y, (int)range.Y));
        }

        public Color GenerateRandomColour()
        {
            return new Color(rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255), 255);
        }
    }
}
