﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CharacterDrawing
{
    public enum CharacterType
    {
        EXPLODING,
        NORMAL,
    };

    public class DrawnCharacter
    {
        Vector2 position;
        Vector2 startPos;
        Vector2 direction;
        float movementSpeed;
        float currentDistFromStart;
        float fadeMultiplier;
        float maxDistFromStart = 500;
        bool isAlive;
        char value;
        Color colour;
        CharacterType charType;
        Random rand = new Random();

        public DrawnCharacter(char a_Value, int a_CharType, Vector2 a_Pos, Vector2 a_Dir, float a_MovementSpeed, float a_FadeMultiplier, Color a_Colour)
        {
            position = a_Pos;
            startPos = position;
            direction = a_Dir;
            movementSpeed = a_MovementSpeed;
            isAlive = true;
            fadeMultiplier = a_FadeMultiplier;
            colour = a_Colour;
            value = a_Value;
            charType = (CharacterType)(a_CharType);
        }

        public void Update(GameTime gt)
        {
            currentDistFromStart = fadeMultiplier * Vector2.Distance(startPos, position);

            if (isAlive)
            {
                position += direction * movementSpeed * (float)gt.ElapsedGameTime.TotalMilliseconds;
            }
            
            if (position.X > Game1.screenWidth || position.Y > Game1.screenHeight)
            {
                isAlive = false;
            }

            if (position.X < 0 || position.Y < 0)
            {
                isAlive = false;
            }

            if (currentDistFromStart > maxDistFromStart)
            {
                isAlive = false;
            }

            if (direction.Equals(Vector2.Zero) || movementSpeed.Equals(0))
            {
                isAlive = false;
            }
        }

        public void Draw(SpriteBatch sb, SpriteFont sf)
        {
            if (isAlive)
            {
                sb.DrawString(sf, value.ToString(), position, colour * (1 - (currentDistFromStart / maxDistFromStart)));
            }
        }

        public bool IsAlive
        {
            get { return isAlive; }
        }

        public CharacterType CharType
        {
            get { return charType; }
        }

        public Vector2 Position
        {
            get { return position; }
        }
    }
}
