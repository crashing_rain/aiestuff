﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SteeringEntities
{
    public class RogueLikeState : GameState
    {
        List<Obstacle> obstacles = new List<Obstacle>();
        Particle.Emitter particleEmitter;

        Player test;

        public RogueLikeState()
            : base("RogueLikeState")
        {
            m_BackgroundColour = Color.White;
            obstacles.Clear();
        }

        public override void Initialise()
        {
            if (!initialised)
            {
                //particleEmitter = new Particle.Emitter(Game1.RandomPosition(), new Texture2D [] {m_Content.Load<Texture2D>("animatedColours1"), m_Content.Load<Texture2D>("animatedColours2"), m_Content.Load<Texture2D>("animatedColours3"), m_Content.Load<Texture2D>("animatedColours4"), m_Content.Load<Texture2D>("animatedColours5")}, Game1.RandomColour());
                particleEmitter = new Particle.Emitter(Game1.RandomPosition(), m_Content.Load<Texture2D>("smoke"), Game1.RandomColour());

                for (int i = 0; i < 250; ++i)
                {
                    obstacles.Add(new Obstacle(Game1.RandomPosition(), new Vector2((float)Game1.cryptoRand.NextDouble() * 100, (float)Game1.cryptoRand.NextDouble() * 100), blankSquare, Game1.RandomColour()));
                }

                stateCamera.CentreOnPos(new Vector2(Game1.g_iScreenWidth * 0.5f, Game1.g_iScreenHeight * 0.5f));
                test = new Player(m_Content.Load<Texture2D>("kefka_cropped"), new Vector2(40, 50), new Vector2(48, 48));
            }

            base.Initialise();
        }

        public override void Update(GameTime gt)
        {
            foreach (Obstacle n in obstacles)
            {
                n.Update(gt);
            }

            ProjectileManager.Instance.Update(gt);

            stateCamera.CentreOnPos(new Vector2(test.Position.X - test.GetRect.Width, test.Position.Y - test.GetRect.Height));
            //stateCamera.HandleInput();

            test.Update(gt);

            particleEmitter.Position = new Vector2(test.Position.X - test.GetRect.Width * 4, test.Position.Y - test.GetRect.Height * 1.2f);
            //particleEmitter.Position = stateCamera.ScreenToWorld(new Vector2(Mouse.GetState().X, Mouse.GetState().Y));

            //particleEmitter.Update(gt);

            base.Update(gt);
        }

        public override void Draw()
        {
            //particleEmitter.Draw(Game1.sb, Game1.sf);

            foreach (Obstacle n in obstacles)
            {
                n.Draw(Game1.sb, Game1.sf);
            }

            ProjectileManager.Instance.Draw(Game1.sb, Game1.sf);

            test.Draw(Game1.sb, Game1.sf);

            base.Draw();
        }
    }
}
