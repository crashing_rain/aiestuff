#include <iostream>

void main()
{
	int arrayCount = 20;
	int* a = new int[arrayCount];
	
	for (int i = 0; i < arrayCount; ++i)
	{
		a[i] = arrayCount - 1 - i;
	}

	for (int i = 0; i < arrayCount; ++i)
	{
		a[i] = a[a[i]];
	}

	for (int i = 0; i < arrayCount; ++i)
	{
		std::cout << a[i] << std::endl;
	}

	delete a;
	system("pause");
}
