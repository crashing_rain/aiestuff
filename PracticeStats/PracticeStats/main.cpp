#include <iostream>
#include <time.h>
#include <vector>
#include <map>

class BaseValue
{
protected:
	float value;
	float multiplier;
public:
	BaseValue(float a_Value = 1, float a_Multiplier = 0)
	{
		value = a_Value;
		multiplier = a_Multiplier;
	}

	float GetValue() const
	{
		return value;
	}

	float GetMultiplier() const
	{
		return multiplier;
	}
};

class RawValue : public BaseValue
{
private:
public:
	RawValue(float a_Value = 1, float a_Multiplier = 0)
		: BaseValue(a_Value, a_Multiplier)
	{

	}
};

class FinalValue : public BaseValue
{	
public:
	FinalValue(float a_Value = 1, float a_Multiplier = 0)
		: BaseValue(a_Value, a_Multiplier)
	{
		
	}

	void SetValue(float a_Value)
	{
		value = a_Value;
	}

	void IncreaseValue(float a_Value)
	{
		value += a_Value;
	}

	void SetMultiplier(float a_Value)
	{
		multiplier = a_Value;
	}

	void IncreaseMultiplier(float a_Multiplier)
	{
		multiplier += a_Multiplier;
	}
};

class Attribute
{
private:
	//looking at actionScript equivalent
	//for removal of raw and final values,
	//index of uses strict equality,
	//meaning that in comparing two
	//objects, they must refer to the
	//same object
	//these need to be changed so they
	//are collections of pointers instead
	std::vector<RawValue> rawValues;
	std::vector<FinalValue> finalValues;

	std::string m_Name;
public:
	Attribute(const char* a_Name = "No name")
	{
		m_Name = a_Name;
	}

	void AddRawValue(float a_Value, float a_Multiplier)
	{
		rawValues.push_back(RawValue(a_Value, a_Multiplier));
	}

	void AddRawValue(RawValue a_RawValue)
	{
		rawValues.push_back(a_RawValue);
	}

	void AddFinalValue(float a_Value, float a_Multiplier)
	{
		finalValues.push_back(FinalValue(a_Value, a_Multiplier));
	}

	void AddFinalValue(FinalValue a_FinalValue)
	{
		finalValues.push_back(a_FinalValue);
	}

	//combination of both value and multiplier for raw values
	float GetBase() const
	{
		float value = 0;
		float adds = 0;
		float mults = 0;

		for (const RawValue& i : rawValues)
		{
			adds += i.GetValue();
			mults += i.GetMultiplier();
		}

		value += adds;
		value *= (1 + mults);

		return value;
	}

	float GetBaseValue() const
	{
		float value = 0;
		for (const RawValue& i : rawValues)
		{
			value += i.GetValue();
		}
		return value;
	}

	float GetBaseMultiplier() const
	{
		float value = 1;
		float mults = 0;

		for (const RawValue& i : rawValues)
		{
			mults += i.GetMultiplier();
		}

		value *= (1 + mults);

		return value;
	}

	std::string GetName() const
	{
		return m_Name;
	}

	float Calculate() const
	{
		float value = 0;

		float baseValue = 0;
		float baseMultiplier = 0;

		//loop through all base values, adding respective
		//values and multipliers
		for (const RawValue& i : rawValues)
		{
			baseValue += i.GetValue();
			baseMultiplier += i.GetMultiplier();
		}

		value += baseValue;
		value *= (1 + baseMultiplier);

		float derivedValue = 0;
		float derivedMultiplier = 0;

		for (const FinalValue& i : finalValues)
		{
			derivedValue += i.GetValue();
			derivedMultiplier += i.GetMultiplier();
		}

		value += derivedValue;
		value *= (1 + derivedMultiplier);

		return value;
	}

	friend std::ostream& operator<<(std::ostream& out, const Attribute& attribute)
	{
		out << "Name: " << attribute.GetName().c_str() << 
			   " Base value: " << attribute.GetBaseValue() << 
			   " Base multiplier: " << attribute.GetBaseMultiplier() << 
			   " Overall: " << attribute.GetBase() << std::endl;
		return out;
	}
};

class RPGEntity
{
protected:
	int currentLevel;
	const char* name;

	//physical damage, enemy attack reduction
	Attribute muscles;

	//dodge chance, crit chance
	Attribute courage;

	//physical damage reduction, enemy armour reduction
	Attribute charm;

	//hit chance, retaliation damage
	Attribute perception;

	//hit points, attacks before damage decreases
	Attribute endurance;
public:
	//current level needs to be > 0
	RPGEntity(const char* a_Name = "RPGEntity", int a_CurrentLevel = 1)
		: name(a_Name)
	{
		currentLevel = a_CurrentLevel > 0 ? a_CurrentLevel : 1;
	}

	//base entity has base calculation, without modifiers
	//derived classes have the class specific modifiers
	virtual float CalculateExpectedPhysicalDamage()
	{
		return muscles.Calculate() * endurance.Calculate() + perception.Calculate();
	}

	virtual float CalculateExpectedHitPoints()
	{
		return endurance.Calculate() + courage.Calculate();
	}

	virtual float CalculateExpectedDamageReduction() 
	{
		return charm.Calculate() * endurance.GetBaseValue() + perception.Calculate();
	}

	virtual float CalculateExpectedHitChance()
	{
		return perception.Calculate() + sinf(perception.GetBase()) * 10;
	}

	virtual float CalculateExpectedDodgeChance()
	{
		return (perception.Calculate() + courage.Calculate()) * ((perception.GetBaseMultiplier() + 0.1f) / (courage.GetBaseMultiplier() + 0.1f));
	}
};

class Player : public RPGEntity
{
private:

public:
	Player(const char* a_Name = "Player", int a_CurrentLevel = 1)
		: RPGEntity(a_Name, a_CurrentLevel)
	{
		muscles = Attribute("Muscles");
		muscles.AddRawValue(1, 1);
		muscles.AddFinalValue((float)(currentLevel * (5.843f / 7.578f)), 0.87f / 7.0f);

		courage = Attribute("Courage");
		courage.AddRawValue(1, 1);
		courage.AddFinalValue((float)(currentLevel * (4.818f / 6.53f)), 3.5f / 7.63f);

		charm = Attribute("Charm");
		charm.AddRawValue(1, 1);
		charm.AddFinalValue((float)(currentLevel * (5.182f / 7.39f)), 7.73f / 13.67f);

		perception = Attribute("Perception");
		perception.AddRawValue(1, 1);
		perception.AddFinalValue((float)(currentLevel * (6.178f / 7.88f)), 3.456f / 6.123f);

		endurance = Attribute("Endurance");
		endurance.AddRawValue(1, 1);
		endurance.AddFinalValue((float)(currentLevel * (4.184f / 13.77f)), 4.78f / 7.89f);
	}

	void PrintAttributes()
	{
		std::cout << std::endl << "Name: " << name << std::endl;

		std::cout << muscles << courage << charm << perception << endurance << std::endl;

		//Derived stats
		std::cout << "Physical damage: " << CalculateExpectedPhysicalDamage() << std::endl;
		std::cout << "Hit points: " << CalculateExpectedHitPoints() << std::endl;
		std::cout << "Damage reduction: " << CalculateExpectedDamageReduction() << std::endl;
		std::cout << "Hit chance: " << CalculateExpectedHitChance() << std::endl;
		std::cout << "Dodge chance: " << CalculateExpectedDodgeChance() << std::endl;
	}

	virtual float CalculateExpectedHitPoints()
	{
		return RPGEntity::CalculateExpectedHitPoints() * (endurance.GetBaseValue() / currentLevel);
	}

	//overall physical damage is a calculation between
	//the attacker's expected physical with the receiver's expected damage reduction
	virtual float CalculateExpectedPhysicalDamage()
	{
		return RPGEntity::CalculateExpectedPhysicalDamage() * (currentLevel / (perception.GetBaseMultiplier() + 0.1f));
	}

	//overall physical damage is a calculation between
	//the attacker's expected physical with the receiver's expected damage reduction
	virtual float CalculateExpectedDamageReduction()
	{
		return RPGEntity::CalculateExpectedDamageReduction() * (currentLevel / (perception.GetBase() * endurance.GetBaseMultiplier()));
	}

	//overall hit chance is a calculation between
	//the attacker's hit chance and the receiver's dodge chance
	virtual float CalculateExpectedHitChance()
	{
		return RPGEntity::CalculateExpectedHitChance() * (perception.GetBaseValue() / currentLevel) * muscles.GetBaseMultiplier();
	}

	//overall dodge chance is a calculation between 
	//the attacker's hit chance and the receiver's dodge chance
	virtual float CalculateExpectedDodgeChance()
	{
		return RPGEntity::CalculateExpectedDodgeChance() * (currentLevel / courage.GetBaseValue()) * perception.GetBaseMultiplier();
	}
};

class Enemy : public RPGEntity
{
private:
public:
	Enemy(const char* a_Name = "Enemy", int a_CurrentLevel = 1)
		: RPGEntity(a_Name, a_CurrentLevel)
	{
		muscles = Attribute("Muscles");
		muscles.AddRawValue(.5f, .5f);
		muscles.AddFinalValue((float)(currentLevel * (5.843f / 7.578f)), 0.87f / 7.0f);

		courage = Attribute("Courage");
		courage.AddRawValue(.5f, .5f);
		courage.AddFinalValue((float)(currentLevel * (4.818f / 6.53f)), 3.5f / 7.63f);

		charm = Attribute("Charm");
		charm.AddRawValue(.5f, .5f);
		charm.AddFinalValue((float)(currentLevel * (5.182f / 7.39f)), 7.73f / 13.67f);

		perception = Attribute("Perception");
		perception.AddRawValue(.5f, .5f);
		perception.AddFinalValue((float)(currentLevel * (6.178f / 7.88f)), 3.456f / 6.123f);

		endurance = Attribute("Endurance");
		endurance.AddRawValue(.5f, .5f);
		endurance.AddFinalValue((float)(currentLevel * (4.184f / 13.77f)), 4.78f / 7.89f);
	}

	void PrintAttributes()
	{
		std::cout << std::endl << "Name: " << name << std::endl;
		std::cout << muscles << courage << charm << perception << endurance << std::endl;

		//Derived stats
		std::cout << "Physical damage: " << CalculateExpectedPhysicalDamage() << std::endl;
		std::cout << "Hit points: " << CalculateExpectedHitPoints() << std::endl;
		std::cout << "Damage reduction: " << CalculateExpectedDamageReduction() << std::endl;
		std::cout << "Hit chance: " << CalculateExpectedHitChance() << std::endl;
		std::cout << "Dodge chance: " << CalculateExpectedDodgeChance() << std::endl;
	}

	virtual float CalculateExpectedHitPoints()
	{
		return RPGEntity::CalculateExpectedHitPoints();
	}

	virtual float CalculateExpectedPhysicalDamage()
	{
		return RPGEntity::CalculateExpectedPhysicalDamage();
	}

	virtual float CalculateExpectedDamageReduction()
	{
		return RPGEntity::CalculateExpectedDamageReduction();
	}

	virtual float CalculateExpectedHitChance()
	{
		return RPGEntity::CalculateExpectedHitChance();
	}

	virtual float CalculateExpectedDodgeChance()
	{
		return RPGEntity::CalculateExpectedDodgeChance();
	}
};

void AttackPhase(Player t, Enemy a)
{
	float playerAttackDamage = t.CalculateExpectedPhysicalDamage() - a.CalculateExpectedDamageReduction();
	float playerDamageReduction = t.CalculateExpectedDamageReduction() - a.CalculateExpectedPhysicalDamage();

	float enemyAttackDamage = a.CalculateExpectedPhysicalDamage() - t.CalculateExpectedDamageReduction();
	float enemyDamageReduction = a.CalculateExpectedDamageReduction() - t.CalculateExpectedPhysicalDamage();

	float playerHitChance = t.CalculateExpectedHitChance() * a.CalculateExpectedDodgeChance();
	float playerDodgeChance = t.CalculateExpectedDodgeChance();

	float enemyHitChance = a.CalculateExpectedHitChance() * t.CalculateExpectedDodgeChance();
	float enemyDodgeChance = a.CalculateExpectedDodgeChance();
}

void main()
{
	using namespace std;

	srand((unsigned int)(time(NULL)));

	Player t = Player();
	Enemy a = Enemy();

	char* buffer = new char[1024];
	bool quit = false;

	while (!quit)
	{
		//display the current state of the world
		cout << "Current state of the world: " << endl;
		t.PrintAttributes();
		a.PrintAttributes();
		
		system("pause");

		//query user for next action
		cout << "1. Attack" << endl;
		cout << "2. Flee" << endl;
		cout << "3. Quit" << endl;
		
		cin.getline(buffer, 5, '\n');
		switch (buffer[0])
		{
		case '1':
			AttackPhase(t, a);
			break;
		case '2':
			break;
		case '3':
			quit = true;
			break;
		default:
			break;
		}

		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	cout << "GOODBYE" << endl;
	system("pause");
}