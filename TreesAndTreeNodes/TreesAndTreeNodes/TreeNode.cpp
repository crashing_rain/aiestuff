#include "TreeNode.h"

//TreeNode::TreeNode()
//	: parent(nullptr) { }

TreeNode::TreeNode(TreeNode* a_Parent)
	: parent(a_Parent) 
{
	if (parent != nullptr)
	{
		parent->InsertChildNode(this);
	}
}

TreeNode::~TreeNode()
{
	for (std::list<TreeNode*>::iterator treeNodeIter = children.begin(); treeNodeIter != children.end();)
	{
		treeNodeIter = children.erase(treeNodeIter);
		++treeNodeIter;
	}
	children.clear();
}

//void TreeNode::SetParent(TreeNode* a_ParentNode)
//{
//	//Node has a parent
//	if (parent != nullptr)
//	{
//		//New parent add child
//		a_ParentNode->InsertChildNode(this);
//
//		//Old parent remove child
//		parent->RemoveChildNode(this);
//	}
//	parent = a_ParentNode;
//}

void TreeNode::InsertChildNode(TreeNode* a_NewNode)
{
	if (a_NewNode != nullptr)
	{
		if (a_NewNode->parent == nullptr)
		{
			a_NewNode->parent = this;
		}
		children.push_back(a_NewNode);
	}
}

//void TreeNode::RemoveChildNode(TreeNode* a_NodeToDelete)
//{
//	//Has no children, is leaf node
//	if (a_NodeToDelete->children.size() == 0)
//	{
//		//remove from parent's child list,
//		//delete node,
//		//leave function
//		if (a_NodeToDelete->parent != nullptr)
//		{
//			for (std::list<TreeNode*>::iterator i = a_NodeToDelete->parent->children.begin(); i != a_NodeToDelete->parent->children.end(); ++i)
//			{
//				if ((*i) == a_NodeToDelete)
//				{
//					a_NodeToDelete->parent->children.erase(i);
//					delete a_NodeToDelete;
//					return;
//				}
//			}
//		}
//	}
//	else
//	{
//		//need to find node
//		//has children, need to change parent of children of node to delete
//		//to be node to delete's parent
//		for (std::list<TreeNode*>::iterator i = children.begin(); i != children.end(); ++i)
//		{		
//			//Only gets activated if the node to delete is found
//			//Found node to delete
//			if ((*i) == a_NodeToDelete)
//			{
//				//Loop through node to delete's children, changing their parents to be node to delete's parent
//				for (std::list<TreeNode*>::iterator j = a_NodeToDelete->children.begin(); i != a_NodeToDelete->children.end(); ++j)
//				{
//					//Change children's parents to be something else
//					(*j)->parent = a_NodeToDelete->parent;
//				}
//
//				//Done moving pointers around, now to delete node
//				delete a_NodeToDelete;
//				return;
//			}
//
//			RemoveChildNode(a_NodeToDelete);
//		}
//	}
//}
