#ifndef _TREE_NODE_H
#define _TREE_NODE_H

#include <list>

class TreeNode
{
public:
	//TreeNode();
	TreeNode(TreeNode* a_Parent);
	~TreeNode();

	void InsertChildNode(TreeNode* a_NewNode);
	//void RemoveChildNode(TreeNode* a_NodeToDelete);
	//void SetParent(TreeNode* a_ParentNode);

	bool HasChildren()
	{
		if (children.empty())
		{
			return false;
		}
		return true;
	}

	std::list<TreeNode*> children;
	TreeNode* parent;
};

#endif