#ifndef _TREE_H
#define _TREE_H

#include "TreeNode.h"

class Tree
{
public:
	Tree();
	Tree(TreeNode* a_RootNode);
	~Tree();
	void InsertNode(TreeNode* a_NodeToInsert, TreeNode* a_ParentNode, TreeNode* a_SiblingNode);
	void PreOrderTraversal(TreeNode* a_CurrentNode);
	void PostOrderTraversal(TreeNode* a_CurrentNode);
	
private:
	TreeNode* rootNode;
};

#endif